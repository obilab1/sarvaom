// plotUtils.js
import Plotly from 'plotly.js';

export const handlePlotUpdate = (result, setPlotLib, setBokehPlotData, setPlotlyData, setTableData, plotRendered) => {
  // Handling for Bokeh
  if (result.lib === "bokeh") {
    setPlotLib("bokeh");

    if (result.bokehPlot) {
      setBokehPlotData(result.bokehPlot);
      plotRendered.current = false; // Reset to ensure re-rendering
    } else {
      console.error("Bokeh plot data missing");
    }

    if (Array.isArray(result.tableData)) {
      setTableData(result.tableData);
    } else {
      console.error("Table data missing or not an array");
    }
  }
  // Handling for Plotly
  else if (result.lib === "plotly") {
    setPlotLib("plotly");

    if (result.plotlyData) {
      const plotlyConfig = {
        ...result.plotlyData.config,
        modeBarButtonsToAdd: [
          {
            name: "Download SVG",
            icon: Plotly.Icons.camera,
            click: function (gd) {
              Plotly.downloadImage(gd, { format: "svg", filename: "plot" });
            },
          },
        ],
        toImageButtonOptions: {
          format: "png",
          filename: "plot",
        },
      };
      setPlotlyData({
        ...result.plotlyData,
        config: plotlyConfig,
      });
    } else {
      console.error("Plotly plot data missing");
    }

    if (Array.isArray(result.tableData)) {
      setTableData(result.tableData);
    } else {
      console.error("Table data missing or not an array");
    }

    plotRendered.current = false; // Reset to ensure re-rendering
  }
};
