// App.js
import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Sidebar from './components/Sidebar'; // Ensure this path matches your project structure
import { StateProvider } from './components/StateContext'; // Import the StateProvider

const App = () => {
  return (
    <Router>
      {/* Wrap Sidebar and all its children with StateProvider */}
      <StateProvider>
        <Sidebar />
      </StateProvider>
    </Router>
  );
};

export default App;
