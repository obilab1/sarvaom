import React, { useState } from 'react';

const MetadataForm = ({ files, onMetadataSubmit }) => {
  const [metadata, setMetadata] = useState({});

  const handleChange = (fileId, field, value) => {
    setMetadata((prev) => ({
      ...prev,
      [fileId]: {
        ...prev[fileId],
        [field]: value,
      },
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onMetadataSubmit(metadata);
  };

  return (
    <form onSubmit={handleSubmit}>
      {files.map((file) => (
        <div key={file.id}>
          <h3>{file.name}</h3>
          <div>
            <label>Project Name:</label>
            <input
              type="text"
              value={metadata[file.id]?.projectName || ''}
              onChange={(e) => handleChange(file.id, 'projectName', e.target.value)}
            />
          </div>
          <div>
            <label>Project Description:</label>
            <textarea
              value={metadata[file.id]?.projectDescription || ''}
              onChange={(e) => handleChange(file.id, 'projectDescription', e.target.value)}
            />
          </div>
          <div>
            <label>Data Type:</label>
            <select
              value={metadata[file.id]?.dataType || ''}
              onChange={(e) => handleChange(file.id, 'dataType', e.target.value)}
            >
              <option value="">Select data type</option>
              <option value="Metabolomics">Metabolomics</option>
              <option value="Single cell data">Single cell data</option>
              <option value="Others">Others</option>
            </select>
          </div>
        </div>
      ))}
      <button type="submit">Submit Metadata</button>
    </form>
  );
};

export default MetadataForm;
