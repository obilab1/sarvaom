import React, { useState } from 'react';

const NormalizationForm = ({ onSubmit }) => {
    const [selectedNormalization, setSelectedNormalization] = useState('Median'); // Default selection

    // Handle radio button selection changes
    const handleChange = (e) => {
        setSelectedNormalization(e.target.value);
    };

    // Handle form submission
    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit({ normalizationMethod: selectedNormalization }); // Submit the selected normalization method
    };

    return (
        <div className="input-form-container">
            <h2 className="input-form-header" style={{ fontSize: '16px', textAlign: 'center' }}>
                Normalization Method
            </h2>
            <form onSubmit={handleSubmit}>
                <div className="input-group" style={{ textAlign: 'center' }}>
                    <label style={{ marginRight: '20px' }}>
                        <input
                            type="radio"
                            name="normalizationMethod"
                            value="Median"
                            checked={selectedNormalization === 'Median'}
                            onChange={handleChange}
                        />
                        Median
                    </label>
                    <label>
                        <input
                            type="radio"
                            name="normalizationMethod"
                            value="CPM"
                            checked={selectedNormalization === 'CPM'}
                            onChange={handleChange}
                        />
                        Counts per million (CPM)
                    </label>
                </div>
                <button type="submit" className="submit-button" style={{ display: 'block', margin: '20px auto' }}>
                    Next
                </button>
            </form>
        </div>
    );
};

export default NormalizationForm;
