// SCAnalysisForm.jsx
import React, { useEffect } from 'react';

// Import forms specific to single-cell analysis options
import UMAPForm from './UMAPForm';
// import ClusteringForm from './ClusteringForm';
// import ManualCellTypeAnnotationForm from './ManualCellTypeAnnotationForm';
// import MarkerGeneSetForm from './MarkerGeneSetForm';
// import DifferentiallyExpressedGenesForm from './DifferentiallyExpressedGenesForm';

const SCAnalysisForm = ({ selectedAnalysisFeature, onSubmit, handleVisualizationSelect, dataType }) => {
    // Log selected analysis feature and data type to debug
    useEffect(() => {
        console.log("Selected Analysis Feature:", selectedAnalysisFeature);
        console.log("Data Type:", dataType);
    }, [selectedAnalysisFeature, dataType]);

    // Function to determine the component to render based on analysis selection
    const getComponentForAnalysis = (selectedAnalysisFeature) => {
        if (!selectedAnalysisFeature) return null;

        const analysisOption = selectedAnalysisFeature.option;

        // Handle Single cell data-specific cases
        if (dataType === "Single cell data") {
            switch (analysisOption) {
                case 'UMAP':
                    return UMAPForm;
                // case 'Clustering':
                //     return ClusteringForm;
                // case 'Manual cell-type annotation':
                //     return ManualCellTypeAnnotationForm;
                // case 'Marker gene set':
                //     return MarkerGeneSetForm;
                // case 'Differentially-expressed Genes as Markers':
                //     return DifferentiallyExpressedGenesForm;
                default:
                    console.warn(`No component found for analysis option: ${analysisOption}`);
                    return null;
            }
        }

        console.warn(`Data type "${dataType}" is not handled for analysis options.`);
        return null;
    };

    // Select the component to render
    const ComponentToRender = getComponentForAnalysis(selectedAnalysisFeature);

    return (
        <div className="sc-analysis-form-container">
            {ComponentToRender ? (
                <ComponentToRender
                    selectedAnalysisFeature={selectedAnalysisFeature}
                    onSubmit={onSubmit}
                    handleVisualizationSelect={handleVisualizationSelect}
                />
            ) : (
                <p>No additional input required for this analysis selection.</p>
            )}
        </div>
    );
};

export default SCAnalysisForm;
