import React, { createContext, useContext, useState, useEffect,useRef } from 'react';
import useAuth from '../hooks/useAuth';
const StateContext = createContext();

export const useSharedState = () => useContext(StateContext);

export const StateProvider = ({ children }) => {
  const [selectedFeature, setSelectedFeature] = useState(null);
  const [completedSteps, setCompletedSteps] = useState([]);
  const [currentStepIndex, setCurrentStepIndex] = useState(0);
  const [steps, setSteps] = useState([
    "Quality Control", 
    "Filter Cells & Genes", 
    "Doublet Detection", 
    "Normalization", 
    "Feature Selection", 
    "Dimensionality Reduction"
  ]);
  const [nextStep, setNextStep] = useState('');
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [formData, setFormData] = useState({});
  const [selectedAnalysisFeature, setSelectedAnalysisFeature] = useState(null);
  const [selectedTab, setSelectedTab] = useState(0);
  const { auth } = useAuth();
  // Dynamically compute the current step
  const [currentStep, setCurrentStep] = useState(steps[currentStepIndex] || 'Quality Control');


  // const { auth } = useAuth();

  const [bokehPlotData, setBokehPlotData] = useState(null);
  const [tableData, setTableData] = useState([]); // Store DataTable data
  const plotRendered = useRef(false); // Ref to track if plot has been rendered
 
  const [plotLib, setPlotLib] = useState(''); // Store the plot library used
  const [plotlyData, setPlotlyData] = useState(null); // Store Plotly data



  useEffect(() => {
    // Update currentStep whenever currentStepIndex, steps, or selectedFeature changes
    const newCurrentStep = selectedFeature?.option || steps[currentStepIndex];
    setCurrentStep(newCurrentStep);
  }, [currentStepIndex, steps, selectedFeature]);

  return (
    <StateContext.Provider
      value={{
        selectedFeature,
        setSelectedFeature,
        completedSteps,
        setCompletedSteps,
        currentStep,
        setCurrentStepIndex,
        currentStepIndex,
        steps,
        setSteps,
        nextStep,
        setNextStep,
        isFormSubmitted,
        setIsFormSubmitted,
        formData,
        setFormData,
        selectedAnalysisFeature,
        setSelectedAnalysisFeature,
        setSelectedTab,
        selectedTab,
        auth,
        setPlotLib, 
        bokehPlotData,
        setBokehPlotData,
        plotLib,
        setPlotlyData,
        tableData, 
        setTableData, 
        plotRendered,
        plotlyData

      }}
    >
      {children}
    </StateContext.Provider>
  );
};
