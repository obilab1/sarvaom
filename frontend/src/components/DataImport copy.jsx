import React, { useState, useEffect, useCallback, useRef } from 'react';
import './DataImport.css';
import useAuth from '../hooks/useAuth';
import FileUpload from './FileUpload';
import FileList from './FileList';
import Analysis from './Analysis';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import DataTable from 'react-data-table-component';
import 'react-tabs/style/react-tabs.css';
import * as Bokeh from '@bokeh/bokehjs';
import { CSVLink } from 'react-csv';
// import { saveAs } from 'file-saver';
import * as XLSX from 'xlsx';
import ReactTooltip from 'react-tooltip';
const DataImport = () => {
  const [projectName, setProjectName] = useState('');
  const [projectDescription, setProjectDescription] = useState('');
  const [dataType, setDataType] = useState('');
  const [cards, setCards] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [error, setError] = useState('');
  const { auth } = useAuth();
  const [currentProject, setCurrentProject] = useState(null);
  const [selectedTab, setSelectedTab] = useState(0); // Manage active tab state
  const [selectedFeature, setSelectedFeature] = useState(null); // To pass selected feature to Analysis
  const [loading, setLoading] = useState(false); // To manage loading state
  const [bokehPlotData, setBokehPlotData] = useState(null);
  const [tableData, setTableData] = useState([]); // Store DataTable data
  const plotRendered = useRef(false); // Ref to track if plot has been rendered
  const [resultSubTab, setResultSubTab] = useState(0); // Manage subtab state in Results tab
  const [selectedRows, setSelectedRows] = useState(new Set());
  const [metadataEnabled, setMetadataEnabled] = useState(false);
  // const [filterText, setFilterText] = useState('');

  // const PersistentTabPanel = ({ children, isActive }) => {
  //   return (
  //     <div style={{ display: isActive ? 'block' : 'none' }}>
  //       {children}
  //     </div>
  //   );
  // };

  useEffect(() => {
    if (selectedTab === 3 && resultSubTab === 0 && bokehPlotData) {
      if (!plotRendered.current) {
        Bokeh.embed.embed_item(bokehPlotData, 'bokeh-container');
        plotRendered.current = true; // Mark the plot as rendered
      } else {
        const container = document.getElementById('bokeh-container');
        if (container.children.length === 0) {
          Bokeh.embed.embed_item(bokehPlotData, 'bokeh-container');
        }
      }
    }
  }, [selectedTab, resultSubTab, bokehPlotData]);




  // Predefined image URLs for data types
  const images = {
    'Metabolomics': '/static/images/metabolomics.png',
    'Single cell data': '/static/images/singlecell.png',
    'Others': '/static/images/other.png',
    'default': '/static/images/other.png'
  };

  const fetchCards = useCallback(async () => {
    try {
      const response = await fetch('http://127.0.0.1:8000/api/get_project_cards/', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${auth.accessToken}`
        }
      });

      if (response.ok) {
        const data = await response.json();
        const formattedCards = data.map(card => ({
          ...card,
          imageUrl: images[card.data_type] || images['default']
        }));
        setCards(formattedCards);
      } else {
        console.error('Failed to fetch project cards');
      }
    } catch (error) {
      console.error('There was an error fetching the project cards!', error);
    }
  }, [auth.accessToken]);

  useEffect(() => {
    if (auth.accessToken) {
      fetchCards();
    }
  }, [auth.accessToken, fetchCards]);

  const handleFeatureSelect = (feature) => {
    setSelectedFeature(feature);
    setSelectedTab(2); // Switch to the "Analysis" tab
  };

  const handleVisualizationSelect = async (mainCategory, subCategory, option, selectedFiles, formParams, projectName, metadataEnabled) => {
    if (!projectName) {
      console.error("Project name is missing");
      return;
    }

    setLoading(true);

    const data = {
      selectedFiles: selectedFiles,
      selectedOptions: {
        mainCategory,
        subCategory,
        option,
      },
      metadataEnabled: metadataEnabled,
      params: formParams,
    };

    try {
      const response = await fetch(
        `http://127.0.0.1:8000/api/process_selection/${projectName}/`,
        {
          method: "POST",
          headers: {
            Authorization: `Bearer ${auth.accessToken}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        }
      );

      if (response.ok) {
        const result = await response.json();
        console.log("Backend response:", result);
        setBokehPlotData(result.bokehPlot);
        //  // Assuming tableData is the key for your pandas DataFrame
        const arrayTableData = Array.from(result.tableData); // Ensure it’s a true array
        console.log("Is arrayTableData an array?", Array.isArray(arrayTableData));
        console.log("Length of arrayTableData:", arrayTableData.length);

        setTableData(arrayTableData); // Set the state with the true array
        
        plotRendered.current = false; // Reset plotRendered when new data is fetched
        setSelectedTab(3); // Switch to the Results tab
        setResultSubTab(0); // Default to the Figure subtab
      } else {
        console.error("Failed to process selection");
      }
    } catch (error) {
      console.error("Error processing selection:", error);
    } finally {
      setLoading(false);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const isDuplicate = cards.some(card => card.project_name === projectName);
    if (isDuplicate) {
      setError('Project name already exists. Please choose a different name.');
      return;
    }

    const newCard = {
      project_name: projectName,
      project_description: projectDescription,
      data_type: dataType,
      imageUrl: images[dataType] || images['default']
    };

    try {
      const response = await fetch('http://127.0.0.1:8000/api/create_project_card/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${auth.accessToken}`
        },
        body: JSON.stringify({
          project_name: projectName,
          project_description: projectDescription,
          data_type: dataType
        })
      });

      if (response.status === 201) {
        setCards(prevCards => [...prevCards, newCard]);
        setProjectName('');
        setProjectDescription('');
        setDataType('');
        setShowModal(false);
        setError('');
      } else {
        const errorData = await response.json();
        console.error('There was an error creating the project card!', errorData);
      }
    } catch (error) {
      console.error('There was an error creating the project card!', error);
    }
  };

  const handleOpenModal = () => setShowModal(true);
  const handleCloseModal = () => {
    setShowModal(false);
    setError('');
  };

  const handleStart = (projectName) => {
    setCurrentProject(projectName);
  };

    // Define your columns
    // const columns = Object.keys(tableData[0] || {}).map((key) => ({
    //   name: key,
    //   selector: (row) => row[key],
    //   sortable: true,
    // }));


    // Define your columns with tooltips
  const columns = Object.keys(tableData[0] || {}).map((key) => ({
    name: key,
    selector: row => row[key],
    sortable: true,
    cell: row => (
      <div
        data-tip={row[key]} // Set tooltip content
        style={{
          textOverflow: 'ellipsis',
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          maxWidth: '200px', // Set max width for cells
        }}
      >
        {row[key]}
      </div>
    ),
  }));

  //   // Handle search filter
  // const filteredData = tableData.filter(item =>
  //   Object.values(item).some(val => String(val).toLowerCase().includes(filterText.toLowerCase()))
  // );

  // Handle Excel export
  // const exportToExcel = () => {
  //   const ws = XLSX.utils.json_to_sheet(filteredData);
  //   const wb = XLSX.utils.book_new();
  //   XLSX.utils.book_append_sheet(wb, ws, 'TableData');
  //   XLSX.writeFile(wb, 'tableData.xlsx');
  // };

  const exportToExcel = () => {
    const ws = XLSX.utils.json_to_sheet(tableData); // Use tableData if there's no filter
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'TableData');
    XLSX.writeFile(wb, 'tableData.xlsx');
  };



  const customStyles = {
    headRow: {
      style: {
        backgroundColor: '#000', // Black background for header
        color: '#fff', // White text color
        fontWeight: 'bold',
      },
    },
    headCells: {
      style: {
        color: '#fff', // White text color for headers
        whiteSpace: 'nowrap', // Prevent text wrapping
      },
    },
    cells: {
      style: {
        padding: '8px',
        textAlign: 'center',
        wordWrap: 'break-word', // Allow text to wrap within cells
        overflow: 'hidden', // Hide overflowed content
        textOverflow: 'ellipsis', // Add ellipsis for overflowed content
        maxWidth: '200px', // Set a maximum column width
        position: 'relative', // Ensure proper positioning for tooltips
      },
    },
  };
  


  return (
    <div className="container">
      <button className="open-modal-button" onClick={handleOpenModal}>Create Project</button>

      {showModal && (
        <div className="modal">
          <div className="modal-content">
            <span className="close" onClick={handleCloseModal}>&times;</span>
            <h1>Project Creation Form</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-group">
                <label htmlFor="projectName">Project Name</label>
                <input
                  type="text"
                  id="projectName"
                  placeholder="Enter project name"
                  value={projectName}
                  onChange={(e) => setProjectName(e.target.value)}
                />
              </div>

              <div className="form-group">
                <label htmlFor="projectDescription">Project Description</label>
                <textarea
                  id="projectDescription"
                  rows="3"
                  placeholder="Enter project description"
                  value={projectDescription}
                  onChange={(e) => setProjectDescription(e.target.value)}
                />
              </div>

              <div className="form-group">
                <label htmlFor="dataType">Data Type</label>
                <select
                  id="dataType"
                  value={dataType}
                  onChange={(e) => setDataType(e.target.value)}
                >
                  <option value="">Select data type</option>
                  <option value="Metabolomics">Metabolomics</option>
                  <option value="Single cell data">Single cell data</option>
                  <option value="Others">Others</option>
                </select>
              </div>

              {error && <p className="error">{error}</p>}

              <button type="submit">Submit</button>
            </form>
          </div>
        </div>
      )}

      <div className="cards-container">
        {cards.map((card, index) => (
          <div className="card" key={index}>
            <div className="card-header">{card.project_name}</div>
            <img src={card.imageUrl} alt={card.project_name} />
            <div className="card-body">
              <p><strong>Data Type:</strong> {card.data_type}</p>
              <p className="description"><strong>Project Description:</strong> {card.project_description}</p>
              <button className="start-button" onClick={() => handleStart(card.project_name)}>Start</button>
            </div>
          </div>
        ))}
      </div>

      {currentProject && (
        <div className="tabs-container">
          <Tabs selectedIndex={selectedTab} onSelect={(index) => setSelectedTab(index)}>
            <TabList>
              <Tab>Upload Files</Tab>
              <Tab>Uploaded Files</Tab>
              <Tab>Analysis</Tab>
              <Tab>Results</Tab>
            </TabList>

            <TabPanel>
              <FileUpload projectName={currentProject} />
            </TabPanel>
            <TabPanel>
              <FileList 
                projectName={currentProject} 
                onFeatureSelect={handleFeatureSelect} 
                selectedRows={selectedRows} 
                setSelectedRows={setSelectedRows} 
                metadataEnabled={metadataEnabled} 
                setMetadataEnabled={setMetadataEnabled} 
                handleVisualizationSelect={handleVisualizationSelect} 
              />
            </TabPanel>
            <TabPanel>
              <Analysis 
                projectName={currentProject} 
                selectedFeature={selectedFeature}
                selectedFiles={Array.from(selectedRows)} 
                metadataEnabled={metadataEnabled} 
                handleVisualizationSelect={handleVisualizationSelect} 
              />
            </TabPanel>
            <TabPanel>
              <Tabs selectedIndex={resultSubTab} onSelect={(index) => setResultSubTab(index)}>
                <TabList>
                  <Tab>Figure</Tab>
                  <Tab>Table</Tab>
                </TabList>
                <TabPanel>
                  <div id="results-content">
                    {loading ? <p>Loading...</p> : (
                      <div>
                        <div id="bokeh-container"></div>
                      </div>
                    )}
                  </div>
                </TabPanel>
                <TabPanel>
      <div id="table-container">
        {loading ? (
          <p>Loading...</p>
        ) : (
          <div>
            <div style={{ marginBottom: '10px', display: 'flex', gap: '10px' }}>
              <button
                onClick={() => document.getElementById('csvDownloadLink').click()}
                className="btn btn-primary"
                style={{
                  padding: '8px 12px',
                  fontSize: '14px',
                  textAlign: 'center',
                  borderRadius: '4px',
                  lineHeight: '1.5',
                  display: 'inline-block',
                  backgroundColor: '#007bff',
                  color: '#fff',
                  border: '1px solid transparent',
                  cursor: 'pointer',
                }}
              >
                Download CSV
              </button>
              <CSVLink
                data={tableData}
                filename={"tableData.csv"}
                id="csvDownloadLink"
                style={{ display: 'none' }} // Hide the actual CSVLink
              />
              <button
                onClick={exportToExcel}
                className="btn btn-success"
                style={{
                  padding: '8px 12px',
                  fontSize: '14px',
                  textAlign: 'center',
                  borderRadius: '4px',
                  lineHeight: '1.5',
                  display: 'inline-block',
                  backgroundColor: '#28a745',
                  color: '#fff',
                  border: '1px solid transparent',
                  cursor: 'pointer',
                }}
              >
                Download Excel
              </button>
            </div>
            {tableData.length > 0 ? (
              <>
              <DataTable
                columns={columns}
                data={tableData}
                pagination
                paginationPerPage={30} // Set the default items per page
                paginationRowsPerPageOptions={[10, 30, 50, 100, tableData.length]} // Custom pagination options
                highlightOnHover
                dense
                fixedHeader
                fixedHeaderScrollHeight="400px"
                paginationTotalRows={tableData.length} // Show total rows
                customStyles={customStyles} // Apply custom styles
              />
              <ReactTooltip /> {/* Tooltip container */}
            </>
            ) : (
              <p>No data available</p>
            )}
          </div>
        )}
      </div>
    </TabPanel>


              </Tabs>
            </TabPanel>
          </Tabs>
        </div>
      )}
    </div>
  );
};

export default DataImport;
