import React from 'react';

const Explore = () => {
  return (
    <div>
      <h1>Explore</h1>
      <p>Welcome to the Explore page!</p>
    </div>
  );
};

export default Explore;