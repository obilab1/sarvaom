// GeneDropdown.jsx
import React, { useMemo } from 'react';
import Select, { components } from 'react-select';
import { FixedSizeList as List } from 'react-window';
import './GeneDropdown.css';

// VirtualizedMenuList Component
const VirtualizedMenuList = (props) => {
  const { options, children, maxHeight, getValue } = props;
  const height = 35; // Height of each option

  // Calculate the total height of the list based on the number of options
  const [value] = getValue();
  const initialOffset = options.indexOf(value) * height;

  return (
    <List
      height={maxHeight}
      itemCount={children.length}
      itemSize={height}
      initialScrollOffset={initialOffset}
    >
      {({ index, style }) => <div style={style}>{children[index]}</div>}
    </List>
  );
};

const GeneDropdown = ({ genes = [], onGeneSelect, isLoading }) => {
  // Memoize gene options to prevent unnecessary recalculations
  const geneOptions = useMemo(
    () =>
      genes.map((gene) => ({
        value: gene,
        label: gene,
      })),
    [genes]
  );

  // Handle select change
  const handleChange = (selectedOption) => {
    onGeneSelect(selectedOption ? selectedOption.value : null);
  };

  return (
    <div className="gene-dropdown-container">
      <label htmlFor="gene-select" aria-label="Select a gene">
        Select Gene or metadata:
      </label>
      <Select
        id="gene-select"
        options={geneOptions} // Pass gene options
        onChange={handleChange} // Handle change
        placeholder="Type to search genes..."
        isClearable // Allows clearing the selection
        isSearchable // Enables search functionality
        isLoading={isLoading} // Show a loading spinner when fetching genes
        noOptionsMessage={() => 'No genes found'} // Custom message when no options are found
        components={{ MenuList: VirtualizedMenuList }} // Use virtualized menu list
        styles={{
          container: (provided) => ({ ...provided, width: '100%' }), // Full width
          menu: (provided) => ({ ...provided, zIndex: 9999 }), // Ensure dropdown menu is above other content
        }}
      />
    </div>
  );
};

export default GeneDropdown;
