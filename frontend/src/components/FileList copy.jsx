import React, { useState, useEffect, useCallback, useMemo } from "react";
import useAuth from "../hooks/useAuth";
import DataTable from "react-data-table-component";
import { Spinner } from "react-bootstrap";
import "./FileList.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
// import DynamicFormRenderer from "./DynamicFormRenderer";
// import * as Bokeh from "@bokeh/bokehjs";
import { useSharedState } from './StateContext';
import { useNavigate } from 'react-router-dom';

const FileList = ({
  projectName,
  dataType,
  shouldRefetch,
  setShouldRefetch,
  onFeatureSelect,
  handleVisualizationSelect,
  selectedRows,
  setSelectedRows,
  metadataEnabled,
  setMetadataEnabled,
  // completedSteps,
  // setCompletedSteps,
  //currentStep,
  // setCurrentStep
}) => {
  const { auth } = useAuth();
  const [files, setFiles] = useState([]);
  const [newFileName, setNewFileName] = useState("");
  const [editingFile, setEditingFile] = useState(null);
  const [metadata, setMetadata] = useState({});
  const [columns, setColumns] = useState([]);
  const [newColumnName, setNewColumnName] = useState("");
  // const [selectedFeature, setSelectedFeature] = useState(null);
  const [selectedVisualization, setSelectedVisualization] = useState({
    mainCategory: "",
    option: "",
  });
  const [loading, setLoading] = useState(false);
  const [params, setParams] = useState(null); // To store parameters
  // const [completedSteps, setCompletedSteps] = useState([]); // Track completed steps
  const [skippedSteps, setSkippedSteps] = useState([]); // Track skipped steps

  const navigate = useNavigate();

  const {
    selectedFeature,
    setSelectedFeature,
    currentStepIndex,
    setCurrentStepIndex,
    steps,
    setSteps,
    completedSteps,
    setCompletedSteps,
    currentStep,
    selectedAnalysisFeature,
    setSelectedAnalysisFeature
  } = useSharedState();

  // Use useCallback to define fetchFiles
  const fetchFiles = useCallback(async () => {
    try {
      const response = await fetch(
        `http://127.0.0.1:8000/api/projects/${projectName}/files/`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${auth.accessToken}`,
          },
        }
      );

      if (response.ok) {
        const files = await response.json();
        setFiles(files);
        const initialMetadata = files.reduce((acc, file) => {
          acc[file.id] = file.metadata || {};
          return acc;
        }, {});
        setMetadata(initialMetadata);

        if (files.length > 0) {
          const firstFile = files[0];
          if (firstFile.metadata) {
            setColumns(Object.keys(firstFile.metadata));
          }
        }
      } else {
        console.error("Failed to fetch files");
      }
    } catch (error) {
      console.error("Error fetching files:", error);
    }
  }, [auth.accessToken, projectName]);

  useEffect(() => {
    fetchFiles();
  }, [fetchFiles, auth.accessToken, projectName]);

  useEffect(() => {
    if (shouldRefetch) {
      fetchFiles();
      setShouldRefetch(false);
    }
  }, [shouldRefetch, setShouldRefetch, fetchFiles]);

  const handleSelectAll = (isChecked) => {
    if (isChecked) {
      setSelectedRows(new Set(files.map((file) => file.id)));
    } else {
      setSelectedRows(new Set());
    }
  };

  const handleRowSelect = (fileId, isChecked) => {
    setSelectedRows((prev) => {
      const updatedSelection = new Set(prev);
      if (isChecked) {
        updatedSelection.add(fileId);
      } else {
        updatedSelection.delete(fileId);
      }
      return updatedSelection;
    });
  };

  const handleRename = async (fileId) => {
    try {
      const response = await fetch(
        `http://127.0.0.1:8000/api/projects/${projectName}/files/${fileId}/rename/`,
        {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${auth.accessToken}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ new_name: newFileName }),
        }
      );

      if (response.ok) {
        setFiles(
          files.map((file) =>
            file.id === fileId ? { ...file, name: newFileName } : file
          )
        );
        setEditingFile(null);
        setNewFileName("");
      } else {
        const errorData = await response.json();
        console.error("Failed to rename file", errorData);
      }
    } catch (error) {
      console.error("Error renaming file:", error);
    }
  };

  const handleMetadataChange = (fileId, column, value) => {
    setMetadata((prev) => ({
      ...prev,
      [fileId]: {
        ...prev[fileId],
        [column]: value,
      },
    }));
  };

  const handleDelete = async (fileId) => {
    const confirmDelete = window.confirm(
      "Are you sure you want to delete this file?"
    );
    if (!confirmDelete) return;

    try {
      const response = await fetch(
        `http://127.0.0.1:8000/api/projects/${projectName}/files/${fileId}/`,
        {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${auth.accessToken}`,
          },
        }
      );

      if (response.ok) {
        setFiles(files.filter((file) => file.id !== fileId));
      } else {
        console.error("Failed to delete file");
      }
    } catch (error) {
      console.error("Error deleting file:", error);
    }
  };

  const handleAddColumn = () => {
    if (newColumnName.trim() === "") return;
    if (columns.length >= 2) {
      alert("You can only add up to 2 columns.");
      return;
    }
    setColumns([...columns, newColumnName]);
    setNewColumnName("");
  };

  const handleMetadataSubmit = async () => {
    try {
      const promises = files.map((file) => {
        return fetch(
          `http://127.0.0.1:8000/api/projects/${projectName}/files/${file.id}/metadata/`,
          {
            method: "POST",
            headers: {
              Authorization: `Bearer ${auth.accessToken}`,
              "Content-Type": "application/json",
            },
            body: JSON.stringify(metadata[file.id]),
          }
        );
      });

      await fetch(
        `http://127.0.0.1:8000/api/projects/${projectName}/metadata-enabled/`,
        {
          method: "POST",
          headers: {
            Authorization: `Bearer ${auth.accessToken}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ metadataEnabled }),
        }
      );

      const responses = await Promise.all(promises);
      const failedResponses = responses.filter((response) => !response.ok);

      if (failedResponses.length === 0) {
        console.log("Metadata submitted successfully");
        fetchFiles();
      } else {
        console.error("Failed to submit metadata for some files");
      }
    } catch (error) {
      console.error("Error submitting metadata:", error);
    }
  };

  const handleFeatureSelect = (mainCategory, subCategory, option) => {
    const feature = { mainCategory, subCategory, option };
    console.log(feature)
    setSelectedFeature(feature);
    onFeatureSelect(feature); // Notify the parent component about the selected feature
  
    // Check the dataType and adjust behavior accordingly
    if (dataType === "Single cell data") {
      // Behavior specific to "Single cell data"
      setCurrentStepIndex(steps.indexOf(option)); // Update the current step index
    } else {
      // Default behavior for other data types
      // You can include different logic here if needed
    }
  
    // Reset parameters when a new feature is selected
    setParams(null);
  };

  // Handling form submission
  const handleFormSubmit = async (formParams) => {
    setParams(formParams);
    setLoading(true);

    const selectedFiles = Array.from(selectedRows);
    console.log("Selected Files:", selectedFiles);
    console.log("Form Params:", formParams);
    console.log("Current Step Before Submit:", steps[currentStepIndex]);

    const data = {
      selectedFiles: selectedFiles,
      selectedOptions: selectedFeature,
      metadataEnabled,
      params: formParams, 
      currentStep: steps[currentStepIndex], // Include the parameters from the for
    };

    try {
      const response = await fetch(
        `http://127.0.0.1:8000/api/process_selection/${projectName}/`,
        {
          method: "POST",
          headers: {
            Authorization: `Bearer ${auth.accessToken}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        }
      );

      if (response.ok) {
        const result = await response.json();
        console.log("Backend response:", result);
        // Bokeh.embed.embed_item(result.bokehPlot, "bokeh-container");

        // Mark the current step as completed
        // setCompletedSteps((prevSteps) => [...prevSteps, selectedFeature.option]);
        setCompletedSteps((prev) => [...prev, steps[currentStepIndex]]);
        console.log("Completed Steps After Update:", completedSteps);

        // If there is a next step, set it as the current step
      if (result.nextStep) {
        const nextStepIndex = steps.indexOf(result.nextStep);
          if (nextStepIndex !== -1) {
            setCurrentStepIndex(nextStepIndex);
          } else {
            setSteps((prevSteps) => {
              const updatedSteps = [...prevSteps, result.nextStep];
              setCurrentStepIndex(updatedSteps.length - 1); // Correct usage of updated steps
              return updatedSteps;
            });
          }
          console.log("Next Step Set to:", result.nextStep);
      }

      } else {
        console.error("Failed to process selection");
      }
    } catch (error) {
      console.error("Error processing selection:", error);
    } finally {
      setLoading(false);
    }
  };

  // Skip the current step and move to the next
  const handleSkipStep = (option) => {
    setSkippedSteps((prevSteps) => [...prevSteps, option]);
    setCompletedSteps((prevSteps) => [...prevSteps, option]);

    const currentIndex = steps.indexOf(option);
    const nextStep = steps[currentIndex + 1];

    if (nextStep) {
      setCurrentStepIndex(currentIndex + 1);
    }
  };
  
  const featureMenuItems = {
    "LC-MS": {
      Untargeted: ["Feature Detection", "Alignment", "Annotation"],
      Targeted: ["Feature Detection", "Alignment", "Annotation"],
    },
    "GC-MS": {
      "Type A": ["Option A"],
      "Type B": ["Option C"],
    },
  };

  const preprocessingMenuItems = {
    "Preprocessing Type": ["Quality Control", "Filter Cells & Genes", "Doublet Detection", "Normalization","Feature Selection","Dimensionality Reduction"],
  };

  const analysisMenuItems = {
    "Analysis Type": ["UMAP","Clustering", "Manual cell-type annotation","Marker gene set","Differentially-expressed Genes as Markers"],
  };

  const visualizationMenuItems = {
    "Chart Type": ["Line Chart", "Bar Chart", "Pie Chart"],
    "3D Visualization": ["3D Scatter", "3D Surface"],
  };

  // const isStepEnabled = (index) => {
  //   if (index === 0) return true; // First step is always enabled
  //   const previousStep = preprocessingMenuItems["Preprocessing Type"][index - 1];
  //   return completedSteps.includes(previousStep) || skippedSteps.includes(previousStep);
  // };

  console.log("Rendering component and checking steps");


  // Helper function to check if a step is enabled based on the current state
const isStepEnabled = useCallback(
  (step) => {
    const steps = preprocessingMenuItems["Preprocessing Type"];
    const stepIndex = steps.indexOf(step);

    // First step should always be enabled
    if (stepIndex === 0) {
      console.log(`${step} is the first step and is enabled.`);
      return true;
    }

    const previousStep = steps[stepIndex - 1];
    const isEnabled =
      completedSteps.includes(previousStep) || skippedSteps.includes(previousStep);

    console.log(
      `Checking if step "${step}" is enabled. Previous step: "${previousStep}". Enabled: ${isEnabled}`
    );

    // Enable the step if the previous one is completed or skipped
    return isEnabled;
  },
  [completedSteps, skippedSteps, preprocessingMenuItems]
);

// Memoize enabled steps to avoid unnecessary recalculations
const enabledSteps = useMemo(() => {
  const steps = preprocessingMenuItems["Preprocessing Type"];

  const enabledStepsStatus = steps.reduce((acc, step) => {
    acc[step] = isStepEnabled(step);
    console.log(`Step "${step}" is ${acc[step] ? "enabled" : "disabled"}.`);
    return acc;
  }, {});

  console.log("Enabled Steps:", enabledStepsStatus);
  return enabledStepsStatus;
}, [isStepEnabled]); // Depend only on `isStepEnabled` to ensure it recalculates correctly

  
useEffect(() => {
  console.log("Completed Steps Updated:", completedSteps);
}, [completedSteps]);

useEffect(() => {
  console.log("Skipped Steps Updated:", skippedSteps);
}, [skippedSteps]);

useEffect(() => {
  console.log("Current Step Index Updated:", currentStepIndex);
}, [currentStepIndex]);

useEffect(() => {
  console.log("Selected Feature Updated:", selectedFeature);
}, [selectedFeature]);




// Function to handle selections from Analysis menu
const handleAnalysisSelect = (mainCategory, option) => {
  const feature = { mainCategory, subCategory: "Analysis", option };
  console.log("Selected analysis feature:", feature);
  setSelectedAnalysisFeature(feature);
};



  const dataTableColumns = [
    {
      name: (
        <input
          type="checkbox"
          onChange={(e) => handleSelectAll(e.target.checked)}
          checked={selectedRows.size === files.length}
        />
      ),
      cell: (row) => (
        <input
          type="checkbox"
          checked={selectedRows.has(row.id)}
          onChange={(e) => handleRowSelect(row.id, e.target.checked)}
        />
      ),
      width: "60px",
    },
    {
      name: "File Name",
      selector: (row) => row.name,
      sortable: true,
      grow: 2,
      cell: (row) =>
        editingFile === row.id ? (
          <div>
            <input
              type="text"
              value={newFileName}
              onChange={(e) => setNewFileName(e.target.value)}
              placeholder={row.name}
              style={{ width: "100px", fontSize: "12px" }}
            />
            <button
              style={{
                padding: "3px 6px",
                marginRight: "5px",
                fontSize: "12px",
              }}
              onClick={() => handleRename(row.id)}
            >
              Save
            </button>
            <button
              style={{ padding: "3px 6px", fontSize: "12px" }}
              onClick={() => {
                setEditingFile(null);
                setNewFileName("");
              }}
            >
              Cancel
            </button>
          </div>
        ) : (
          <span>{row.name}</span>
        ),
    },
    ...columns.map((column) => ({
      name: column,
      cell: (row) => (
        <input
          type="text"
          value={metadata[row.id]?.[column] || ""}
          onChange={(e) => handleMetadataChange(row.id, column, e.target.value)}
          style={{ width: "80px", fontSize: "12px" }}
        />
      ),
    })),
    {
      name: "Actions",
      cell: (row) => (
        <div>
          <button
            className="delete-button"
            style={{
              padding: "3px 6px",
              marginRight: "5px",
              fontSize: "12px",
            }}
            onClick={() => handleDelete(row.id)}
          >
            Delete
          </button>
          {editingFile !== row.id && (
            <button
              className="rename-button"
              style={{ padding: "3px 6px", fontSize: "12px" }}
              onClick={() => {
                setEditingFile(row.id);
                setNewFileName(row.name);
              }}
            >
              Rename
            </button>
          )}
        </div>
      ),
      button: true,
      width: "120px",
    },
  ];

  // Render different UI based on the dataType
  if (dataType === "Single cell data") {
    return (
      <>
        {loading && (
          <div className="spinner-container">
            <Spinner animation="border" role="status">
              <span className="sr-only">Processing...</span>
            </Spinner>
            <span>Processing your selection...</span>
          </div>
        )}
        {/* <div id="bokeh-container"></div> */}
        {/* {!loading && selectedFeature && !params ? (
  <>
    {console.log("Rendering DynamicFormRenderer with selectedFeature:", selectedFeature)}
    <DynamicFormRenderer
      selectedFeature={selectedFeature}
      onSubmit={handleFormSubmit}
      handleVisualizationSelect={handleVisualizationSelect}
      currentStep={selectedFeature.option}  // Use selectedFeature.option as the current step
      dataType={dataType}  // Ensure to pass the dataType if needed
    />
  </>
) : (
  <p>Loading or waiting for selection...</p>
)} */}
        {!loading && files.length > 0 && (
          <div className="file-list-container">
            <h6 className="files-header">Uploaded Files</h6>
            <div className="table-container">
              <DataTable
                columns={dataTableColumns}
                data={files}
                highlightOnHover
                dense
                responsive
                striped
                noHeader
                pagination={false}
              />
            </div>
            <div className="button-wrapper">
              <div className="dropdown">
                <button className="process-button">
                  Preprocessing <FontAwesomeIcon icon={faCaretDown} />
                </button>
                <div className="dropdown-content">
                  {Object.entries(preprocessingMenuItems).map(
                    ([mainCategory, options]) => (
                      <div key={mainCategory} className="menu-category">
                        <span className="category-title">{mainCategory}</span>
                        <ul className="submenu">
                          {Array.isArray(options) &&
                            options.map((option) => (
                              <li
                                key={option}
                                className="submenu-item"
                                onClick={() => {
                                  console.log(`Clicked on option: ${option}`); // Debug log
                                  handleFeatureSelect(mainCategory, "Preprocessing", option); // Call the function directly
                                }}
                                style={{
                                  pointerEvents: isStepEnabled(option) ? "auto" : "none",
                                  opacity: isStepEnabled(option) ? 1 : 0.5,
                                  display: "flex", // Added to align text and button horizontally
                                  alignItems: "center", // Center align items vertically
                                  justifyContent: "space-between", // Space out option and button
                                }}
                              >
                                {option}
                                <button
                                  className="skip-button"
                                  onClick={(e) => {
                                    e.stopPropagation();
                                    handleSkipStep(option);
                                  }}
                                  disabled={!isStepEnabled(option)}
                                  style={{
                                    marginLeft: "10px",
                                    padding: "2px 5px",
                                    fontSize: "10px",
                                    lineHeight: "1", // Adjust line height for smaller button
                                    borderRadius: "3px",
                                    cursor: "pointer",
                                    backgroundColor: "#f44336", // Red background for visibility
                                    color: "#fff", // White text for contrast
                                    border: "none", // Remove border for a flat button look
                                  }}
                                >
                                  Skip
                                </button>
                              </li>
                            ))}
                        </ul>
                      </div>
                    )
                  )}
                </div>
                
              </div>
              <div className="dropdown">
                <button className="process-button">
                  Analysis <FontAwesomeIcon icon={faCaretDown} />
                </button>
                <div className="dropdown-content">
                  {Object.entries(analysisMenuItems).map(
                    ([mainCategory, options]) => (
                      <div key={mainCategory} className="menu-category">
                        <span className="category-title">{mainCategory}</span>
                        <ul className="submenu">
                          {Array.isArray(options) &&
                            options.map((option) => (
                              <li
                                key={option}
                                className="submenu-item"
                                onClick={() =>
                                  // handleAnalysisSelect(mainCategory, option)
                                  handleFeatureSelect('Analysis',mainCategory, option)
                                }
                              >
                                {option}
                              </li>
                            ))}
                        </ul>
                      </div>
                    )
                  )}
                </div>
              </div>
              <div className="dropdown">
                <button className="process-button">
                  Visualization <FontAwesomeIcon icon={faCaretDown} />
                </button>
                <div className="dropdown-content">
                  {Object.entries(visualizationMenuItems).map(
                    ([mainCategory, options]) => (
                      <div key={mainCategory} className="menu-category">
                        <span className="category-title">{mainCategory}</span>
                        <ul className="submenu">
                          {Array.isArray(options) &&
                            options.map((option) => (
                              <li
                                key={option}
                                className={`submenu-item ${
                                  selectedVisualization.mainCategory ===
                                    mainCategory &&
                                  selectedVisualization.option === option
                                    ? "selected"
                                    : ""
                                }`}
                                onClick={() =>
                                  handleVisualizationSelect(mainCategory, option)
                                }
                              >
                                {option}
                              </li>
                            ))}
                        </ul>
                      </div>
                    )
                  )}
                </div>
              </div>
            </div>
          </div>
        )}
      </>
    );
  }

  // Default render for "Metabolomics" and other data types
  return (
    <>
      {loading && (
        <div className="spinner-container">
          <Spinner animation="border" role="status">
            <span className="sr-only">Processing...</span>
          </Spinner>
          <span>Processing your selection...</span>
        </div>
      )}

      {/* <div id="bokeh-container"></div> */}
      {/* {!loading && selectedFeature && !params && (
        <DynamicFormRenderer
          selectedFeature={selectedFeature}
          onSubmit={handleFormSubmit}
          handleVisualizationSelect={handleVisualizationSelect} // Pass the function as a prop
        />
      )} */}
    
      {!loading && files.length > 0 && (
        <div className="file-list-container">
          <div className="add-column">
            <input
              type="text"
              value={newColumnName}
              onChange={(e) => setNewColumnName(e.target.value)}
              placeholder="New column name"
              style={{ width: "120px", marginRight: "10px", padding: "3px" }}
            />
            <button
              onClick={handleAddColumn}
              style={{ padding: "3px 6px", fontSize: "12px" }}
            >
              Add Column
            </button>
          </div>
          <h6 className="files-header">Uploaded Files</h6>
          <div className="metadata-checkbox">
            <input
              type="checkbox"
              id="metadata-check"
              checked={metadataEnabled}
              onChange={(e) => setMetadataEnabled(e.target.checked)}
            />
            <label htmlFor="metadata-check">
              * Have you uploaded the Sample.xlsx file? This file includes
              metadata for your mzML files.
            </label>
          </div>
          <div className="table-container">
            <DataTable
              columns={dataTableColumns}
              data={files}
              highlightOnHover
              dense
              responsive
              striped
              noHeader
              pagination={false}
            />
          </div>
          <div className="button-wrapper">
            <button
              onClick={handleMetadataSubmit}
              style={{
                padding: "5px 10px",
                fontSize: "12px",
                marginTop: "10px",
                backgroundColor: "#6f42c1",
                color: "#fff",
                border: "none",
                borderRadius: "4px",
                cursor: "pointer",
              }}
            >
              Save Metadata
            </button>
            <div className="dropdown">
              <button className="process-button">
                Features <FontAwesomeIcon icon={faCaretDown} />
              </button>
              <div className="dropdown-content">
                {Object.entries(featureMenuItems).map(
                  ([mainCategory, subcategories]) => (
                    <div key={mainCategory} className="menu-category">
                      <span className="category-title">{mainCategory}</span>
                      <ul className="submenu">
                        {Object.entries(subcategories).map(
                          ([subCategory, options]) => (
                            <li
                              key={subCategory}
                              className="submenu-item"
                              onMouseEnter={(e) => {
                                e.currentTarget
                                  .querySelector(".subsubmenu")
                                  .classList.add("active");
                              }}
                              onMouseLeave={(e) => {
                                e.currentTarget
                                  .querySelector(".subsubmenu")
                                  .classList.remove("active");
                              }}
                            >
                              <span className="subcategory-title">
                                {subCategory}
                              </span>
                              <ul className="subsubmenu">
                                {Array.isArray(options) &&
                                  options.map((option) => (
                                    <li
                                      key={option}
                                      className={`subsubmenu-item ${
                                        selectedFeature &&
                                        selectedFeature.mainCategory ===
                                          mainCategory &&
                                        selectedFeature.subCategory ===
                                          subCategory &&
                                        selectedFeature.option === option
                                          ? "selected"
                                          : ""
                                      }`}
                                      onClick={() =>
                                        handleFeatureSelect(
                                          mainCategory,
                                          subCategory,
                                          option
                                        )
                                      }
                                    >
                                      {option}
                                    </li>
                                  ))}
                              </ul>
                            </li>
                          )
                        )}
                      </ul>
                    </div>
                  )
                )}
              </div>
            </div>

            <div className="dropdown">
              <button className="process-button">
                Visualization <FontAwesomeIcon icon={faCaretDown} />
              </button>
              <div className="dropdown-content">
                {Object.entries(visualizationMenuItems).map(
                  ([mainCategory, options]) => (
                    <div key={mainCategory} className="menu-category">
                      <span className="category-title">{mainCategory}</span>
                      <ul className="submenu">
                        {Array.isArray(options) &&
                          options.map((option) => (
                            <li
                              key={option}
                              className={`submenu-item ${
                                selectedVisualization.mainCategory ===
                                  mainCategory &&
                                selectedVisualization.option === option
                                  ? "selected"
                                  : ""
                              }`}
                              onClick={() =>
                                handleVisualizationSelect(mainCategory, option)
                              }
                            >
                              {option}
                            </li>
                          ))}
                      </ul>
                    </div>
                  )
                )}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default FileList;
