import React from 'react';

const DoubletDetectionForm = ({ onSubmit }) => {
    // Handle form submission
    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(); // Submit the form without parameters, as defaults are used
    };

    return (
        <div className="input-form-container" style={{ textAlign: 'center', padding: '20px' }}>
            <h2 className="input-form-header" style={{ fontSize: '18px', marginBottom: '20px' }}>
                Doublet Detection
            </h2>
            <p style={{ 
                fontSize: '16px', 
                lineHeight: '1.6', 
                margin: '0 auto 20px', 
                maxWidth: '600px',
                textAlign: 'center'
            }}>
                In the next step, we will run a doublet detection algorithm. This process is important because doublets can 
                cause errors in our analysis, such as misclassifications or misleading results.
            </p>
            <p style={{ 
                fontSize: '16px', 
                lineHeight: '1.6', 
                margin: '0 auto 20px', 
                maxWidth: '600px',
                textAlign: 'center'
            }}>
                We use the Scrublet method, a tool in Scanpy, to detect doublets. Scrublet predicts doublets by comparing 
                real cell data with simulated doublets using a nearest-neighbor classifier.
            </p>
            <p style={{ 
                fontSize: '16px', 
                lineHeight: '1.6', 
                margin: '0 auto 20px', 
                maxWidth: '600px',
                textAlign: 'center'
            }}>
                This method adds two key metrics to our data: <code>doublet_score</code> and <code>predicted_doublet</code>. 
                We can filter directly using <code>predicted_doublet</code> or use <code>doublet_score</code> later to remove 
                clusters with high doublet scores.
            </p>
            <button onClick={handleSubmit} className="submit-button" style={{ marginTop: '20px' }}>Next</button>
        </div>
    );
};

export default DoubletDetectionForm;
