import React, { useState } from 'react';
import './Preprocessing.css';

const Preprocessing = ({ projectName }) => {
  const [selectedAlgorithm, setSelectedAlgorithm] = useState({});
  const [dropdownOpen, setDropdownOpen] = useState({});

  const steps = [
    {
      id: 'peak-detection',
      label: 'Peak Detection',
      description: 'Detect peaks in each mzML file.',
      algorithms: ['Algorithm A', 'Algorithm B', 'Algorithm C'],
      color: '#66c2a5' // Custom blue color
    },
    {
      id: 'alignment',
      label: 'Alignment',
      description: 'Align peaks across different samples to account for retention time shifts.',
      algorithms: ['Algorithm X', 'Algorithm Y', 'Algorithm Z'],
      color: '#fc8d62' // Custom green color
    },
    {
      id: 'normalization',
      label: 'Normalization',
      description: 'Normalize the data to correct for variations in sample concentration or injection volume.',
      algorithms: ['Algorithm 1', 'Algorithm 2', 'Algorithm 3'],
      color: '#8da0cb' // Custom yellow color
    },
    {
      id: 'annotation',
      label: 'Annotation',
      description: 'Annotate features',
      algorithms: ['Algorithm 1', 'Algorithm 2', 'Algorithm 3'],
      color: '#a50f15' // Custom yellow color
    }
  ];

  const handleToggle = (stepId) => {
    setDropdownOpen((prevOpen) => ({ ...prevOpen, [stepId]: !prevOpen[stepId] }));
  };

  const handleAlgorithmSelect = (stepId, algo) => {
    setSelectedAlgorithm({ ...selectedAlgorithm, [stepId]: algo });
    setDropdownOpen((prevOpen) => ({ ...prevOpen, [stepId]: false })); // Close the dropdown menu
  };

  return (
    <div className="preprocessing-container">
      <h6>Preprocessing Steps</h6>
      <div className="dropdown-container">
        {steps.map(step => (
          <div key={step.id} className="dropdown-inline">
            <div
              className="dropdown-toggle"
              onClick={() => handleToggle(step.id)}
              title={step.description}
              style={{ backgroundColor: step.color, borderColor: step.color, fontSize: '0.8rem', padding: '5px 10px', color: 'white' }}
            >
              {step.label}
            </div>
            {dropdownOpen[step.id] && (
              <div className="dropdown-menu show">
                {step.algorithms.map(algo => (
                  <div
                    key={algo}
                    className="dropdown-item"
                    onClick={() => handleAlgorithmSelect(step.id, algo)}
                    style={{ fontSize: '0.8rem', whiteSpace: 'normal' }}
                  >
                    {algo}
                  </div>
                ))}
              </div>
            )}
            {selectedAlgorithm[step.id] && (
              <div className="selected-algorithm">
                <p>Selected: {selectedAlgorithm[step.id]}</p>
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default Preprocessing;
