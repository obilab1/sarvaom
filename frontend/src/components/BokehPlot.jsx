// BokehPlot.jsx
import React, { useEffect, useRef } from 'react';
import * as Bokeh from '@bokeh/bokehjs';

const BokehPlot = ({ plotData }) => {
  const plotContainerRef = useRef(null); // Reference to the container where the plot will render
  const plotRendered = useRef(false); // Track whether the plot has been rendered

  useEffect(() => {
    console.log('BokehPlot useEffect triggered'); // Log when useEffect is called

    const renderPlot = () => {
      const container = plotContainerRef.current;

      if (container) {
        // Clear the container to avoid duplicate rendering
        while (container.firstChild) {
          container.removeChild(container.firstChild);
        }

        // Embed the Bokeh plot only if not rendered
        if (plotData && !plotRendered.current) {
          Bokeh.embed.embed_item(plotData, container);
          console.log('Bokeh plot embedded'); // Log when the plot is embedded
          plotRendered.current = true; // Mark as rendered
        }
      }
    };

    // Render the plot if conditions are met
    renderPlot();

    // Cleanup function to clear the container and reset flags
    return () => {
      const container = plotContainerRef.current;
      if (container) {
        container.innerHTML = '';
        plotRendered.current = false; // Reset the render flag
        console.log('BokehPlot cleanup executed'); // Log cleanup action
      }
    };
  }, [plotData]); // Depend only on plotData to avoid redundant renders

  return <div ref={plotContainerRef} style={{ width: '100%', height: '400px' }}></div>;
};

export default BokehPlot;
