import * as Bokeh from "@bokeh/bokehjs";
import React, { useState, useEffect, useCallback } from "react";
import useAuth from "../hooks/useAuth";
import DataTable from "react-data-table-component";
import { Spinner } from "react-bootstrap";
import "./FileList.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import DynamicFormRenderer from "./DynamicFormRenderer"; 

const FileList = ({ projectName, shouldRefetch, setShouldRefetch, onFeatureSelect, handleVisualizationSelect ,selectedRows, setSelectedRows, metadataEnabled, setMetadataEnabled}) => {  // Add handleVisualizationSelect as a prop
  const { auth } = useAuth();
  const [files, setFiles] = useState([]);
  const [newFileName, setNewFileName] = useState("");
  const [editingFile, setEditingFile] = useState(null);
  const [metadata, setMetadata] = useState({});
  const [columns, setColumns] = useState([]);
  const [newColumnName, setNewColumnName] = useState("");
  // const [metadataEnabled, setMetadataEnabled] = useState(false);
  // const [selectedRows, setSelectedRows] = useState(new Set());
  const [selectedFeature, setSelectedFeature] = useState(null);
  const [selectedVisualization, setSelectedVisualization] = useState({
    mainCategory: "",
    option: "",
  });
  const [loading, setLoading] = useState(false);
  const [params, setParams] = useState(null); // To store parameters

  // Use useCallback to define fetchFiles
  const fetchFiles = useCallback(async () => {
    try {
      const response = await fetch(
        `http://127.0.0.1:8000/api/projects/${projectName}/files/`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${auth.accessToken}`,
          },
        }
      );

      if (response.ok) {
        const files = await response.json();
        setFiles(files);
        const initialMetadata = files.reduce((acc, file) => {
          acc[file.id] = file.metadata || {};
          return acc;
        }, {});
        setMetadata(initialMetadata);

        if (files.length > 0) {
          const firstFile = files[0];
          if (firstFile.metadata) {
            setColumns(Object.keys(firstFile.metadata));
          }
        }
      } else {
        console.error("Failed to fetch files");
      }
    } catch (error) {
      console.error("Error fetching files:", error);
    }
  }, [auth.accessToken, projectName]);

  useEffect(() => {
    fetchFiles();
  }, [fetchFiles, auth.accessToken, projectName]);

  useEffect(() => {
    if (shouldRefetch) {
      fetchFiles();
      setShouldRefetch(false);
    }
  }, [shouldRefetch, setShouldRefetch, fetchFiles]);

  const handleSelectAll = (isChecked) => {
    if (isChecked) {
      setSelectedRows(new Set(files.map((file) => file.id)));
    } else {
      setSelectedRows(new Set());
    }
  };

  const handleRowSelect = (fileId, isChecked) => {
    setSelectedRows((prev) => {
      const updatedSelection = new Set(prev);
      if (isChecked) {
        updatedSelection.add(fileId);
      } else {
        updatedSelection.delete(fileId);
      }
      return updatedSelection;
    });
  };

  const handleRename = async (fileId) => {
    try {
      const response = await fetch(
        `http://127.0.0.1:8000/api/projects/${projectName}/files/${fileId}/rename/`,
        {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${auth.accessToken}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ new_name: newFileName }),
        }
      );

      if (response.ok) {
        setFiles(
          files.map((file) =>
            file.id === fileId ? { ...file, name: newFileName } : file
          )
        );
        setEditingFile(null);
        setNewFileName("");
      } else {
        const errorData = await response.json();
        console.error("Failed to rename file", errorData);
      }
    } catch (error) {
      console.error("Error renaming file:", error);
    }
  };

  const handleMetadataChange = (fileId, column, value) => {
    setMetadata((prev) => ({
      ...prev,
      [fileId]: {
        ...prev[fileId],
        [column]: value,
      },
    }));
  };

  const handleDelete = async (fileId) => {
    const confirmDelete = window.confirm(
      "Are you sure you want to delete this file?"
    );
    if (!confirmDelete) return;

    try {
      const response = await fetch(
        `http://127.0.0.1:8000/api/projects/${projectName}/files/${fileId}/`,
        {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${auth.accessToken}`,
          },
        }
      );

      if (response.ok) {
        setFiles(files.filter((file) => file.id !== fileId));
      } else {
        console.error("Failed to delete file");
      }
    } catch (error) {
      console.error("Error deleting file:", error);
    }
  };

  const handleAddColumn = () => {
    if (newColumnName.trim() === "") return;
    if (columns.length >= 2) {
      alert("You can only add up to 2 columns.");
      return;
    }
    setColumns([...columns, newColumnName]);
    setNewColumnName("");
  };

  const handleMetadataSubmit = async () => {
    try {
      const promises = files.map((file) => {
        return fetch(
          `http://127.0.0.1:8000/api/projects/${projectName}/files/${file.id}/metadata/`,
          {
            method: "POST",
            headers: {
              Authorization: `Bearer ${auth.accessToken}`,
              "Content-Type": "application/json",
            },
            body: JSON.stringify(metadata[file.id]),
          }
        );
      });

      await fetch(
        `http://127.0.0.1:8000/api/projects/${projectName}/metadata-enabled/`,
        {
          method: "POST",
          headers: {
            Authorization: `Bearer ${auth.accessToken}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ metadataEnabled }),
        }
      );

      const responses = await Promise.all(promises);
      const failedResponses = responses.filter((response) => !response.ok);

      if (failedResponses.length === 0) {
        console.log("Metadata submitted successfully");
        fetchFiles();
      } else {
        console.error("Failed to submit metadata for some files");
      }
    } catch (error) {
      console.error("Error submitting metadata:", error);
    }
  };

  const handleFeatureSelect = (mainCategory, subCategory, option) => {
    const feature = { mainCategory, subCategory, option };
    setSelectedFeature(feature);
    onFeatureSelect(feature); // Notify the parent component (DataImport) to switch tabs
    setParams(null); // Reset params when a new feature is selected
  };

  const handleFormSubmit = async (formParams) => {
    setParams(formParams);
    setLoading(true);

    const selectedFiles = Array.from(selectedRows);
    console.log('slected files:',selectedFiles )

    const data = {
        selectedFiles: selectedFiles,
        selectedOptions: selectedFeature,
        metadataEnabled,
        params: formParams, // Include the parameters from the form
    };

    try {
        const response = await fetch(
            `http://127.0.0.1:8000/api/process_selection/${projectName}/`,
            {
                method: "POST",
                headers: {
                    Authorization: `Bearer ${auth.accessToken}`,
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(data),
            }
        );

        if (response.ok) {
            const result = await response.json();
            console.log("Backend response:", result);
            Bokeh.embed.embed_item(result.bokehPlot, "bokeh-container");
        } else {
            console.error("Failed to process selection");
        }
    } catch (error) {
        console.error("Error processing selection:", error);
    } finally {
        setLoading(false);
    }
};


  const featureMenuItems = {
    "LC-MS": {
      Untargeted: ["Feature Detection", "Alignment", "Annotation"],
      Targeted: ["Feature Detection", "Alignment", "Annotation"],
    },
    "GC-MS": {
      "Type A": ["Option A"],
      "Type B": ["Option C"],
    },
  };

  const visualizationMenuItems = {
    "Chart Type": ["Line Chart", "Bar Chart", "Pie Chart"],
    "3D Visualization": ["3D Scatter", "3D Surface"],
  };

  const dataTableColumns = [
    {
      name: (
        <input
          type="checkbox"
          onChange={(e) => handleSelectAll(e.target.checked)}
          checked={selectedRows.size === files.length}
        />
      ),
      cell: (row) => (
        <input
          type="checkbox"
          checked={selectedRows.has(row.id)}
          onChange={(e) => handleRowSelect(row.id, e.target.checked)}
        />
      ),
      width: "60px",
    },
    {
      name: "File Name",
      selector: (row) => row.name,
      sortable: true,
      grow: 2,
      cell: (row) =>
        editingFile === row.id ? (
          <div>
            <input
              type="text"
              value={newFileName}
              onChange={(e) => setNewFileName(e.target.value)}
              placeholder={row.name}
              style={{ width: "100px", fontSize: "12px" }}
            />
            <button
              style={{
                padding: "3px 6px",
                marginRight: "5px",
                fontSize: "12px",
              }}
              onClick={() => handleRename(row.id)}
            >
              Save
            </button>
            <button
              style={{ padding: "3px 6px", fontSize: "12px" }}
              onClick={() => {
                setEditingFile(null);
                setNewFileName("");
              }}
            >
              Cancel
            </button>
          </div>
        ) : (
          <span>{row.name}</span>
        ),
    },
    ...columns.map((column) => ({
      name: column,
      cell: (row) => (
        <input
          type="text"
          value={metadata[row.id]?.[column] || ""}
          onChange={(e) => handleMetadataChange(row.id, column, e.target.value)}
          style={{ width: "80px", fontSize: "12px" }}
        />
      ),
    })),
    {
      name: "Actions",
      cell: (row) => (
        <div>
          <button
            className="delete-button"
            style={{
              padding: "3px 6px",
              marginRight: "5px",
              fontSize: "12px",
            }}
            onClick={() => handleDelete(row.id)}
          >
            Delete
          </button>
          {editingFile !== row.id && (
            <button
              className="rename-button"
              style={{ padding: "3px 6px", fontSize: "12px" }}
              onClick={() => {
                setEditingFile(row.id);
                setNewFileName(row.name);
              }}
            >
              Rename
            </button>
          )}
        </div>
      ),
      button: true,
      width: "120px",
    },
  ];

  return (
    <>
      {loading && (
        <div className="spinner-container">
          <Spinner animation="border" role="status">
            <span className="sr-only">Processing...</span>
          </Spinner>
          <span>Processing your selection...</span>
        </div>
      )}
  
      <div id="bokeh-container"></div>
      {!loading && selectedFeature && !params && (
        <DynamicFormRenderer
          selectedFeature={selectedFeature}
          onSubmit={handleFormSubmit}
          handleVisualizationSelect={handleVisualizationSelect} // Pass the function as a prop
        />
      )}
    
      {!loading && files.length > 0 && (
        <div className="file-list-container">
          <div className="add-column">
            <input
              type="text"
              value={newColumnName}
              onChange={(e) => setNewColumnName(e.target.value)}
              placeholder="New column name"
              style={{ width: "120px", marginRight: "10px", padding: "3px" }}
            />
            <button
              onClick={handleAddColumn}
              style={{ padding: "3px 6px", fontSize: "12px" }}
            >
              Add Column
            </button>
          </div>
          <h6 className="files-header">Uploaded Files</h6>
          <div className="metadata-checkbox">
            <input
              type="checkbox"
              id="metadata-check"
              checked={metadataEnabled}
              onChange={(e) => setMetadataEnabled(e.target.checked)}
            />
            <label htmlFor="metadata-check">
              * Have you uploaded the Sample.xlsx file? This file includes
              metadata for your mzML files.
            </label>
          </div>
          <div className="table-container">
            <DataTable
              columns={dataTableColumns}
              data={files}
              highlightOnHover
              dense
              responsive
              striped
              noHeader
              pagination={false}
            />
          </div>
          <div className="button-wrapper">
            <button
              onClick={handleMetadataSubmit}
              style={{
                padding: "5px 10px",
                fontSize: "12px",
                marginTop: "10px",
                backgroundColor: "#6f42c1",
                color: "#fff",
                border: "none",
                borderRadius: "4px",
                cursor: "pointer",
              }}
            >
              Save Metadata
            </button>
            <div className="dropdown">
              <button className="process-button">
                Features <FontAwesomeIcon icon={faCaretDown} />
              </button>
              <div className="dropdown-content">
                {Object.entries(featureMenuItems).map(
                  ([mainCategory, subcategories]) => (
                    <div key={mainCategory} className="menu-category">
                      <span className="category-title">{mainCategory}</span>
                      <ul className="submenu">
                        {Object.entries(subcategories).map(
                          ([subCategory, options]) => (
                            <li
                              key={subCategory}
                              className="submenu-item"
                              onMouseEnter={(e) => {
                                e.currentTarget
                                  .querySelector(".subsubmenu")
                                  .classList.add("active");
                              }}
                              onMouseLeave={(e) => {
                                e.currentTarget
                                  .querySelector(".subsubmenu")
                                  .classList.remove("active");
                              }}
                            >
                              <span className="subcategory-title">
                                {subCategory}
                              </span>
                              <ul className="subsubmenu">
                                {Array.isArray(options) &&
                                  options.map((option) => (
                                    <li
                                      key={option}
                                      className={`subsubmenu-item ${
                                        selectedFeature &&
                                        selectedFeature.mainCategory ===
                                          mainCategory &&
                                        selectedFeature.subCategory ===
                                          subCategory &&
                                        selectedFeature.option === option
                                          ? "selected"
                                          : ""
                                      }`}
                                      onClick={() =>
                                        handleFeatureSelect(
                                          mainCategory,
                                          subCategory,
                                          option
                                        )
                                      }
                                    >
                                      {option}
                                    </li>
                                  ))}
                              </ul>
                            </li>
                          )
                        )}
                      </ul>
                    </div>
                  )
                )}
              </div>
            </div>
            <div className="dropdown">
              <button className="process-button">
                Visualization <FontAwesomeIcon icon={faCaretDown} />
              </button>
              <div className="dropdown-content">
                {Object.entries(visualizationMenuItems).map(
                  ([mainCategory, options]) => (
                    <div key={mainCategory} className="menu-category">
                      <span className="category-title">{mainCategory}</span>
                      <ul className="submenu">
                        {Array.isArray(options) &&
                          options.map((option) => (
                            <li
                              key={option}
                              className={`submenu-item ${
                                selectedVisualization.mainCategory ===
                                  mainCategory &&
                                selectedVisualization.option === option
                                  ? "selected"
                                  : ""
                              }`}
                              onClick={() =>
                                handleVisualizationSelect(mainCategory, option)
                              }
                            >
                              {option}
                            </li>
                          ))}
                      </ul>
                    </div>
                  )
                )}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default FileList;
