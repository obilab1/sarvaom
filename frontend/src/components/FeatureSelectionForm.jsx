import React, { useState } from 'react';

const FeatureSelectionForm = ({ onSubmit }) => {
    // Define the state for form inputs
    const [params, setParams] = useState({
        n_top_genes: 2000,  // Default value
        flavor: 'seurat'    // Default option
    });

    // Handle input changes
    const handleChange = (e) => {
        const { name, value } = e.target;
        setParams({
            ...params,
            [name]: name === 'n_top_genes' ? parseInt(value, 10) : value // Ensure n_top_genes is a number
        });
    };

    // Handle form submission
    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(params); // Submit the form parameters
    };

    return (
        <div className="input-form-container">
            <h2 className="input-form-header" style={{ fontSize: '16px' }}>
                Feature Selection
            </h2>
            <form onSubmit={handleSubmit}>
                <div className="input-group">
                    <label>
                        Number of highly-variable genes to keep (n_top_genes):
                        <input
                            type="number"
                            name="n_top_genes"
                            value={params.n_top_genes}
                            onChange={handleChange}
                            min="1"
                            placeholder="2000"
                        />
                    </label>
                </div>
                <div className="input-group">
                    <label>
                        Flavor:
                        <select
                            name="flavor"
                            value={params.flavor}
                            onChange={handleChange}
                        >
                            <option value="seurat">seurat</option>
                            <option value="cell_ranger">cell_ranger</option>
                            <option value="seurat_v3">seurat_v3</option>
                            <option value="seurat_v3_paper">seurat_v3_paper</option>
                        </select>
                    </label>
                </div>
                <button type="submit" className="submit-button">Next</button>
            </form>
        </div>
    );
};

export default FeatureSelectionForm;
