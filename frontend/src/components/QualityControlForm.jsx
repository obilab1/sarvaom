import React, { useState } from 'react';

const QualityControlForm = ({ onSubmit }) => {
    const [params, setParams] = useState({
        filterType: 'startswith',
        filterValue: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setParams({
            ...params,
            [name]: value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('Submitting form with params:', params); // Debug log to check form data
        onSubmit(params); // Call the onSubmit function passed from parent
    };

    return (
        <div className="input-form-container">
            <h2 className="input-form-header" style={{ fontSize: '16px' }}>
                Quality Control
            </h2>
            <form onSubmit={handleSubmit}>
                <div className="input-group">
                    <label>
                        Filter Type:
                        <select
                            name="filterType"
                            value={params.filterType}
                            onChange={handleChange}
                        >
                            <option value="startswith">Starts With</option>
                            <option value="regexp">Regexp</option>
                        </select>
                    </label>
                </div>
                <div className="input-group">
                    <label>
                        Filter Value:
                        <input
                            type="text"
                            name="filterValue"
                            value={params.filterValue}
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <button type="submit" className="submit-button">Next</button>
            </form>
        </div>
    );
};

export default QualityControlForm;
