import React, { useState, useEffect, useCallback, useRef } from 'react';
import './DataImport.css';
import useAuth from '../hooks/useAuth';
import FileUpload from './FileUpload';
import FileList from './FileList';
import Analysis from './Analysis';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import DataTable from 'react-data-table-component';
import 'react-tabs/style/react-tabs.css';
import * as Bokeh from '@bokeh/bokehjs';
import { CSVLink } from 'react-csv';
import * as XLSX from 'xlsx';
import ReactTooltip from 'react-tooltip';
import Plot from 'react-plotly.js';
import Plotly from 'plotly.js-dist-min';
import { useSharedState } from './StateContext';
import BokehPlot from './BokehPlot';
import ResultsPanel from './ResultsPanel';
import { handlePlotUpdate } from '../utils/plotUtils'; 

const DataImport = () => {

  // Access shared state from context

  const {
    selectedFeature,
    setSelectedFeature,
    completedSteps,
    setCompletedSteps,
    currentStep,
    setCurrentStep,
    formData,
    setFormData, // Use shared form data if needed
    setNextStep,
    setSteps,
    nextStep,
    selectedAnalysisFeature, // Add selectedAnalysisFeature to the context
    setSelectedAnalysisFeature, // Setter for the selectedAnalysisFeature
    selectedTab, 
    setSelectedTab,
    auth,
    setPlotLib, 
    bokehPlotData,
    setBokehPlotData,
    plotLib,
    setPlotlyData,
    tableData, 
    setTableData, 
    plotRendered,
    plotlyData
  } = useSharedState();

  
    // Keep the rest of the local states as they are
    const [projectName, setProjectName] = useState('');
    const [projectDescription, setProjectDescription] = useState('');
    const [dataType, setDataType] = useState('');
    const [cards, setCards] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [error, setError] = useState('');
    // const { auth } = useAuth();
    const [currentProject, setCurrentProject] = useState(null);
    // const [selectedTab, setSelectedTab] = useState(0); // Manage active tab state
    const [loading, setLoading] = useState(false); // To manage loading state
    // const [bokehPlotData, setBokehPlotData] = useState(null);
    // const [tableData, setTableData] = useState([]); // Store DataTable data
    // const plotRendered = useRef(false); // Ref to track if plot has been rendered
    const [resultSubTab, setResultSubTab] = useState(0); // Manage subtab state in Results tab
    const [selectedRows, setSelectedRows] = useState(new Set());
    const [metadataEnabled, setMetadataEnabled] = useState(false);
    // const [plotLib, setPlotLib] = useState(''); // Store the plot library used
    // const [plotlyData, setPlotlyData] = useState(null); // Store Plotly data
  
  // Define currentParameters state to manage form parameters
  // const [currentParameters, setCurrentParameters] =  useState({});

  // const [projectName, setProjectName] = useState('');
  // const [projectDescription, setProjectDescription] = useState('');
  // const [dataType, setDataType] = useState('');
  // const [cards, setCards] = useState([]);
  // const [showModal, setShowModal] = useState(false);
  // const [error, setError] = useState('');
  // const { auth } = useAuth();
  // const [currentProject, setCurrentProject] = useState(null);
  // const [selectedTab, setSelectedTab] = useState(0); // Manage active tab state
  // const [selectedFeature, setSelectedFeature] = useState(null); // To pass selected feature to Analysis
  // const [loading, setLoading] = useState(false); // To manage loading state
  // const [bokehPlotData, setBokehPlotData] = useState(null);
  // const [tableData, setTableData] = useState([]); // Store DataTable data
  // const plotRendered = useRef(false); // Ref to track if plot has been rendered
  // const [resultSubTab, setResultSubTab] = useState(0); // Manage subtab state in Results tab
  // const [selectedRows, setSelectedRows] = useState(new Set());
  // const [metadataEnabled, setMetadataEnabled] = useState(false);
  // const [plotLib, setPlotLib] = useState(''); // Store the plot library used
  // const [plotlyData, setPlotlyData] = useState(null); // Store Plotly data
  // const [completedSteps, setCompletedSteps] = useState([]);
  // const [currentStep, setCurrentStep] = useState("Quality Control"); // Initial step


  // useEffect(() => {
  //   if (selectedTab === 3 && resultSubTab === 0 && bokehPlotData) {
  //     if (!plotRendered.current) {
  //       Bokeh.embed.embed_item(bokehPlotData, 'bokeh-container');
  //       plotRendered.current = true; // Mark the plot as rendered
  //     } else {
  //       const container = document.getElementById('bokeh-container');
  //       if (container.children.length === 0) {
  //         Bokeh.embed.embed_item(bokehPlotData, 'bokeh-container');
  //       }
  //     }
  //   }
  // }, [selectedTab, resultSubTab, bokehPlotData]);

  useEffect(() => {
    // Check if the plot needs to be rendered
    if (selectedTab === 3 && resultSubTab === 0 && plotLib === 'bokeh' && bokehPlotData) {
      if (!plotRendered.current) {
        plotRendered.current = true; // Mark plot as rendered
      }
    } else {
      // Reset the rendered state when switching away or updating
      plotRendered.current = false;
    }
  }, [selectedTab, resultSubTab, plotLib, bokehPlotData, plotRendered.current]); // Ensure plotRendered.current is part of the dependencies
  
  
  // Predefined image URLs for data types
  const images = {
    'Metabolomics': '/static/images/metabolomics.png',
    'Single cell data': '/static/images/singlecell.png',
    'Others': '/static/images/other.png',
    'default': '/static/images/other.png'
  };

  const fetchCards = useCallback(async () => {
    try {
      const response = await fetch('http://127.0.0.1:8000/api/get_project_cards/', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${auth.accessToken}`
        }
      });

      if (response.ok) {
        const data = await response.json();
        const formattedCards = data.map(card => ({
          ...card,
          imageUrl: images[card.data_type] || images['default']
        }));
        setCards(formattedCards);
      } else {
        console.error('Failed to fetch project cards');
      }
    } catch (error) {
      console.error('There was an error fetching the project cards!', error);
    }
  }, [auth.accessToken]);

  useEffect(() => {
    if (auth.accessToken) {
      fetchCards();
    }
  }, [auth.accessToken, fetchCards]);

// Unified handler for switching to Analysis tab
const handleSelect = (feature, isAnalysis = false) => {
  if (isAnalysis) {
    setSelectedAnalysisFeature(feature);
  } else {
    setSelectedFeature(feature);
  }
  setSelectedTab(2); // Switch to the "Analysis" tab
};


  const handleVisualizationSelect = async (
    mainCategory,
    subCategory,
    option,
    selectedFiles,
    formParams,
    projectName,
    metadataEnabled,
    dataType
  ) => {
    if (!projectName) {
      console.error("Project name is missing");
      return;
    }
  
    setLoading(true);
  
    const data = {
      selectedFiles: selectedFiles,
      selectedOptions: {
        mainCategory,
        subCategory,
        option,
      },
      metadataEnabled: metadataEnabled,
      params: formParams,
      currentStep: currentStep, // Include currentStep in the request
      nextStep: nextStep, // Include nextStep in the request
    };
  
    try {
      let apiUrl;
      // Determine the API endpoint based on the dataType
      if (dataType === "Metabolomics") {
        console.log("Metabolomics data");
        apiUrl = `http://127.0.0.1:8000/api/process_selection/${projectName}/`;
      } else if (dataType === "Single cell data") {
        console.log("Single cell data");
        apiUrl = `http://127.0.0.1:8000/api/single_cell_process/${projectName}/`;
      } else {
        console.error("Unsupported data type");
        return;
      }
  
      const response = await fetch(apiUrl, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${auth.accessToken}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });
  
      const currentStep = selectedFeature?.option; // Get the current step
      if (response.ok) {
        const result = await response.json();
        console.log("Backend response:", result);
        setSelectedTab(3); // Switch to the Results tab
        setResultSubTab(0); // Default to the Figure subtab
  
        // Specific handling for 'Single cell data'
        if (dataType === "Single cell data") {
          const backendCurrentStep = result.currentStep; // Use the currentStep from the backend
          const backendNextStep = result.nextStep; // Use the nextStep from the backend

          setNextStep(backendNextStep); // Set the next step for the workflow
          setSteps((prevSteps) => {
          if (!prevSteps.includes(backendNextStep)) {
            return [...prevSteps, backendNextStep];
          }
          return prevSteps;
         });
  
          // Ensure that step advancement happens only when necessary
          if (backendNextStep && currentStep !== backendNextStep && backendCurrentStep !== currentStep) {
            // Only advance step if the next step is different from the current step
            console.log("Advancing to the next step2:", backendNextStep);
  
            // setSelectedFeature({
            //   mainCategory: "Preprocessing Type",
            //   subCategory: "Preprocessing",
            //   option: backendNextStep,
            // });
          } else if (backendCurrentStep === currentStep) {
            // Handle parameter updates for the current step without changing the step
            console.log("Updating parameters for the current step:", currentStep);
  

            // setFormData((prev) => ({
            //   ...prev,
            //   [currentStep]: formParams, // Update parameters for the current step
            // }));
          }
        }
  
        // Handling for Metabolomics (or when Bokeh is used)
        handlePlotUpdate(result,setPlotLib, setBokehPlotData, setPlotlyData, setTableData, plotRendered); // Call the new function to handle plot updates
      } else {
        console.error("Failed to process selection");
      }
    } catch (error) {
      console.error("Error processing selection:", error);
    } finally {
      setLoading(false);
    }
  };
  
  
  
  // // New function to handle plot updates and reset rendering  state
  // const handlePlotUpdate = (result) => {
  //   // Handling for Bokeh
  //   if (result.lib === "bokeh") {
  //     setPlotLib("bokeh");
  
  //     if (result.bokehPlot) {
  //       setBokehPlotData(result.bokehPlot);
  //       plotRendered.current = false; // Reset to ensure re-rendering
  //     } else {
  //       console.error("Bokeh plot data missing");
  //     }
  
  //     if (Array.isArray(result.tableData)) {
  //       setTableData(result.tableData);
  //     } else {
  //       console.error("Table data missing or not an array");
  //     }
  //   } 
  //   // Handling for Plotly
  //   else if (result.lib === "plotly") {
  //     setPlotLib("plotly");
  
  //     if (result.plotlyData) {
  //       const plotlyConfig = {
  //         ...result.plotlyData.config,
  //         modeBarButtonsToAdd: [
  //           {
  //             name: "Download SVG",
  //             icon: Plotly.Icons.camera,
  //             click: function (gd) {
  //               Plotly.downloadImage(gd, { format: "svg", filename: "plot" });
  //             },
  //           },
  //         ],
  //         toImageButtonOptions: {
  //           format: "png",
  //           filename: "plot",
  //         },
  //       };
  //       setPlotlyData({
  //         ...result.plotlyData,
  //         config: plotlyConfig,
  //       });
  //     } else {
  //       console.error("Plotly plot data missing");
  //     }
  
  //     if (Array.isArray(result.tableData)) {
  //       setTableData(result.tableData);
  //     } else {
  //       console.error("Table data missing or not an array");
  //     }
  
  //     plotRendered.current = false; // Reset to ensure re-rendering
  //   }
  // };
  
  


  // const handleVisualizationSelect2 = async (
  //   mainCategory,
  //   subCategory,
  //   option,
  //   selectedFiles,
  //   formParams,
  //   projectName,
  //   metadataEnabled,
  //   dataType
  // ) => {
  //   if (!projectName) {
  //     console.error("Project name is missing");
  //     return;
  //   }
  
  //   setLoading(true);
  
  //   const data = {
  //     selectedFiles: selectedFiles,
  //     selectedOptions: {
  //       mainCategory,
  //       subCategory,
  //       option,
  //     },
  //     metadataEnabled: metadataEnabled,
  //     params: formParams,
  //   };
  
  //   try {
  //     let apiUrl;
  //     // Determine the API endpoint based on the dataType
  //     if (dataType === "Metabolomics") {
  //       console.log("Metabolomics data");
  //       apiUrl = `http://127.0.0.1:8000/api/process_selection/${projectName}/`;
  //     } else if (dataType === "Single cell data") {
  //       console.log("Single cell data");
  //       apiUrl = `http://127.0.0.1:8000/api/single_cell_process/${projectName}/`;
  //     } else {
  //       console.error("Unsupported data type");
  //       return;
  //     }
  
  //     const response = await fetch(apiUrl, {
  //       method: "POST",
  //       headers: {
  //         Authorization: `Bearer ${auth.accessToken}`,
  //         "Content-Type": "application/json",
  //       },
  //       body: JSON.stringify(data),
  //     });
  
  //     if (response.ok) {
  //       const result = await response.json();
  //       console.log("Backend response:", result);
  //       setSelectedTab(3); // Switch to the Results tab
  //       setResultSubTab(0); // Default to the Figure subtab
  
  //       // Specific handling for 'Single cell data'
  //       if (dataType === "Single cell data") {
  //         // Update the completed steps with the current step
  //         setCompletedSteps((prevSteps) => [...prevSteps, option]);
  //         console.log("Completed Steps After Update:", completedSteps);
  
  //         // Set the next step as active
  //         if (result.nextStep) {
  //           setNextStep(result.nextStep); // Set the next step for the workflow
  //           setSteps((prevSteps) => {
  //             if (!prevSteps.includes(result.nextStep)) {
  //               return [...prevSteps, result.nextStep];
  //             }
  //             return prevSteps;
  //           });
  //           setSelectedFeature({
  //             mainCategory: "Preprocessing Type",
  //             subCategory: "Preprocessing",
  //             option: result.nextStep,
  //           });
  //         }
  //       }
  
  //       // Handling for Metabolomics (or when Bokeh is used)
  //       if (result.lib === "bokeh") {
  //         setPlotLib("bokeh");
  
  //         if (result.bokehPlot) {
  //           setBokehPlotData(result.bokehPlot);
  //           plotRendered.current = false; 
  //         } else {
  //           console.error("Bokeh plot data missing");
  //         }
  
  //         if (Array.isArray(result.tableData)) {
  //           setTableData(result.tableData);
  //         } else {
  //           console.error("Table data missing or not an array");
  //         }
  
  //         // Ensure plot rendering is triggered correctly
  //         // Reset plotRendered when new data is fetched
  //         // setSelectedTab(3); // Switch to the Results tab
  //         // setResultSubTab(0); // Default to the Figure subtab
  //       } else if (result.lib === "plotly") {
  //         setPlotLib("plotly");
  
  //         if (result.plotlyData) {
  //           const plotlyConfig = {
  //             ...result.plotlyData.config,
  //             modeBarButtonsToAdd: [
  //               {
  //                 name: "Download SVG",
  //                 icon: Plotly.Icons.camera,
  //                 click: function (gd) {
  //                   Plotly.downloadImage(gd, { format: "svg", filename: "plot" });
  //                 },
  //               },
  //             ],
  //             toImageButtonOptions: {
  //               format: "png",
  //               filename: "plot",
  //             },
  //           };
  //           setPlotlyData({
  //             ...result.plotlyData,
  //             config: plotlyConfig,
  //           });
  //         } else {
  //           console.error("Plotly plot data missing");
  //         }
  
  //         if (Array.isArray(result.tableData)) {
  //           setTableData(result.tableData);
  //         } else {
  //           console.error("Table data missing or not an array");
  //         }
  
  //         // Ensure plot rendering is triggered correctly
  //         plotRendered.current = false; // Reset plotRendered when new data is fetched
          
  //       }
  
  //     } else {
  //       console.error("Failed to process selection");
  //     }
  //   } catch (error) {
  //     console.error("Error processing selection:", error);
  //   } finally {
  //     setLoading(false);
  //   }
  // };
  


  // // const handleVisualizationSelect = async (mainCategory, subCategory, option, selectedFiles, formParams, projectName, metadataEnabled) => {
  // //   if (!projectName) {
  // //     console.error("Project name is missing");
  //     return;
  //   }

  //   setLoading(true);

  //   const data = {
  //     selectedFiles: selectedFiles,
  //     selectedOptions: {
  //       mainCategory,
  //       subCategory,
  //       option,
  //     },
  //     metadataEnabled: metadataEnabled,
  //     params: formParams,
  //   };

  //   try {
  //     const response = await fetch(
  //       `http://127.0.0.1:8000/api/process_selection/${projectName}/`,
  //       {
  //         method: "POST",
  //         headers: {
  //           Authorization: `Bearer ${auth.accessToken}`,
  //           "Content-Type": "application/json",
  //         },
  //         body: JSON.stringify(data),
  //       }
  //     );

  //     if (response.ok) {
  //       const result = await response.json();
  //       console.log("Backend response:", result);
  //       setBokehPlotData(result.bokehPlot);
  //       const arrayTableData = Array.from(result.tableData); // Ensure it’s a true array
  //       setTableData(arrayTableData); // Set the state with the true array

  //       plotRendered.current = false; // Reset plotRendered when new data is fetched
  //       setSelectedTab(3); // Switch to the Results tab
  //       setResultSubTab(0); // Default to the Figure subtab
  //     } else {
  //       console.error("Failed to process selection");
  //     }
  //   } catch (error) {
  //     console.error("Error processing selection:", error);
  //   } finally {
  //     setLoading(false);
  //   }
  // };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const isDuplicate = cards.some(card => card.project_name === projectName);
    if (isDuplicate) {
      setError('Project name already exists. Please choose a different name.');
      return;
    }

    const newCard = {
      project_name: projectName,
      project_description: projectDescription,
      data_type: dataType,
      imageUrl: images[dataType] || images['default']
    };

    try {
      const response = await fetch('http://127.0.0.1:8000/api/create_project_card/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${auth.accessToken}`
        },
        body: JSON.stringify({
          project_name: projectName,
          project_description: projectDescription,
          data_type: dataType
        })
      });

      if (response.status === 201) {
        setCards(prevCards => [...prevCards, newCard]);
        setProjectName('');
        setProjectDescription('');
        setDataType('');
        setShowModal(false);
        setError('');
      } else {
        const errorData = await response.json();
        console.error('There was an error creating the project card!', errorData);
      }
    } catch (error) {
      console.error('There was an error creating the project card!', error);
    }
  };

  const handleOpenModal = () => setShowModal(true);
  const handleCloseModal = () => {
    setShowModal(false);
    setError('');
  };

  const handleStart = (projectName, dataType) => {
    setCurrentProject(projectName);
    setDataType(dataType); // Store the data type of the selected project
  };

  // Define your columns with tooltips
  const columns = Object.keys(tableData[0] || {}).map((key) => ({
    name: key,
    selector: row => row[key],
    sortable: true,
    cell: row => (
      <div
        data-tip={row[key]} // Set tooltip content
        style={{
          textOverflow: 'ellipsis',
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          maxWidth: '200px', // Set max width for cells
        }}
      >
        {row[key]}
      </div>
    ),
  }));

  const exportToExcel = () => {
    const ws = XLSX.utils.json_to_sheet(tableData); // Use tableData if there's no filter
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'TableData');
    XLSX.writeFile(wb, 'tableData.xlsx');
  };

  const customStyles = {
    headRow: {
      style: {
        backgroundColor: '#000', // Black background for header
        color: '#fff', // White text color
        fontWeight: 'bold',
      },
    },
    headCells: {
      style: {
        color: '#fff', // White text color for headers
        whiteSpace: 'nowrap', // Prevent text wrapping
      },
    },
    cells: {
      style: {
        padding: '8px',
        textAlign: 'center',
        wordWrap: 'break-word', // Allow text to wrap within cells
        overflow: 'hidden', // Hide overflowed content
        textOverflow: 'ellipsis', // Add ellipsis for overflowed content
        maxWidth: '200px', // Set a maximum column width
        position: 'relative', // Ensure proper positioning for tooltips
      },
    },
  };

  return (
    <div className="container">
      <button className="open-modal-button" onClick={handleOpenModal}>Create Project</button>

      {showModal && (
        <div className="modal">
          <div className="modal-content">
            <span className="close" onClick={handleCloseModal}>&times;</span>
            <h1>Project Creation Form</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-group">
                <label htmlFor="projectName">Project Name</label>
                <input
                  type="text"
                  id="projectName"
                  placeholder="Enter project name"
                  value={projectName}
                  onChange={(e) => setProjectName(e.target.value)}
                />
              </div>

              <div className="form-group">
                <label htmlFor="projectDescription">Project Description</label>
                <textarea
                  id="projectDescription"
                  rows="3"
                  placeholder="Enter project description"
                  value={projectDescription}
                  onChange={(e) => setProjectDescription(e.target.value)}
                />
              </div>

              <div className="form-group">
                <label htmlFor="dataType">Data Type</label>
                <select
                  id="dataType"
                  value={dataType}
                  onChange={(e) => setDataType(e.target.value)}
                >
                  <option value="">Select data type</option>
                  <option value="Metabolomics">Metabolomics</option>
                  <option value="Single cell data">Single cell data</option>
                  <option value="Others">Others</option>
                </select>
              </div>

              {error && <p className="error">{error}</p>}

              <button type="submit">Submit</button>
            </form>
          </div>
        </div>
      )}

      <div className="cards-container">
        {cards.map((card, index) => (
          <div className="card" key={index}>
            <div className="card-header">{card.project_name}</div>
            <img src={card.imageUrl} alt={card.project_name} />
            <div className="card-body">
              <p><strong>Data Type:</strong> {card.data_type}</p>
              <p className="description"><strong>Project Description:</strong> {card.project_description}</p>
              <button className="start-button" onClick={() => handleStart(card.project_name, card.data_type)}>Start</button>
            </div>
          </div>
        ))}
      </div>

      {currentProject && (
        <div className="tabs-container">
          <Tabs selectedIndex={selectedTab} onSelect={(index) => setSelectedTab(index)} forceRenderTabPanel={true} >
            <TabList>
              <Tab>Upload Files</Tab>
              <Tab>Uploaded Files</Tab>
              <Tab>Analysis</Tab>
              <Tab>Results</Tab>
            </TabList>

            <TabPanel>
              <FileUpload projectName={currentProject} />
            </TabPanel>
            <TabPanel>
              <FileList 
                projectName={currentProject} 
                dataType={dataType} // Pass dataType as a prop
                onFeatureSelect={(feature) => handleSelect(feature, false)}
                // onAnalysisFeatureSelect={(feature) => handleSelect(feature, true)}
                selectedRows={selectedRows} 
                setSelectedRows={setSelectedRows} 
                metadataEnabled={metadataEnabled} 
                setMetadataEnabled={setMetadataEnabled} 
                handleVisualizationSelect={handleVisualizationSelect} 
                completedSteps={completedSteps}
                setCompletedSteps={setCompletedSteps}
                currentStep={currentStep}
                setCurrentStep={setCurrentStep}
                // selectedAnalysisFeature={selectedAnalysisFeature}
                // setSelectedAnalysisFeature={setSelectedAnalysisFeature}
              />
            </TabPanel>
            <TabPanel>
              <Analysis 
                projectName={currentProject} 
                dataType={dataType} // Pass dataType as a prop
                selectedFeature={selectedFeature}
                selectedFiles={Array.from(selectedRows)} 
                metadataEnabled={metadataEnabled} 
                handleVisualizationSelect={handleVisualizationSelect} 
                formData={formData} // Pass formData to Analysis for form persistence
                setFormData={setFormData} // Pass setFormData to manage form state
                // selectedAnalysisFeature={selectedAnalysisFeature}
                // setSelectedAnalysisFeature={setSelectedAnalysisFeature}
              />
            </TabPanel>
            <TabPanel>
          <ResultsPanel
            projectName={currentProject}
            resultSubTab={resultSubTab}
            setResultSubTab={setResultSubTab}
            loading={loading}
            plotLib={plotLib}
            bokehPlotData={bokehPlotData}
            plotlyData={plotlyData}
            tableData={tableData}
            columns={columns}
            exportToExcel={exportToExcel}
            customStyles={customStyles}
          />
        </TabPanel>
            
          </Tabs>
        </div>
      )}
    </div>
  );
};

export default DataImport;
