import React, { useState } from "react";
import useAuth from "../hooks/useAuth";
import "./FileUpload.css";

const FileUpload = ({ projectName }) => {
  const { auth } = useAuth();
  const [selectedFiles, setSelectedFiles] = useState([]);
  const [uploadProgress, setUploadProgress] = useState({});
  const [uploadStatus, setUploadStatus] = useState("");

  const handleFileChange = (event) => {
    setSelectedFiles(event.target.files);
    setUploadProgress({});
    setUploadStatus("");
  };

  const handleUpload = () => {
    if (!selectedFiles.length) {
      setUploadStatus("No files selected");
      return;
    }

    const formData = new FormData();
    for (let i = 0; i < selectedFiles.length; i++) {
      formData.append("files", selectedFiles[i]);
    }

    const xhr = new XMLHttpRequest();
    xhr.open(
      "POST",
      `http://127.0.0.1:8000/api/upload_files/${projectName}/`,
      true
    );
    xhr.setRequestHeader("Authorization", `Bearer ${auth.accessToken}`);

    xhr.upload.onprogress = (event) => {
      if (event.lengthComputable) {
        const percentage = Math.floor((event.loaded / event.total) * 100);
        const newProgress = {};
        for (let i = 0; i < selectedFiles.length; i++) {
          newProgress[selectedFiles[i].name] = percentage;
        }
        setUploadProgress(newProgress);
      }
    };

    xhr.onload = () => {
      if (xhr.status === 201) {
        setUploadStatus("Files uploaded successfully");
        // You can trigger a refetch here if needed
        // setShouldRefetch(true); // Trigger a refetch in FileList
      } else {
        setUploadStatus("Failed to upload files");
      }
    };

    xhr.onerror = () => {
      setUploadStatus("Error uploading files");
    };

    xhr.send(formData);
  };

  return (
    <div className="file-upload-container">
      <h2>Upload Files for {projectName}</h2>
      <input type="file" multiple onChange={handleFileChange} />
      <button onClick={handleUpload}>Upload</button>
      <p>{uploadStatus}</p>
      {Array.from(selectedFiles).map((file) => (
        <div key={file.name}>
          <span>{file.name}</span>
          <progress value={uploadProgress[file.name] || 0} max="100" />
          <span>{uploadProgress[file.name] || 0}%</span>
        </div>
      ))}

      {/* Add a download link for the sample Excel file */}
      <div className="download-sample">
        <a href="/static/data/sample.xlsx" download className="download-link">
          Download Sample Excel File
        </a>
      </div>
    </div>
  );
};

export default FileUpload;
