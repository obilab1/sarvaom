import React, { useEffect } from 'react';
import InputForm from './InputForm';
import TargatedForm from './TargatedForm';
import QualityControlForm from './QualityControlForm';
import FilterLowQualityCells from './FilterLowQualityCells';
import DoubletDetectionForm from './DoubletDetectionForm';
import NormalizationForm from './NormalizationForm';
import FeatureSelectionForm from './FeatureSelectionForm';
import DimensionalityReductionForm from './DimensionalityReductionForm';
import UMAPForm from './UMAPForm';

const DynamicFormRenderer = ({ selectedFeature, onSubmit, handleVisualizationSelect, currentStep, dataType }) => {
    useEffect(() => {
        console.log("Selected Feature:", selectedFeature);
        console.log("Current Step:", currentStep);
        console.log("Data Type:", dataType);
    }, [selectedFeature, currentStep, dataType]);

    // Function to determine the component to render
    const getComponentForFeature = () => {
        // Handle components specific to "Single cell data"
        if (dataType === "Single cell data") {
            // Check if we are dealing with UMAP analysis
            if (selectedFeature) {
                const featurePath = `${selectedFeature.mainCategory} > ${selectedFeature.subCategory} > ${selectedFeature.option}`;
                
                if (featurePath === 'Analysis > Analysis Type > UMAP') {
                    return UMAPForm;
                }
            }

            // Handle other steps for "Single cell data"
            switch (currentStep) {
                case 'Quality Control':
                    return QualityControlForm;
                case 'Filter Cells & Genes':
                    return FilterLowQualityCells;
                case 'Doublet Detection':
                    return DoubletDetectionForm;
                case 'Normalization':
                    return NormalizationForm;
                case 'Feature Selection':
                    return FeatureSelectionForm;
                case "Dimensionality Reduction":
                    return DimensionalityReductionForm;
                default:
                    console.warn(`No component found for Single cell data step: ${currentStep}`);
                    return null;
            }
        }

        // Handle cases where selectedFeature is required
        if (!selectedFeature) return null;

        const featurePath = `${selectedFeature.mainCategory} > ${selectedFeature.subCategory} > ${selectedFeature.option}`;

        // Handle other feature paths based on selectedFeature
        switch (featurePath) {
            case 'LC-MS > Untargeted > Feature Detection':
                return InputForm;
            case 'LC-MS > Targeted > Feature Detection':
                return TargatedForm;
            case 'Analysis > Analysis Type > UMAP':
                return UMAPForm; // This line will be reached only if dataType is not "Single cell data"
            // Add more cases as needed
            default:
                console.warn(`No component found for feature path: ${featurePath}`);
                return null;
        }
    };

    // Determine the component to render
    const ComponentToRender = getComponentForFeature();

    return (
        <div className="dynamic-form-container">
            {ComponentToRender ? (
                <ComponentToRender
                    selectedFeature={selectedFeature}
                    onSubmit={onSubmit}
                    handleVisualizationSelect={handleVisualizationSelect}
                />
            ) : (
                <p>No additional input required for this selection.</p>
            )}
        </div>
    );
};

export default DynamicFormRenderer;
