import React, { useState } from 'react';
import { NavLink, Routes, Route } from 'react-router-dom';
import { Navbar, Nav, Container, Row, Col, Button } from 'react-bootstrap';
import { FaHome, FaUserLock, FaArrowRight, FaChartLine, FaCompass, FaSignOutAlt, FaChevronRight, FaChevronLeft, FaChevronDown } from 'react-icons/fa';
import './Sidebar.css';
import logo from '../assets/logo3.png';

import Home from './Home';
import DataImport from './DataImport';
import Analytics from './Analytics';
import Explore from './Explore';
import Login from './Login';
import Register from './Register';
import RequireAuth from './RequireAuth';
import Unauthorized from './Unauthorized';
import Admin from './Admin';
import Logout from './Logout';

const ROLES = {
  'User': 'User',
  'Editor': 'Editor',
  'Admin': 'Admin'
};

const Sidebar = () => {
  const [isSidebarCollapsed, setIsSidebarCollapsed] = useState(false);

  const handleSidebarToggle = () => {
    setIsSidebarCollapsed(!isSidebarCollapsed);
  };

  return (
    <Container fluid className="d-flex flex-column min-vh-100">
      <Row className="flex-grow-1">
        <Col xs={12} md={isSidebarCollapsed ? 1 : 3} lg={isSidebarCollapsed ? 1 : 2} className={`bg-dark min-vh-100 p-0 sidebar-wrapper d-flex flex-column ${isSidebarCollapsed ? 'collapsed' : ''}`}>
          <Navbar bg="dark" variant="dark" expand="md" className="flex-md-column flex-row align-items-start py-2">
            <div className="d-flex w-100 justify-content-between align-items-center px-3">
              <div className="d-flex align-items-center">
                <img src={logo} alt="Logo" className="sidebar-logo" width="80" />
                {!isSidebarCollapsed && (
                  <Navbar.Brand href="/" className="text-decoration-none d-none d-md-block" style={{ color: 'inherit' }}>
                    My Brand
                  </Navbar.Brand>
                )}
              </div>
              <Button variant="link" className="text-white sidebar-toggle-button" onClick={handleSidebarToggle}>
                {isSidebarCollapsed ? <FaChevronRight /> : <FaChevronLeft />}
              </Button>
            </div>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="flex-column w-100 sidebar-content">
                <Nav.Link as={NavLink} to="/" className={({ isActive }) => (isActive ? 'activeClicked' : '')}>
                  <FaHome /> {!isSidebarCollapsed && 'Home'}
                </Nav.Link>
                <Nav.Link as={NavLink} to="/auth" className={({ isActive }) => (isActive ? 'activeClicked' : '')}>
                  <FaUserLock /> {!isSidebarCollapsed && 'Authentication'}
                </Nav.Link>
                <Nav.Link as={NavLink} to="/logout" className={({ isActive }) => (isActive ? 'activeClicked' : '')}>
                  <FaSignOutAlt /> {!isSidebarCollapsed && 'Logout'}
                </Nav.Link>
                <div className="custom-selection">
                  <div className="custom-selection-toggle d-flex justify-content-between">
                    <span>{!isSidebarCollapsed && 'Dashboard'}</span> <FaChevronDown />
                  </div>
                  <div className={`custom-selection-menu ${isSidebarCollapsed ? 'collapsed' : ''}`}>
                    <Nav.Link as={NavLink} to="/import" className={({ isActive }) => (isActive ? 'activeClicked' : '')}>
                      <FaArrowRight /> {!isSidebarCollapsed && 'Data Import'}
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/analytics" className={({ isActive }) => (isActive ? 'activeClicked' : '')}>
                      <FaChartLine /> {!isSidebarCollapsed && 'Analytics'}
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/explore" className={({ isActive }) => (isActive ? 'activeClicked' : '')}>
                      <FaCompass /> {!isSidebarCollapsed && 'Explore'}
                    </Nav.Link>
                  </div>
                </div>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          <footer className="mt-auto bg-dark text-white text-center py-3">
            <p>&copy; 2024 My Brand. All rights reserved.</p>
          </footer>
        </Col>
        <Col xs={12} md={isSidebarCollapsed ? 11 : 9} lg={isSidebarCollapsed ? 11 : 10} className="p-4">
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="auth" element={<Login />} />
            <Route path="register" element={<Register />} />
            <Route path="unauthorized" element={<Unauthorized />} />
            <Route path="logout" element={<Logout />} />
            <Route element={<RequireAuth allowedRoles={[ROLES.User, ROLES.Admin, ROLES.Editor]} />}>
              <Route path="analytics" element={<Analytics />} />
              <Route path="explore" element={<Explore />} />
              <Route path="import" element={<DataImport />} />
            </Route>
            <Route element={<RequireAuth allowedRoles={[ROLES.Admin]} />}>
              <Route path="admin" element={<Admin />} />
            </Route>
          </Routes>
        </Col>
      </Row>
    </Container>
  );
};

export default Sidebar;
