import React, { useState } from 'react';

const FilterLowQualityCells = ({ onSubmit }) => {
    const [params, setParams] = useState({
        minGenes: 100,
        minCells: 3
    });

    // Handle input field changes
    const handleChange = (e) => {
        const { name, value } = e.target;
        setParams({
            ...params,
            [name]: parseFloat(value)
        });
    };

    // Handle form submission
    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(params); // Submit the form parameters
    };

    return (
        <div className="input-form-container">
            <h2 className="input-form-header" style={{ fontSize: '16px' }}>
                Quality Control
            </h2>
            <form onSubmit={handleSubmit}>
                <div className="input-group">
                    <label>
                        Filter cells (minimum number of genes):
                        <input
                            type="number"
                            step="any"
                            name="minGenes"
                            placeholder="100"
                            value={params.minGenes}
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <div className="input-group">
                    <label>
                        Filter genes (minimum number of cells):
                        <input
                            type="number"
                            step="any"
                            name="minCells"
                            placeholder="3"
                            value={params.minCells}
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <button type="submit" className="submit-button">Next</button>
            </form>
        </div>
    );
};

export default FilterLowQualityCells;
