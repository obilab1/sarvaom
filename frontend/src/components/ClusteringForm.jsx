import React, { useState } from 'react';

const ClusteringForm = ({ onSubmit }) => {
    const [params, setParams] = useState({
        resolution: 1.0,     // Default value for resolution as a float
        flavor: 'leidenalg', // Default value for flavor
        n_iterations: -1,    // Default value for n_iterations
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setParams({
            ...params,
            [name]: name === 'n_iterations' ? parseInt(value) : name === 'resolution' ? parseFloat(value) : value,
            // Convert 'n_iterations' to an integer and 'resolution' to a float
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('Submitting form with params:', params); // Debug log to check form data
        onSubmit(params); // Call the onSubmit function passed from parent
    };

    return (
        <div className="input-form-container">
            <h2 className="input-form-header" style={{ fontSize: '16px' }}>
                Clustering
            </h2>
            <form onSubmit={handleSubmit}>
                <div className="input-group">
                    <label>
                        Resolution:
                        <input
                            type="number"
                            step="0.01" // Allows float values with two decimal places
                            name="resolution"
                            value={params.resolution}
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <div className="input-group">
                    <label>
                        Flavor:
                        <select
                            name="flavor"
                            value={params.flavor}
                            onChange={handleChange}
                        >
                            <option value="leidenalg">Leidenalg</option>
                            <option value="igraph">Igraph</option>
                        </select>
                    </label>
                </div>
                <div className="input-group">
                    <label>
                        Number of Iterations (n_iterations):
                        <input
                            type="number"
                            name="n_iterations"
                            value={params.n_iterations}
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <button type="submit" className="submit-button">Next</button>
            </form>
        </div>
    );
};

export default ClusteringForm;
