import React, { useState } from 'react';

const DimensionalityReductionForm = ({ onSubmit }) => {
  // State to handle the number of components
  const [params, setParams] = useState({
    n_comps: 50, // Default value for number of components
  });

  // Handle input field changes
  const handleChange = (e) => {
    const { name, value } = e.target;
    setParams({
      ...params,
      [name]: parseInt(value, 10), // Convert input to integer
    });
  };

  // Handle form submission
  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit(params); // Submit the form parameters
  };

  return (
    <div className="input-form-container">
      <h2 className="input-form-header" style={{ fontSize: '16px' }}>
        Dimensionality Reduction
      </h2>
      <form onSubmit={handleSubmit}>
        <div className="input-group">
          <label>
            Number of Components (n_comps):
            <input
              type="number"
              name="n_comps"
              value={params.n_comps}
              onChange={handleChange}
              min="1"
              placeholder="50"
            />
          </label>
        </div>
        <button type="submit" className="submit-button">
          Next
        </button>
      </form>
    </div>
  );
};

export default DimensionalityReductionForm;
