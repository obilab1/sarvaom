import React from 'react';

const Analytics = () => {
  return (
    <div>
      <h1>Analytics</h1>
      <p>Welcome to the Analytics page!</p>
    </div>
  );
};

export default Analytics;