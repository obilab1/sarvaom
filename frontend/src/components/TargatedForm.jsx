import React, { useState } from 'react';

const TargatedForm = ({ selectedFeature, onSubmit, handleVisualizationSelect }) => {
    const defaultParams = {
        minIntensity: '0.0',
        massTolerance: '0.0',
        mzValues: '',
        algorithm: 'algorithm1'
    };

    const [params, setParams] = useState(defaultParams);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setParams({
            ...params,
            [name]: value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(params);

        // Call handleVisualizationSelect after form submission
        if (handleVisualizationSelect) {
            handleVisualizationSelect(selectedFeature.mainCategory, selectedFeature.option);
        }
    };

    return (
        <div className="input-form-container">
            <h2 className="input-form-header" style={{ fontSize: '16px' }}>
                {`Selected Feature: ${selectedFeature.mainCategory} > ${selectedFeature.subCategory} > ${selectedFeature.option}`}
            </h2>
            <form onSubmit={handleSubmit}>
                <div className="input-group">
                    <label>
                        Minimum Intensity:
                        <input
                            type="number"
                            step="1000.0"
                            name="minIntensity"
                            value={params.minIntensity}
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <div className="input-group">
                    <label>
                        Mass Tolerance:
                        <input
                            type="number"
                            step="0.01"
                            name="massTolerance"
                            value={params.massTolerance}
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <div className="input-group">
                    <label>
                        MZ Values (Newline separated):
                        <textarea
                            name="mzValues"
                            value={params.mzValues}
                            onChange={handleChange}
                            placeholder="Enter mz values, each on a new line"
                            rows="5"
                            style={{ width: '100%' }}
                        />
                    </label>
                </div>
                <div className="input-group">
                    <label>
                        Algorithm:
                        <select
                            name="algorithm"
                            value={params.algorithm}
                            onChange={handleChange}
                        >
                            <option value="algorithm1">Default</option>
                            <option value="algorithm2">FeatureFinderAlgorithmMetaboIdent</option>
                        </select>
                    </label>
                </div>
                <button type="submit" className="submit-button">Next</button>
            </form>
        </div>
    );
};

export default TargatedForm;
