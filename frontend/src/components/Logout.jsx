
import React from 'react';
import { useNavigate } from 'react-router-dom';
import useAuth from '../hooks/useAuth';
import { FaSignOutAlt } from 'react-icons/fa'; // Assuming you want to use the icon

const Logout = () => {
  const { setAuth } = useAuth();
  const navigate = useNavigate();
  
  const handleLogout = () => {
    // console.log('logout is called !!'); // Debugging line to ensure the function is called
    // setAuth({ user: null, pwd: null, roles: [], accessToken: null });
    setAuth({});
    localStorage.removeItem('auth');

    // localStorage.removeItem('authTokens');
    navigate('/auth');

  };

  return (
    <button onClick={handleLogout}>
      <FaSignOutAlt /> Logout
    </button>
  );
};

export default Logout;


