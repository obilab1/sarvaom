// UMAPForm.jsx
import React, { useState } from 'react';

const UMAPForm = ({ onSubmit, setActiveTab }) => {
  // State to store form input values
  const [params, setParams] = useState({
    min_dist: 0.5, // Default value for min_dist
    n_neighbors: 15, // Default value for n_neighbors
    n_pcs: null, // Default value for n_pcs
  });

  // Handle input field changes
  const handleChange = (e) => {
    const { name, value } = e.target;
    setParams({
      ...params,
      [name]: name === "n_pcs" && value === "" ? null : parseFloat(value), // Ensure n_pcs can be set to None
    });
  };

    // Handle form submission
    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(params); // Submit the form parameters
    };

  return (
    <div className="input-form-container">
      <h2 className="input-form-header" style={{ fontSize: '16px' }}>
        UMAP Configuration
      </h2>
      <form onSubmit={handleSubmit}>
        <div className="input-group">
          <label>
            Minimum Distance (min_dist):
            <input
              type="number"
              step="any"
              name="min_dist"
              placeholder="0.5"
              value={params.min_dist}
              onChange={handleChange}
            />
          </label>
          <p className="input-description">
            The effective minimum distance between embedded points. Smaller values result in a more clustered embedding, while larger values provide a more even dispersal of points.
          </p>
        </div>
        <div className="input-group">
          <label>
            Number of Neighbors (n_neighbors):
            <input
              type="number"
              step="1"
              name="n_neighbors"
              placeholder="15"
              value={params.n_neighbors}
              onChange={handleChange}
            />
          </label>
          <p className="input-description">
            The size of the local neighborhood (in terms of the number of neighboring data points) used for manifold approximation. Larger values result in more global views, while smaller values preserve more local data structures.
          </p>
        </div>
        <div className="input-group">
          <label>
            Number of Principal Components (n_pcs):
            <input
              type="number"
              step="1"
              name="n_pcs"
              placeholder="None"
              value={params.n_pcs ?? ""}
              onChange={handleChange}
            />
          </label>
          <p className="input-description">
            Use this many PCs. If <code>n_pcs</code> is set to 0, use the original data in <code>.X</code> if <code>use_rep</code> is None.
          </p>
        </div>
        <button type="submit" className="submit-button">Next</button>
      </form>
    </div>
  );
};

export default UMAPForm;
