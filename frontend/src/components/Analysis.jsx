import React, { useEffect } from 'react';
import DynamicFormRenderer from './DynamicFormRenderer';
import { useSharedState } from './StateContext';
import './Analysis.css';

const Analysis = ({ 
  projectName, 
  handleVisualizationSelect, 
  selectedFiles, 
  metadataEnabled, 
  dataType 
}) => {
  const { 
    currentStepIndex, 
    steps, 
    formData, 
    setFormData, 
    isFormSubmitted, 
    setIsFormSubmitted,
    setCurrentStepIndex,
    selectedFeature 
  } = useSharedState();

  // Determine the current step based on the current step index or selectedFeature.option
  const effectiveCurrentStep = steps[currentStepIndex] || selectedFeature?.option;
  console.log('Current Step:', effectiveCurrentStep, selectedFeature);

  useEffect(() => {
    if (effectiveCurrentStep && !formData[effectiveCurrentStep]) {
      setFormData((prevData) => ({
        ...prevData,
        [effectiveCurrentStep]: {}, // Initialize with empty data if not set
      }));
    }
  }, [effectiveCurrentStep, formData, setFormData]);

  const handleSubmit = (params) => {
    console.log("Form submitted with params:", params);

    // Update form data in shared state
    setFormData((prevData) => ({
      ...prevData,
      [effectiveCurrentStep]: params,
    }));

    setIsFormSubmitted(true); // Mark the form as submitted
    
    // Check if mainCategory and subCategory are provided, if not, set default values
    const mainCategory = selectedFeature?.mainCategory || "Preprocessing";
    const subCategory = selectedFeature?.subCategory || "Preprocessing Type";
    const option = selectedFeature?.option || effectiveCurrentStep;
   

    // Call handleVisualizationSelect to handle visualization logic
    handleVisualizationSelect(
      mainCategory,
      subCategory,
      option,
      selectedFiles, 
      params,
      projectName,
      metadataEnabled,
      dataType
    );
  };

  // Navigate to the next step
  const handleNextStep = () => {
    if (currentStepIndex < steps.length - 1) {
      setCurrentStepIndex(currentStepIndex + 1);
      setIsFormSubmitted(false); // Reset the submission state for the new step
    }
  };

  // Navigate to the previous step
  const handlePreviousStep = () => {
    if (currentStepIndex > 0) {
      setCurrentStepIndex(currentStepIndex - 1);
      setIsFormSubmitted(false); // Reset the submission state for the new step
    }
  };

  // Skip the current step
  const handleSkipStep = () => {
    if (currentStepIndex < steps.length - 1) {
      setCurrentStepIndex(currentStepIndex + 1);
      setIsFormSubmitted(false); // Reset the submission state for the new step
    }
  };

  // List of options for which "Previous" and "Next" buttons should not be displayed
  // const hideNavigationForOptions = ['Clustering', 'Doublet Detection', 'UMAP']; // Add the options where you want to hide the buttons

  // Determine if navigation buttons should be hidden
  const showNavigationButtons = !selectedFeature || selectedFeature?.mainCategory === "Preprocessing";


  console.log('Selected Feature Option:', selectedFeature?.option);
 
  console.log('Selected Feature Main Category:', selectedFeature?.mainCategory);
  console.log('Hide Navigation Buttons:', showNavigationButtons);

  // Check if the main category is "Analysis"
  // const isAnalysisCategory = selectedFeature?.mainCategory === "Analysis";

  // Check if there's a valid step to display
  if (!effectiveCurrentStep) {
    return <p>Please select a feature to start analysis.</p>;
  }

  return (
    <div className="analysis-container">
      <h2>Analysis for {effectiveCurrentStep}</h2>

      {/* Show a message if the main category is "Analysis" */}
      {selectedFeature?.mainCategory === "Analysis"  ? (
        <>
        <p>You need to complete preprocessing steps before proceeding with analysis.</p>
        {(effectiveCurrentStep || selectedFeature?.option) && (
            <DynamicFormRenderer
              currentStep={effectiveCurrentStep}  // Pass currentStep or selectedFeature.option
              onSubmit={handleSubmit} 
              dataType={dataType} // Pass the data type explicitly
            />
          )}
        </>
        
      ) : (
        <>
          {/* Render the step progress indicator */}
          <div className="step-progress">
            {steps.map((step, index) => (
              <div
                key={step}
                className={`step-item ${index === currentStepIndex ? 'current-step' : ''}`}
              >
                {step}
              </div>
            ))}
          </div>

          {/* Render the form based on the current step or selected feature option */}
          {(effectiveCurrentStep || selectedFeature?.option) && (
            <DynamicFormRenderer
              currentStep={effectiveCurrentStep}  // Pass currentStep or selectedFeature.option
              onSubmit={handleSubmit} 
              dataType={dataType} // Pass the data type explicitly
            />
          )}

                  {/* Navigation controls */}
        {showNavigationButtons && (
          <div className="navigation-buttons">
            {currentStepIndex > 0 && (
              <button className="previous-next-button" onClick={handlePreviousStep}>Previous Step</button>
            )}
            {currentStepIndex < steps.length - 1 && (
              <button className="previous-next-button" onClick={handleNextStep}>Next Step</button>
            )}
            {effectiveCurrentStep === 'Doublet Detection' && (
              <button className="skip-button" onClick={handleSkipStep}>Skip Step</button>
            )}
          </div>
        )}
        </>
      )}
    </div>
  );
};

export default Analysis;
