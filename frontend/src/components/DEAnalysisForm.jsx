import React, { useState } from 'react';

const DEAnalysisForm = ({ onSubmit }) => {
    const [params, setParams] = useState({
        method: 't-test', // Default method
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setParams({
            ...params,
            [name]: value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('Submitting form with params:', params); // Debug log to check form data
        onSubmit(params); // Call the onSubmit function passed from parent
    };

    return (
        <div className="input-form-container">
            <h2 className="input-form-header" style={{ fontSize: '16px' }}>
                Differential Expression Analysis
            </h2>
            <form onSubmit={handleSubmit}>
                <div className="input-group">
                    <label>
                        Method:
                        <select
                            name="method"
                            value={params.method}
                            onChange={handleChange}
                            className="method-dropdown"
                        >
                            <option value="t-test">t-test</option>
                            <option value="t-test_overestim_var">t-test overestimating variance</option>
                            <option value="wilcoxon">Wilcoxon rank-sum</option>
                            <option value="logreg">Logistic regression</option>
                        </select>
                    </label>
                </div>
                <button type="submit" className="submit-button">Next</button>
            </form>
        </div>
    );
};

export default DEAnalysisForm;
