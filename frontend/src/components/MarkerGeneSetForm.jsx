import React, { useState } from 'react';

const MarkerGeneSetForm = ({ onSubmit }) => {
    // Default genes separated by new lines
    const [genes, setGenes] = useState(`NOC2L\nAGRN\nPUSL1`);

    const handleChange = (e) => {
        setGenes(e.target.value); // Update state with the current text area value
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        // Split the textarea value by new lines and remove any empty lines
        const geneList = genes.split('\n').map(gene => gene.trim()).filter(gene => gene !== '');
        console.log('Submitting gene list:', geneList); // Debug log to check the list of genes
        onSubmit(geneList); // Call the onSubmit function passed from the parent with the gene list
    };

    return (
        <div className="input-form-container">
            <h2 className="input-form-header" style={{ fontSize: '16px' }}>
                Marker Gene Set
            </h2>
            <form onSubmit={handleSubmit}>
                <div className="input-group">
                    <label>
                        Genes (one per line):
                        <textarea
                            rows="10"
                            cols="30"
                            name="genes"
                            value={genes}
                            onChange={handleChange}
                            placeholder="Enter genes, one per line"
                            className="gene-textarea"
                        />
                    </label>
                </div>
                <button type="submit" className="submit-button">Next</button>
            </form>
        </div>
    );
};

export default MarkerGeneSetForm;
