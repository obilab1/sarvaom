// ResultsPanel.jsx
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Plot from 'react-plotly.js';
import { CSVLink } from 'react-csv';
import DataTable from 'react-data-table-component';
import ReactTooltip from 'react-tooltip';
import BokehPlot from './BokehPlot';
import GeneDropdown from './GeneDropdown';
import { useSharedState } from './StateContext';
import { useEffect, useState, useRef } from 'react';
import { handlePlotUpdate } from '../utils/plotUtils'; 

const ResultsPanel = ({
  projectName,
  resultSubTab,
  setResultSubTab,
  loading,
  plotLib,
  bokehPlotData,
  plotlyData,
  tableData,
  columns,
  exportToExcel,
  customStyles
}) => {

const { 
    currentStep, 
    auth,
    setPlotLib, 
    setBokehPlotData,
    setPlotlyData,
    setTableData, 
    plotRendered,
  
  } = useSharedState();
  console.log("currentstep...",currentStep)

  // Define the state and handler for gene selection
  const [genes, setGenes] = useState(['NOC2L', 'Gene1', 'Gene2', 'Gene3']);
  const [selectedGene, setSelectedGene] = useState('NOC2L'); // Default selected gene
  // const plotRendered = useRef(false); // Ref to track if plot has been rendered

  const [plotInitialized, setPlotInitialized] = useState(false)

  const stepsWithDropdown = ['Clustering','Cluster marker analysis','UMAP']; // List of steps where the dropdown should be shown
  
  // Determine if the dropdown should be shown
  const showGeneDropdown = stepsWithDropdown.includes(currentStep); // Check if the current step is in the list




  const handleGeneSelect = (selectedGene) => {
    console.log('Selected Gene:', selectedGene);
    setSelectedGene(selectedGene);
    fetchGeneFigure(selectedGene); // Fetch data when a gene is selected
    
  };

  // Function to fetch figure data based on gene selection
const fetchGeneFigure = async (selectedGene) => {
  try {
    console.log('fetchGeneFigure is executing');
    const data2 = { selected_gene: selectedGene }; // Payload to specify selected gene

    const apiUrl = `http://127.0.0.1:8000/api/sc_populate_genes/${projectName}/`;

    const response = await fetch(apiUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${auth.accessToken}`,  // Ensure this is the correct token
      },
      body: JSON.stringify(data2),
    });

    if (!response.ok) {
      console.error('Failed to fetch figure data:', response.statusText);
      return;
    }

    const data = await response.json();
    // // Update plotly data state
    // setPlotlyData(data.plotlyData);

    // // Update table data state if provided in response
    // setTableData(data.tablelyData || []);
    

    console.log('Fetched figure data:', data);
    handlePlotUpdate(data,setPlotLib, setBokehPlotData, setPlotlyData, setTableData, plotRendered); 
    // Handle the figure data (e.g., update state to display the figure)
  } catch (error) {
    console.error('Error fetching figure data:', error);
  }
};



  // Function to fetch gene list from the backend API
  const fetchGenes = async () => {
    try {
      console.log('fetchGenes is executing');

      // Dynamically construct the data1 object based on different conditions
    let data1 = {};

    // Example condition: Change data1 based on some component state or props
    if (currentStep === 'UMAP') {
      data1 = { umap: 'genes and parameters' };
    } else if (currentStep === 'Clustering') {
      data1 = { umap: 'genes and parameters' };
    } else {
      data1 = { default: 'default action' }; // Default condition
    }

      const apiUrl = `http://127.0.0.1:8000/api/sc_populate_genes/${projectName}/`;
      
      
      const response = await fetch(apiUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${auth.accessToken}`,  // Ensure this is the correct token
        },
        body: JSON.stringify(data1),
      });
  
      if (!response.ok) {
        console.error('Failed to fetch genes:', response.statusText);
        return;
      }
  
      const data = await response.json();
      console.log(data)
      setGenes(data['genes and parameters'] || []); // Assuming the API returns a JSON object with a "genes" array
    } catch (error) {
      console.error('Error fetching genes:', error);
    }
  };
  

  // const fetchGenes = async () => {
  //   try {
  //     // Simulate fetching genes data from an API or some source
  //     const genes = ['Gene1', 'Gene2', 'Gene3']; // This would be your fetched data
  
  //     // Call setGenes function with the fetched genes
  //     setGenes(genes); 
  //   } catch (error) {
  //     console.error('Error fetching genes:', error);
  //   }
  // };
  // Effect to monitor plot rendering and trigger gene fetching
  useEffect(() => {
    if (projectName && plotInitialized && showGeneDropdown) {
      fetchGenes(); // Call fetchGenes only after the plot is rendered and when dropdown should be shown
    }
  }, [projectName, plotInitialized, showGeneDropdown]); // Include showGeneDropdown in the dependency array

  return (
    <Tabs selectedIndex={resultSubTab} onSelect={(index) => setResultSubTab(index)}>
      <TabList>
        <Tab>Figure</Tab>
        <Tab>Table</Tab>
      </TabList>
      <TabPanel>
        <div id="results-content">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <div>
              {plotLib === 'bokeh' && <BokehPlot plotData={bokehPlotData} />}
              {plotLib === 'plotly' && (
                <>
                  {/* Plotly Plot */}
                  <Plot
                    data={plotlyData.data}
                    layout={plotlyData.layout}
                    config={plotlyData.config}
                    style={{ width: '100%', height: '100%' }}
                    onInitialized={() => setPlotInitialized(true)} // Mark as rendered when initialized
                    onUpdate={() => setPlotInitialized(true)} // Mark as rendered when updated

                  />

                 {/* Dynamic Dropdown - Only show for certain steps */}
                  {showGeneDropdown && (
                    <GeneDropdown genes={genes} onGeneSelect={handleGeneSelect} />
                  )}
                </>
              )}
            </div>
          )}
        </div>
      </TabPanel>
      <TabPanel>
        <div id="table-container">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <div>
              <div style={{ marginBottom: '10px', display: 'flex', gap: '10px' }}>
                <button
                  onClick={() => document.getElementById('csvDownloadLink').click()}
                  className="btn btn-primary"
                  style={{
                    padding: '8px 12px',
                    fontSize: '14px',
                    textAlign: 'center',
                    borderRadius: '4px',
                    lineHeight: '1.5',
                    display: 'inline-block',
                    backgroundColor: '#007bff',
                    color: '#fff',
                    border: '1px solid transparent',
                    cursor: 'pointer',
                  }}
                >
                  Download CSV
                </button>
                <CSVLink
                  data={tableData}
                  filename={'tableData.csv'}
                  id="csvDownloadLink"
                  style={{ display: 'none' }} // Hide the actual CSVLink
                />
                <button
                  onClick={exportToExcel}
                  className="btn btn-success"
                  style={{
                    padding: '8px 12px',
                    fontSize: '14px',
                    textAlign: 'center',
                    borderRadius: '4px',
                    lineHeight: '1.5',
                    display: 'inline-block',
                    backgroundColor: '#28a745',
                    color: '#fff',
                    border: '1px solid transparent',
                    cursor: 'pointer',
                  }}
                >
                  Download Excel
                </button>
              </div>
              {tableData.length > 0 ? (
                <>
                  <DataTable
                    columns={columns}
                    data={tableData}
                    pagination
                    paginationPerPage={30}
                    paginationRowsPerPageOptions={[10, 30, 50, 100, tableData.length]}
                    highlightOnHover
                    dense
                    fixedHeader
                    fixedHeaderScrollHeight="400px"
                    paginationTotalRows={tableData.length}
                    customStyles={customStyles}
                  />
                  <ReactTooltip />
                </>
              ) : (
                <p>No data available</p>
              )}
            </div>
          )}
        </div>
      </TabPanel>
    </Tabs>
  );
};

export default ResultsPanel;
