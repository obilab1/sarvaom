// Analysis.jsx
import React, { useEffect } from 'react';
import DynamicFormRenderer from './DynamicFormRenderer';
import { useSharedState } from './StateContext'; // Import shared state hook
import SCAnalysisForm from './SCAnalysisForm'; // Import the new component

const Analysis = ({ 
  projectName, 
  handleVisualizationSelect, 
  selectedFiles, 
  metadataEnabled, 
  dataType 
}) => {
  // Access shared state
  const { 
    selectedFeature, 
    currentStep, 
    nextStep, 
    setCurrentStepIndex, 
    currentStepIndex, 
    steps, 
    formData, 
    setFormData, 
    isFormSubmitted, 
    setIsFormSubmitted,
    selectedAnalysisFeature,
    setSelectedAnalysisFeature
  } = useSharedState();


  // Determine the current step based on selectedFeature or currentStepIndex
  const effectiveCurrentStep =  steps[currentStepIndex];
  console.log(effectiveCurrentStep)
  console.log(selectedFeature)
  console.log(steps[currentStepIndex])
  console.log(selectedAnalysisFeature)

  // Initialize form data if not already set
useEffect(() => {
  if (currentStep && !formData[currentStep]) {
    setFormData((prevData) => ({
      ...prevData,
      [currentStep]: {}, // Initialize with empty data if not set
    }));
  }
}, [currentStep, formData, setFormData]);

  // Handle form submission
  const handleSubmit = (params) => {
    if (!selectedFeature) return; // Prevent submission if no feature is selected
    console.log("Form submitted with params:", params);
    setFormData((prevData) => ({
      ...prevData,
      [selectedFeature.option]: params, // Save form data in shared state
    }));
    setIsFormSubmitted(true); // Mark the form as submitted
    handleVisualizationSelect(
      selectedFeature.mainCategory,
      selectedFeature.subCategory, 
      selectedFeature.option,
      selectedFiles,
      params,
      projectName,
      metadataEnabled,
      dataType
    );
  };

  // Handle next step confirmation
  const handleNextStep = () => {
    setCurrentStepIndex(currentStepIndex + 1);
    setIsFormSubmitted(false); // Reset form submission status for the next step
  };

  // Handle previous step navigation
  const handlePreviousStep = () => {
    if (currentStepIndex > 0) {
      setCurrentStepIndex(currentStepIndex - 1);
      setIsFormSubmitted(false);
    }
  };

  // Display message if no feature is selected
  if (!selectedFeature && !selectedAnalysisFeature) {
    return <p>Please select a feature to start analysis.</p>;
  }

  return (
    <div className="analysis-container">
      <h2>Analysis for {projectName}</h2>

      {/* Display the selected analysis feature */}
      {selectedAnalysisFeature && (
        <p>Selected Analysis: {selectedAnalysisFeature.option}</p>
      )}
      {/* Render the form for the selected analysis feature */}
      {selectedAnalysisFeature && (
        <SCAnalysisForm 
          selectedAnalysisFeature={selectedAnalysisFeature}
          onSubmit={handleSubmit}
          handleVisualizationSelect={handleVisualizationSelect}
          dataType={dataType} // Pass the dataType to the component
        />
      )}


      {effectiveCurrentStep  && (
              <DynamicFormRenderer
                selectedFeature={selectedFeature}
                formData={formData[selectedFeature.option] || {}} 
                onSubmit={handleSubmit} 
                currentStep={effectiveCurrentStep} 
                dataType={dataType}
              />
            )}



      {selectedFeature && (
        <DynamicFormRenderer
          selectedFeature={selectedFeature}
          formData={formData[selectedFeature.option] || {}} // Use existing form data or empty object
          onSubmit={handleSubmit} // Handle form submission
          currentStep={effectiveCurrentStep} // Use currentStep to drive form behavior
          dataType={dataType}
        />
      )}
      <div className="navigation-buttons">
        {currentStepIndex > 0 && (
          <button onClick={handlePreviousStep}>Previous Step</button>
        )}
        {isFormSubmitted && nextStep && (
          <button className="next-step-button" onClick={handleNextStep}>
            Confirm Next Step
          </button>
        )}
      </div>
    </div>
  );
};

export default Analysis;
