import React, { useState } from 'react';

const InputForm = ({ selectedFeature, onSubmit, handleVisualizationSelect }) => {
    // Default parameters for the form fields
    const defaultParams = {
        minIntensity: '1000.0',
        massTolerance: '0.01',
        algorithm: 'algorithm1'
    };

    const [params, setParams] = useState(defaultParams);

    // Handle input field changes
    const handleChange = (e) => {
        const { name, value } = e.target;
        setParams({
            ...params,
            [name]: value
        });
    };

    // Handle form submission
    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(params); // Submit the form parameters

        // Call handleVisualizationSelect after form submission to trigger visualization
        if (handleVisualizationSelect) {
            handleVisualizationSelect(selectedFeature.mainCategory, selectedFeature.option);
        }
    };

    return (
        <div className="input-form-container">
            <h2 className="input-form-header" style={{ fontSize: '16px' }}>
                {`Selected Feature: ${selectedFeature.mainCategory} > ${selectedFeature.subCategory} > ${selectedFeature.option}`}
            </h2>
            <form onSubmit={handleSubmit}>
                <div className="input-group">
                    <label>
                        Minimum Intensity:
                        <input
                            type="number"
                            step="1000.0"
                            name="minIntensity"
                            value={params.minIntensity}
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <div className="input-group">
                    <label>
                        Mass Tolerance:
                        <input
                            type="number"
                            step="0.01"
                            name="massTolerance"
                            value={params.massTolerance}
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <div className="input-group">
                    <label>
                        Algorithm:
                        <select
                            name="algorithm"
                            value={params.algorithm}
                            onChange={handleChange}
                        >
                            <option value="algorithm1">Default</option>
                            <option value="algorithm2">FeatureFindingMetabo</option>
                        </select>
                    </label>
                </div>
                <button type="submit" className="submit-button">Next</button>
            </form>
        </div>
    );
};

export default InputForm;
