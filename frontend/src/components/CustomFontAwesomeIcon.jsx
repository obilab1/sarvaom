// src/CustomFontAwesomeIcon.js
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const CustomFontAwesomeIcon = ({ icon = 'coffee', ...props }) => {
  return <FontAwesomeIcon icon={icon} {...props} />;
};

export default CustomFontAwesomeIcon;
