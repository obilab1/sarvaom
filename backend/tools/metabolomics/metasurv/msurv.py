from tools.metabolomics.metasurv import tidyms as ms
import os
from bokeh.embed import json_item
import numpy as np
import json
# from tools.metabolomics.metasurv.feature_detector import  MzMLFeatureDetector

class Metasurv:
### list of directories and sample files 
# 1. The path to the raw data files. If all files are inside a directory, the path to the directory can be used and all mzML files in the directory will be used. All files must be in centroid mode.

# 2. An assay directory to store the processed data. All processed data is stored to disk to reduce the memory usage in the different processing stages.

# 3. The sample metadata. The metadata can be provided as a csv file or as a Pandas DataFrame. See the docstring for the format.

# 4. The separation method used in the study and the instrument used, which defines the defaults used in the different preprocessing stages.
    def __init__(self ):
        self.datatype='metabolomics'
                 

    def define_folder(self,sample_metadata_path="sample.csv", 
                 data_path="mzmls",
                 selected_options=None, 
                 params=None,
                 assay_path="processed_Data",
                 separation="uplc", 
                 instrument="qtof"):
        self.sample_metadata_path = sample_metadata_path
        self.data_path = data_path
        self.assay_path = os.path.join(self.data_path, assay_path)
        self.assay_path_pyopenms=os.path.join(self.data_path, 'pyopenms')
        self.separation = separation
        self.instrument = instrument


        if params and (params['algorithm']=='algorithm2'):
            print('pyopenms is executing')
            #
                # Initialize the assay
            self.assay = ms.Assay(
                data_path=self.data_path,
                assay_path=self.assay_path_pyopenms,
                sample_metadata=self.sample_metadata_path,
                separation=self.separation,
                instrument=self.instrument
            )

            make_roi_params = {
            "tolerance": 0.01,
            "min_intensity": 1000,
            }
            
            self.assay.detect_features_pyopenms(verbose=False, **make_roi_params)
        

            # detector=MzMLFeatureDetector(self.assay_pyopenms.manager)


            
            
           
        # process_samples = self.manager.sample_queue
        # n_samples = len(process_samples)

        # if n_jobs is None:
        #     n_jobs = self.n_jobs

        # if n_samples:

        #     def iter_func(sample_list):
        #         for sample in sample_list:
        #             roi_path = self.manager.get_roi_path(sample)
        #             ms_data = self.get_ms_data(sample)
        #             yield roi_path, ms_data

        #     def worker(args):
        #         roi_path, ms_data = args
        #         roi_list = detect_features_func(ms_data, **kwargs)
        #         _save_roi_list(roi_path, roi_list)

        #     worker = delayed(worker)
        #     iterator = iter_func(process_samples)
        #     if verbose:
        #         print("Creating ROI in {} samples".format(n_samples))
        #         bar = get_progress_bar()
        #         iterator = bar(iterator, total=n_samples)

        #     Parallel(n_jobs=n_jobs)(worker(x) for x in iterator)
        # else:
        #     if verbose:
        #         print("All samples are processed already.")

        # return self

        
        # Initialize the assay
        self.assay = ms.Assay(
            data_path=self.data_path,
            assay_path=self.assay_path,
            sample_metadata=self.sample_metadata_path,
            separation=self.separation,
            instrument=self.instrument
        )
        
        mz_tol=0.01
        min_intensity=1000

        mz_list=[]
        algo='algorithm1'
        if params:
            min_intensity=params['minIntensity']
            mz_tol=params['massTolerance']
            algo=params['algorithm']
            if 'mzValues' in params.keys():
                mz_list_str=params['mzValues']
                mz_list_str = mz_list_str.split('\n')

                # Convert each string in the list to a float
                mz_list = [float(value) for value in mz_list_str]

        if selected_options:
            main_cat=selected_options['mainCategory']
            method=selected_options['subCategory']
            option=selected_options['option']


        if main_cat=='LC-MS' and option=='Feature Detection':
            print(algo)
            mz_array=np.array(mz_list)
            
            fig,feature_json=self.feature_detection(method=method,algorithm=algo ,mz_list=mz_array,mz_tol=0.01,min_intensity=1000)
        else:
            fig={}
            feature_json={}

        return fig,feature_json

        

        # parameters adjusted to reduce the number of ROI created
        # to perform untargeted feature detection, remove the `targeted_mz` param
        # mz_list = np.array(
        #  [118.0654, 144.0810, 146.0605, 181.0720, 188.0706,
        # 189.0738, 195.0875, 205.0969]
        # )
        # make_roi_params = {
        # "tolerance": 0.015,
        # "min_intensity": 5000,
        # "targeted_mz": mz_list
        # }

    def feature_detection(self,method='untargeted',algorithm='Default',mz_list=None,mz_tol=0.01,min_intensity=1000):
       
        if (method.lower()=='untargeted') and algorithm=='algorithm1':
            make_roi_params = {
            "tolerance": mz_tol,
            "min_intensity": min_intensity,
            }
        elif (method.lower()=='targeted') and algorithm=='algorithm1':
            make_roi_params = {
            "tolerance": mz_tol,
            "min_intensity": min_intensity,
            "targeted_mz": mz_list
            }
        self.assay.detect_features(verbose=False, **make_roi_params)
        
        # # roi_list = ms.make_roi(self.ms_data, min_intensity=10000)
        # roi_list =self. assay.load_roi_list('20200715_QEF1_HILIC_mouse_liver_AW111')
        
        self.assay.extract_features(store_smoothed=True)

        self.assay.describe_features()
    
        self.assay.build_feature_table()
        # self.assay.match_features()
        feature_df=self.assay.feature_table
        fig=self.assay.plot.roi_svg('20200715_QEF1_HILIC_mouse_liver_AW111',show=False, save_format="svg")
        # Convert DataFrame to JSON
        feature_json = feature_df.to_json(orient='records')
    # Return JSON response
   
        return fig,feature_json
        
    def read_mzml_file(self, file_path):
        """
        Reads an mzML file using tidyms and returns the data.

        :param file_path: Path to the mzML file to read.
        :return: Parsed mzML data.
        """
        try:
            # Load the mzML file using tidyms
            mzml_data = ms.io.load_mzml(file_path)
            return mzml_data
        except Exception as e:
            print(f"Error reading mzML file: {file_path}. Error: {e}")
            return None
