import os
import pyopenms as oms

class MzMLFeatureDetector:
    def __init__(self,assay):
        """
        Initialize the feature detector with given parameters.
        
        Parameters:
            min_intensity (float): Minimum intensity for feature detection.
            max_peak_count (int): Maximum number of peaks to consider.
        """
        self.data_type="Metabolomics"
        self.assay=assay
        import pdb;pdb.set_trace()
        self.process_mzml_files(self.mzmlfiles, self.output, self.input)

        # self.min_intensity = min_intensity
        # self.max_peak_count = max_peak_count
        # self.feature_finder = oms.FeatureFinderCentroided()
        # self._set_parameters()

    def _set_parameters(self):
        """
        Set parameters for the FeatureFinderCentroided algorithm.
        """
        params = oms.Param()
        params.setValue("min_intensity", self.min_intensity, "")
        params.setValue("max_peak_count", self.max_peak_count, "")
        self.feature_finder.setParameters(params)

    def detect_features(self, mzml_file, output_file):
        """
        Detect features in the given mzML file and save results to an output file.
        
        Parameters:
            mzml_file (str): Path to the input mzML file.
            output_file (str): Path to save the output mzML file with detected features.
        """
        # Load the mzML file
        exp = oms.MSExperiment()
        oms.MzMLFile().load(mzml_file, exp)
        
        # Create a new MSExperiment to hold the features
        feature_exp = oms.MSExperiment()
        
        # Process each spectrum to detect features
        for spectrum in exp.getSpectra():
            # Create a new MSSpectrum to store detected features
            feature_spectrum = oms.MSSpectrum()
            
            # Apply feature detection algorithm
            self.feature_finder.getFeatures(spectrum, feature_spectrum)
            
            # Add the detected features to the new MSExperiment
            feature_exp.addSpectrum(feature_spectrum)
        
        # Save the features to the output mzML file
        oms.MzMLFile().store(output_file, feature_exp)
        print(f"Features detected and saved to {output_file}")

    def process_mzml_files(self, mzml_files, output_dir,input_dir):
        """
        Process a list of mzML files to detect features and save results.
        
        Parameters:
            mzml_files (list of str): List of paths to mzML files.
            output_dir (str): Directory to save the output mzML files with detected features.
        """
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        
        for mzml_file in mzml_files:
            file_name = os.path.join(input_dir, mzml_file)
            output_file = os.path.join(output_dir, f"features_{file_name}")
            self.detect_features(mzml_file, output_file)
            print(f"Processed {mzml_file}")

# Example usage
if __name__ == "__main__":
    mzml_files = ["file1.mzML", "file2.mzML"]  # Replace with your mzML file paths
    output_dir = "output_features"  # Replace with your desired output directory

    detector = MzMLFeatureDetector()
    detector.process_mzml_files(mzml_files, folder_path=None)
