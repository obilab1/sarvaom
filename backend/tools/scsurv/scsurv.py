import scanpy as sc
import numpy as np
import plotly.express as px
import pandas as pd
from plotly.subplots import make_subplots
import plotly.graph_objects as go
from scipy.sparse import csr_matrix
import os
import shutil

# class ScSurveAnalysis:
#     def _init_(self, adata, mt_gene_prefix='MT-'):
#         self.adata = adata
#         self.mt_gene_prefix = mt_gene_prefix

    # def calculate_qc_metrics(self):
    #     self.adata.obs['n_genes_by_counts'] = (self.adata.X > 0).sum(axis=1)

    #     self.adata.obs['total_counts'] = self.adata.X.sum(axis=1)

    #     mt_genes = self.adata.var_names.str.startswith(self.mt_gene_prefix)
    #     mt_counts = self.adata[:, mt_genes].X.sum(axis=1)

    #     mt_counts = np.array(mt_counts).flatten()

    #     self.adata.obs['pct_counts_mt'] = (mt_counts / self.adata.obs['total_counts']) * 100

    #     return self.adata

#     def plot_qc_metrics(self):
#         qc_df = self.adata.obs[['n_genes_by_counts', 'total_counts', 'pct_counts_mt']]

#         fig1 = px.violin(qc_df, y='n_genes_by_counts', box=True, points="all", title="n_genes_by_counts")
#         fig2 = px.violin(qc_df, y='total_counts', box=True, points="all", title="total_counts")
#         fig3 = px.violin(qc_df, y='pct_counts_mt', box=True, points="all", title="pct_counts_mt")

#         fig1.show()
#         fig2.show()
#         fig3.show()

# # Example usage:
# # Initialize the analysis class with your AnnData object
# analysis = ScSurveAnalysis(adata)

# # Calculate the QC metrics
# adata = analysis.calculate_qc_metrics()

# # Plot the QC metrics
# analysis.plot_qc_metrics()


class SCUtil:
    def __init__(self,file_path=None,logger=None,prev_data=None,fdata=None):
        self.file_path=file_path
        self.logger=logger
        self.logger.info(f"single cell utility is excuting...:{prev_data}")
   
        ## load the data
        if fdata and ('host' in fdata.keys()) and (fdata['host']=='cellxgene'):
            self.host_file=prev_data
            
        else:

            if prev_data:
                self.adata = sc.read_h5ad(prev_data)
            else:
                self.load_data() 



    def copy_file_with_unique_name(self,original_file_path):
        """
        Copies a file to a dynamically determined target directory with a unique name.

        Args:
        - original_file_path (str): The full path to the original file.
        """
        # Extract the common base directory from the original file path
        base_parts = original_file_path.split(os.sep)[:-4]  # Extract base parts except for the last four
        base_directory = os.sep.join(base_parts)  # Reconstruct the base directory path

        # Define the base target directory
        target_directory = os.path.join(base_directory, 'cellxgene_data')

        # Ensure the target directory exists
        if not os.path.exists(target_directory):
            os.makedirs(target_directory)

        # Extract relevant parts for uniqueness from the original file path
        path_parts = original_file_path.split(os.sep)
        relevant_parts = path_parts[-4:-1]  # For example: ['1', 'SCtest', 'sc_data']
        unique_part = '_'.join(relevant_parts)  # Combine parts with underscores, e.g., '1_SCtest_sc_data'

        # Extract the base name of the original file
        base_name = os.path.basename(original_file_path)  # Extracts 'clustering.h5ad'

        # Create a new file name with the unique tag
        new_file_name = f"{unique_part}_{base_name}"  # Example: '1_SCtest_sc_data_clustering.h5ad'

        # Construct the full path to the new file in the target directory
        new_file_path = os.path.join(target_directory, new_file_name)

            # Check if the file already exists and remove it if it does
        if os.path.exists(new_file_path):
            os.remove(new_file_path)  # Remove the existing file

        # Copy the original file to the new location with the new name
        shutil.copyfile(original_file_path, new_file_path)  # Forcef
        # Construct the link for viewing the file
        link = f"http://127.0.0.1:5005/view/{new_file_name}"

        self.logger.info(f"File copied successfully to: {new_file_path}")
        return {'data_key':'finished','file':new_file_name,'link':link}


    def host_on_cell_x_gene(self):
        ''' copy this data on cellxgene_folder'''
        if self.host_file:
            data=self.copy_file_with_unique_name(self.host_file)
        else:
            self.logger.info(f"No cluster data...")
        return  data
            

    def generate_umap_figure(self,gene_name):
        """
        Generate a UMAP scatter plot colored by gene expression.

        Parameters:
        - adata: AnnData object containing the single-cell data.
       
        - gene_name: Name of the gene to color by.

        Returns:
        - fig: Plotly figure object.
        - df_json: JSON string of the DataFrame containing UMAP coordinates and gene expression.
        """
        # Ensure the gene exists in the data
        adata=self.adata.copy()
        # if gene_name not in adata.var.index:
        #     raise ValueError(f"Gene '{gene_name}' not found in the dataset.")
       
        # Extract gene expression values
        # Extract UMAP coordinates
        umap_1 = adata.obsm['X_umap'][:, 0]  # UMAP1 coordinates
        umap_2 = adata.obsm['X_umap'][:, 1]  # UMAP2 coordinates

        try:
            # Check if gene_name is in adata.obs.columns
            if gene_name in adata.obs.columns:
                # Extract values from adata.obs
                gene_expression = adata.obs[gene_name].values
            else:
                # If gene_name is not in adata.obs.columns, attempt to extract from adata.X
                gene_expression = adata[:, gene_name].X.toarray().flatten()
        except Exception as e:
            # Log the exception with an informative message
            self.logger.info(f"Error occurred while accessing gene '{gene_name}': {e}")
            gene_expression = None  # Set gene_expression to None or handle it appropriately



        # gene_expression = adata[:, gene_name].X.toarray().flatten()

        # Create a scatter plot using Plotly
        
        
        if pd.api.types.is_numeric_dtype(gene_expression):
            fig = go.Figure()

            fig.add_trace(go.Scatter(
                x=umap_1,
                y=umap_2,
                mode='markers',
                marker=dict(
                    size=6,
                    color=gene_expression,  # Color by gene expression
                    colorscale='Viridis',   # Choose a color scale
                    showscale=True,         # Show color scale legend
                    colorbar=dict(title=gene_name)  # Color bar title as gene name
                ),
                name=f'UMAP colored by {gene_name}'
            ))
        else:
            # If categorical, use discrete colors
            unique_categories = sorted(pd.unique(gene_expression), key=lambda x: int(x) if x.isdigit() else x)
            color_map = px.colors.qualitative.Safe  # Choose a suitable color set

            # Create a color dictionary to map categories to colors
            color_dict = {category: color_map[i % len(color_map)] for i, category in enumerate(unique_categories)}

            fig = go.Figure()

            # Add a scatter trace for each unique category
            for category in unique_categories:
                category_mask = gene_expression == category
                fig.add_trace(go.Scatter(
                    x=[umap_1[i] for i in range(len(umap_1)) if category_mask[i]],
                    y=[umap_2[i] for i in range(len(umap_2)) if category_mask[i]],
                    mode='markers',
                    marker=dict(
                        size=6,
                        color=color_dict[category]  # Use the color from the color map
                    ),
                    name=f'Cluster {category}',  # Legend entry for each category
                ))


        # Update layout for the plot
        fig.update_layout(
            title=f'UMAP1 vs UMAP2 colored by {gene_name} expression',
            xaxis_title='UMAP1',
            yaxis_title='UMAP2',
            height=600,
            paper_bgcolor='white',  # Set white background
            plot_bgcolor='white',   # Set white plot background
        )

        # Create DataFrame for UMAP and gene expression
        df_umap = pd.DataFrame({
            'UMAP1': umap_1,
            'UMAP2': umap_2,
            gene_name: gene_expression
        })

        # Convert DataFrame to JSON
        df_json = df_umap.to_json(orient='records')

        return fig, df_json
    



    # def generate_umap_figure(self,gene_name):
    #     """
    #     Generate a UMAP scatter plot colored by gene expression.

    #     Parameters:
    #     - adata: AnnData object containing the single-cell data.
       
    #     - gene_name: Name of the gene to color by.

    #     Returns:
    #     - fig: Plotly figure object.
    #     - df_json: JSON string of the DataFrame containing UMAP coordinates and gene expression.
    #     """
    #     # Ensure the gene exists in the data
    #     adata=self.adata.copy()
    #     # if gene_name not in adata.var.index:
    #     #     raise ValueError(f"Gene '{gene_name}' not found in the dataset.")

    #     # Extract gene expression values
    #     # Extract UMAP coordinates
    #     umap_1 = adata.obsm['X_umap'][:, 0]  # UMAP1 coordinates
    #     umap_2 = adata.obsm['X_umap'][:, 1]  # UMAP2 coordinates

    #     try:
    #         # Check if gene_name is in adata.obs.columns
    #         if gene_name in adata.obs.columns:
    #             # Extract values from adata.obs
    #             gene_expression = adata.obs[gene_name].values
    #         else:
    #             # If gene_name is not in adata.obs.columns, attempt to extract from adata.X
    #             gene_expression = adata[:, gene_name].X.toarray().flatten()
    #     except Exception as e:
    #         # Log the exception with an informative message
    #         self.logger.info(f"Error occurred while accessing gene '{gene_name}': {e}")
    #         gene_expression = None  # Set gene_expression to None or handle it appropriately



    #     # gene_expression = adata[:, gene_name].X.toarray().flatten()

    #     # Create a scatter plot using Plotly
    #     fig = go.Figure()

    #     fig.add_trace(go.Scatter(
    #         x=umap_1,
    #         y=umap_2,
    #         mode='markers',
    #         marker=dict(
    #             size=6,
    #             color=gene_expression,  # Color by gene expression
    #             colorscale='Viridis',   # Choose a color scale
    #             showscale=True,         # Show color scale legend
    #             colorbar=dict(title=gene_name)  # Color bar title as gene name
    #         ),
    #         name=f'UMAP colored by {gene_name}'
    #     ))

    #     # Update layout for the plot
    #     fig.update_layout(
    #         title=f'UMAP1 vs UMAP2 colored by {gene_name} expression',
    #         xaxis_title='UMAP1',
    #         yaxis_title='UMAP2',
    #         height=600,
    #         paper_bgcolor='white',  # Set white background
    #         plot_bgcolor='white',   # Set white plot background
    #     )

    #     # Create DataFrame for UMAP and gene expression
    #     df_umap = pd.DataFrame({
    #         'UMAP1': umap_1,
    #         'UMAP2': umap_2,
    #         gene_name: gene_expression
    #     })

    #     # Convert DataFrame to JSON
    #     df_json = df_umap.to_json(orient='records')

    #     return fig, df_json
        


    def get_genes_parameters(self):
        ''' get all genes and parameters'''
        ### get genes 
        # Retrieve gene names from the AnnData object
        adata = self.adata.copy()
        gene_names = adata.var.index.tolist()

        # Retrieve cell metadata columns from the AnnData object
        cell_metadata_columns = adata.obs.columns.tolist()

        # Combine gene names and cell metadata columns into a single list
        combined_data = gene_names + cell_metadata_columns
        return combined_data
            
class SCSurv:
    def __init__(self,file_path=None,
                selected_options=None,
                params=None,logger=None,prev_data=None):
        self.datatype='Single cell data'
        self.file_path=file_path
        self.options=selected_options
        self.params=params
        self.logger=logger
        self.logger.info(f"testing single cell files : {prev_data}")
        ## load the data
        if prev_data:
            self.adata = sc.read_h5ad(prev_data)
        else:
            self.load_data()
        ## claculate qc matrixes
        
        if ('option' in selected_options.keys()) and (selected_options['option']=='Quality Control') :
                self.calculate_qc_metrics()
        elif ('option' in selected_options.keys()) and (selected_options['option']=='Clustering'):
            if 'analysis.h5ad' in prev_data:
                self.umap_flag=1
            else:
                self.umap_flag=0
        ### plot qc matrix



    def load_data(self):
        """
        load mtx data
        """
        adata = sc.read_10x_mtx(
            self.file_path,               # Path to the directory containing .mtx and other files
            var_names='gene_symbols',  # Use gene symbols for the variable names
            cache=True)                # Cache the result to speed up future reads
        self.adata = adata
        # self.mt_gene_prefix = self.options

    def prepare_dot_plot_data(self, adata, target_genes, clusters):
        """
        Helper function to prepare data for the dot plot.
        """
        dot_plot_data = []

        # Loop through each gene and cluster to calculate metrics for the dot plot
        for gene in target_genes:
            gene_idx = adata.var_names.get_loc(gene)  # Get the index of the gene in adata.var_names

            for cluster in clusters:
                # Get the cells that belong to the current cluster
                cluster_cells = adata.obs.index[adata.obs['leiden'] == cluster]

                # Get the expression values for the current gene in the current cluster
                cluster_expression = adata[cluster_cells, gene_idx].X.toarray().flatten()  # Ensure it is a flat array

                # Compute the percentage of cells expressing the gene in this cluster
                pct_cells_expressing = (cluster_expression > 0).mean() * 100

                # Compute mean expression of the gene in cells of the targeted cluster
                mean_expression = cluster_expression[cluster_expression > 0].mean() if (cluster_expression > 0).sum() > 0 else 0

                dot_plot_data.append({
                    'gene': gene,
                    'cluster': cluster,
                    'pct_cells_expressing': pct_cells_expressing,
                    'mean_expression': mean_expression
                })

        # Convert to DataFrame
        df_dot_plot = pd.DataFrame(dot_plot_data)

        # Normalize mean expression between 0 and 1
        df_dot_plot['scaled_expression'] = (df_dot_plot['mean_expression'] - df_dot_plot['mean_expression'].min()) / (df_dot_plot['mean_expression'].max() - df_dot_plot['mean_expression'].min())

        return df_dot_plot
    
    def create_dot_plot(self, df_dot_plot, title):
        """
        Helper function to create a dot plot using Plotly with optional gene annotations.
        """
        # Determine the number of genes and clusters
        n_genes = len(df_dot_plot['gene'].unique())
        n_clusters = len(df_dot_plot['cluster'].unique())

        # Define minimum and maximum width and height
        min_width = 400
        max_width = 1000  # Set a maximum width to prevent the plot from becoming too wide
        min_height = 400

        # Scale width and height dynamically
        width = min(max_width, min_width + n_genes * 40)  # Ensure width is between min_width and max_width
        height = min_height + n_clusters * 40  # Increase height for each cluster

        # Check if 'gene_origin_cluster' is available in df_dot_plot
        hover_data = {}
        if 'gene_origin_cluster' in df_dot_plot.columns:
            hover_data['gene_origin_cluster'] = True  # Include origin cluster in hover data
            text_annotation = 'gene_origin_cluster'  # Annotate with origin cluster
        else:
            text_annotation = None  # No annotation if data is not available

        # Create the dot plot using Plotly
        fig = px.scatter(
            df_dot_plot,
            x='gene',
            y='cluster',
            size='pct_cells_expressing',
            color='scaled_expression',
            color_continuous_scale='Reds',
            labels={
                'pct_cells_expressing': '% of cells expressing',
                'scaled_expression': 'Scaled Mean Expression'
            },
            title=title,
            width=width,  # Set dynamic width
            height=height,  # Set dynamic height
            hover_data=hover_data,  # Include optional hover data
            # text=text_annotation  # Annotate with text if available
        )

        # Customize plot appearance
        fig.update_traces(textposition='top center' if text_annotation else None)  # Position text annotations
        fig.update_layout(
            paper_bgcolor='white',  # White background
            plot_bgcolor='white',   # White plot background
            xaxis=dict(
                showline=True,
                linecolor='black',  # Black axis line
                tickfont=dict(color='black')  # Black tick marks
            ),
            yaxis=dict(
                showline=True,
                linecolor='black',  # Black axis line
                tickfont=dict(color='black')  # Black tick marks
            ),
            coloraxis_colorbar=dict(
            title='Scaled Mean Expression',
            orientation='v',  # Vertical color bar
            x=1.05,  # Position to the right
            y=1,  # Align to the top
            len=0.5  # Half-length color bar
        ),
        legend=dict(
            x=1.05,  # Align size legend to the right
            y=0.5,  # Position size legend below color bar
            xanchor='left',
            yanchor='top'
        )
        )
        # Create fake scatter points for the size legend
        sizes_for_legend = [1, 5, 10]  # Example sizes
        for size in sizes_for_legend:
            fig.add_scatter(
                x=[None],  # Invisible point
                y=[None],
                mode='markers',
                marker=dict(size=size, color='red', opacity=0.5),
                name=f'{size}% cells' , # Custom legend name for size
                showlegend=True 
            )

        # Convert DataFrame to JSON
        df_json = df_dot_plot.to_json(orient='records')

        return fig, df_json

    def marker_gene_set(self):
        """
        Plot dot plot for a set of marker genes.
        """
        adata = self.adata.copy()

        # List of genes you are interested in
        target_genes = self.params  # Replace with your genes of interest

        # Get unique clusters and sort them numerically or alphabetically
        clusters = sorted(adata.obs['leiden'].unique(), key=lambda x: int(x) if str(x).isdigit() else str(x))

        # Prepare dot plot data using the helper function
        df_dot_plot = self.prepare_dot_plot_data(adata, target_genes, clusters)

        # Create the dot plot using the helper function without gene annotations
        fig, df_json = self.create_dot_plot(df_dot_plot, 'Dot Plot of Gene Expression Across Clusters')

        return fig, df_json

    def de_genes_as_markers(self):
        """
        Find DE genes and generate a dot plot for these genes with annotations.
        """
        adata = self.adata.copy()
        top=3
        # Perform DE analysis
        sc.tl.rank_genes_groups(adata, groupby="leiden", method=self.params['method'])

        # Get unique clusters
        clusters = sorted(adata.obs['leiden'].unique(), key=lambda x: int(x) if str(x).isdigit() else str(x))

        # Get top 5 DE genes for each cluster and make a unique ordered list
        top_genes = []
        gene_origin_cluster = {}  # Dictionary to map each gene to its origin cluster
        for cluster in clusters:
            top_cluster_genes = sc.get.rank_genes_groups_df(adata, group=cluster).head(top)['names'].tolist()
            for gene in top_cluster_genes:
                if gene not in gene_origin_cluster:
                    gene_origin_cluster[gene] = cluster  # Assign the origin cluster to the gene
            top_genes.extend(top_cluster_genes)

        # Remove duplicates while preserving order
        top_genes = list(dict.fromkeys(top_genes))

        # Prepare dot plot data using the helper function
        df_dot_plot = self.prepare_dot_plot_data(adata, top_genes, clusters)

        # Add a column for gene origin cluster annotation
        df_dot_plot['gene_origin_cluster'] = df_dot_plot['gene'].map(gene_origin_cluster)

        # Create the dot plot using the helper function with gene annotations
        fig, df_json = self.create_dot_plot(df_dot_plot, 'Dot Plot of Top DE Genes Across Clusters')

        return fig, df_json

    



        

    def normalization(self):
        ''' Perform normalization and plot the effects. '''
        self.logger.info("normalization")

        # Saving count data
        self.adata.layers["counts"] = self.adata.X.copy()
        
        # Set the target sum based on the normalization method
        if self.params['normalizationMethod'] == 'Median':
            target_sum = None  # Use median count depth for scaling
        if self.params['normalizationMethod'] == 'CPM':
            target_sum = 1e6  # Counts per million normalization


        adata = self.adata

        # Normalize to median total counts or counts per million
        sc.pp.normalize_total(adata, target_sum=target_sum)
        # Logarithmize the data
        sc.pp.log1p(adata)
        
        # Calculate total counts per cell before normalization
        total_counts_before = adata.layers["counts"].sum(axis=1).A1  # Convert to 1D array

        # Calculate total counts per cell after normalization
        total_counts_after = adata.X.sum(axis=1).A1  # Convert to 1D array

        # Create DataFrame for before and after normalization
        df_normalization = pd.DataFrame({
            'Total Counts Before': total_counts_before,
            'Total Counts After': total_counts_after
        })

        # Apply log scale to the total counts for visualization
        log_total_counts_before = np.log1p(total_counts_before)  # Log(1 + count)
        log_total_counts_after = np.log1p(total_counts_after)  # Log(1 + count)

        # Create subplots: 1 row, 2 columns
        fig = make_subplots(
            rows=1, cols=2,
            subplot_titles=("Log-Scaled Total Counts Distribution", "Log-Scaled Total Counts Before vs. After Normalization"),
            horizontal_spacing=0.15
        )

        # Plot 1: Histogram of log-scaled total counts per cell before and after normalization
        fig.add_trace(go.Histogram(
            x=log_total_counts_before,
            nbinsx=50,
            opacity=0.5,
            name='Before Normalization',
            marker_color='#91bfdb'
        ), row=1, col=1)

        fig.add_trace(go.Histogram(
            x=log_total_counts_after,
            nbinsx=50,
            opacity=0.5,
            name='After Normalization',
            marker_color='#fc8d59'
        ), row=1, col=1)

        # Plot 2: Scatter plot of log-scaled total counts before vs. after normalization
        fig.add_trace(go.Scatter(
            x=log_total_counts_before,
            y=log_total_counts_after,
            mode='markers',
            marker=dict(size=5, color='#1f78b4'),
            name='Normalized Counts'
        ), row=1, col=2)

        # Update layout for the plot
        fig.update_layout(
            title='Normalization Effects on Total Counts (Log-Scaled)',
            height=600,
            legend=dict(x=1.05, y=1),  # Move legend to the right side
            xaxis1_title='Log(Total Counts)',  # X-axis title for histogram
            yaxis1_title='Distribution',  # Y-axis title for histogram
            xaxis2_title='Log(Total Counts Before Normalization)',  # X-axis title for scatter plot
            yaxis2_title='Log(Total Counts After Normalization)'  # Y-axis title for scatter plot
        )

        # Convert DataFrame to JSON
        df_json = df_normalization.to_json(orient='records')

        return fig, df_json

    

    


    def filter_cells_genes(self):
        ''' Function to filter cells and genes and plot the results. '''
        self.logger.info("filter cells and genes")

        # Calculate initial cell and gene counts before filtering
        initial_cells = self.adata.shape[0]
        initial_genes = self.adata.shape[1]

        # Filter cells and genes
        adata=self.adata.copy()
        sc.pp.filter_cells(adata, min_genes=self.params['minGenes'])
        sc.pp.filter_genes(adata, min_cells=self.params['minCells'])
        self.adata=adata

        # Calculate cell and gene counts after filtering
        filtered_cells = self.adata.shape[0]
        filtered_genes = self.adata.shape[1]

        # Calculate reductions
        cell_reduction = initial_cells - filtered_cells
        gene_reduction = initial_genes - filtered_genes

        # Calculate percentage reductions
        cell_reduction_percent = (cell_reduction / initial_cells) * 100
        gene_reduction_percent = (gene_reduction / initial_genes) * 100

        # Create DataFrame to summarize results
        df_summary = pd.DataFrame({
            'Measure': ['Cells', 'Genes'],
            'Before': [initial_cells, initial_genes],
            'After': [filtered_cells, filtered_genes],
            'Reduction': [cell_reduction, gene_reduction],
            'Reduction (%)': [cell_reduction_percent, gene_reduction_percent]
        })

        # Print the DataFrame for verification
        print(df_summary)

        # Create subplots: 2 rows, 1 column
        fig = make_subplots(
            rows=2, cols=1,
            subplot_titles=("Absolute Counts Before and After Filtering", "Percentage Reduction"),
            vertical_spacing=0.2  # Adjust spacing between subplots
        )

        # Add bar traces for absolute counts before and after filtering in row 1
        fig.add_trace(go.Bar(
            x=['Cells', 'Genes'],
            y=[initial_cells, initial_genes],
            name='Before Filtering',
            marker_color='#1b9e77'  # Light blue
        ), row=1, col=1)

        fig.add_trace(go.Bar(
            x=['Cells', 'Genes'],
            y=[filtered_cells, filtered_genes],
            name='After Filtering',
            marker_color='#7570b3'  # Light red
        ), row=1, col=1)

        # Add bar trace for percentage reduction in row 2
        fig.add_trace(go.Bar(
            x=['Cells', 'Genes'],
            y=[cell_reduction_percent, gene_reduction_percent],
            name='Percentage Reduction',
            marker_color='#d95f02'  # Green color for percentage
        ), row=2, col=1)

        # Update layout for the plot
        fig.update_layout(
            title="Filtering Results: Absolute Counts and Percentage Reductions",
            xaxis_title="Measure",
            yaxis=dict(
                title="Absolute Counts",
                side='left'
            ),
            yaxis2=dict(
                title="Percentage Reduction",
                side='left',
                showgrid=True
            ),
            legend=dict(x=1, y=0.5),  # Position legend to the right side
            margin=dict(l=50, r=100, t=50, b=50),  # Add space for the legend on the right
            height=800
        )

        # Convert DataFrame to JSON
        df_json = df_summary.to_json(orient='records')

        # Show the plot

        return fig, df_json
    

    def compute_clustering(self):
        '''Compute UMAP and plot UMAP1 vs UMAP2 colored by a gene's expression.'''
        # Make a copy of the AnnData object to avoid modifying the original data
        adata = self.adata.copy()

        if self.umap_flag:
            sc.tl.leiden(adata, flavor=self.params['flavor'], n_iterations=self.params['n_iterations'],resolution=self.params['resolution'])
        else:
            sc.pp.neighbors(adata)
            sc.tl.umap(adata)
            sc.tl.leiden(adata, flavor=self.params['flavor'], n_iterations=self.params['n_iterations'],resolution=self.params['resolution'])

        self.adata=adata

        # Extract UMAP coordinates
        umap_1 = adata.obsm['X_umap'][:, 0]  # UMAP1 coordinates
        umap_2 = adata.obsm['X_umap'][:, 1]  # UMAP2 coordinates
        gene_name='leiden'
        # Select the gene expression data for a specific gene (first gene in the index)
        gene_expression = adata.obs['leiden'].values  # Get the first gene name

        if pd.api.types.is_numeric_dtype(gene_expression):

            # Create a scatter plot using Plotly
            fig = go.Figure()

            fig.add_trace(go.Scatter(
                x=umap_1,
                y=umap_2,
                mode='markers',
                marker=dict(
                    size=6,
                    color=gene_expression,  # Color by gene expression
                    colorscale='Viridis',   # Choose a color scale
                    showscale=True,         # Show color scale legend
                    colorbar=dict(title=gene_name)  # Color bar title as gene name
                ),
                name=f'UMAP colored by {gene_name}'

            ))
        else:
            # If categorical, use discrete colors
            unique_categories = sorted(pd.unique(gene_expression), key=lambda x: int(x) if x.isdigit() else x)
            color_map = px.colors.qualitative.Safe  # Choose a suitable color set

            # Create a color dictionary to map categories to colors
            color_dict = {category: color_map[i % len(color_map)] for i, category in enumerate(unique_categories)}

            fig = go.Figure()

            # Add a scatter trace for each unique category
            for category in unique_categories:
                category_mask = gene_expression == category
                fig.add_trace(go.Scatter(
                    x=[umap_1[i] for i in range(len(umap_1)) if category_mask[i]],
                    y=[umap_2[i] for i in range(len(umap_2)) if category_mask[i]],
                    mode='markers',
                    marker=dict(
                        size=6,
                        color=color_dict[category]  # Use the color from the color map
                    ),
                    name=f'Cluster {category}',  # Legend entry for each category
                ))


        # Update layout for the plot
        fig.update_layout(
            title=f'UMAP1 vs UMAP2 colored by {gene_name} expression',
            xaxis_title='UMAP1',
            yaxis_title='UMAP2',
            height=600,
            paper_bgcolor='white',  # Set white background
            plot_bgcolor='white',   # Set white plot background
        )

        df_umap = pd.DataFrame({
        'UMAP1': umap_1,
        'UMAP2': umap_2,
        gene_name: gene_expression
        })

        # Display the plot
        
        # Convert DataFrame to JSON
        df_json = df_umap.to_json(orient='records')

        return fig, df_json
    



    def compute_umap(self):
        '''Compute UMAP and plot UMAP1 vs UMAP2 colored by a gene's expression.'''
        # Make a copy of the AnnData object to avoid modifying the original data
        adata = self.adata.copy()

        # Compute the neighbors and UMAP
        sc.pp.neighbors(adata, n_neighbors=self.params['n_neighbors'], n_pcs=self.params['n_pcs'])
        sc.tl.umap(adata, min_dist=self.params['min_dist'])
        self.adata=adata

        # Extract UMAP coordinates
        umap_1 = adata.obsm['X_umap'][:, 0]  # UMAP1 coordinates
        umap_2 = adata.obsm['X_umap'][:, 1]  # UMAP2 coordinates

        # Select the gene expression data for a specific gene (first gene in the index)
        gene_name = adata.var.index[0]  # Get the first gene name

        gene_expression = adata[:, gene_name].X.toarray().flatten()  # Extract expression values for that gene

        # Create a scatter plot using Plotly
        fig = go.Figure()

        fig.add_trace(go.Scatter(
            x=umap_1,
            y=umap_2,
            mode='markers',
            marker=dict(
                size=6,
                color=gene_expression,  # Color by gene expression
                colorscale='Viridis',   # Choose a color scale
                showscale=True,         # Show color scale legend
                colorbar=dict(title=gene_name)  # Color bar title as gene name
            ),
            name=f'UMAP colored by {gene_name}'
        ))

        # Update layout for the plot
        fig.update_layout(
            title=f'UMAP1 vs UMAP2 colored by {gene_name} expression',
            xaxis_title='UMAP1',
            yaxis_title='UMAP2',
            height=600,
            paper_bgcolor='white',  # Set white background
            plot_bgcolor='white',   # Set white plot background
        )

        df_umap = pd.DataFrame({
        'UMAP1': umap_1,
        'UMAP2': umap_2,
        gene_name: gene_expression
        })

        # Display the plot
        
        # Convert DataFrame to JSON
        df_json = df_umap.to_json(orient='records')

        return fig, df_json


    def doublet_detection(self):
        ''' Function to detect doublets and plot doublet scores. '''
        self.logger.info("Running doublet detection")

        # Copy AnnData object to avoid modifying the original
        adata = self.adata.copy()

        # Perform doublet detection using Scrublet
        sc.pp.scrublet(adata)
        self.adata=adata

        # Create a DataFrame with index and two columns
        df_doublet = adata.obs[['doublet_score', 'predicted_doublet']].copy()
        df_doublet['cell_index'] = adata.obs.index  # Include cell identifiers as a separate column

        # Sort the DataFrame by doublet score in descending order
        df_doublet = df_doublet.sort_values(by='doublet_score', ascending=False)

        # Print the DataFrame for verification
        

        # Create a scatter plot of doublet scores
        fig = go.Figure()

        # Add points for predicted doublets (True)
        fig.add_trace(go.Scatter(
            x=df_doublet[df_doublet['predicted_doublet'] == True]['cell_index'],
            y=df_doublet[df_doublet['predicted_doublet'] == True]['doublet_score'],
            mode='markers',
            marker=dict(
                color='#fc8d59',  # Color for True
                size=8,
                line=dict(width=1)
            ),
            name='Predicted Doublet (True)'
        ))

        # Add points for predicted doublets (False)
        fig.add_trace(go.Scatter(
            x=df_doublet[df_doublet['predicted_doublet'] == False]['cell_index'],
            y=df_doublet[df_doublet['predicted_doublet'] == False]['doublet_score'],
            mode='markers',
            marker=dict(
                color='#91bfdb',  # Color for False
                size=8,
                line=dict(width=1)
            ),
            name='Predicted Doublet (False)'
        ))

        # Update layout for the plot with legend enabled
        fig.update_layout(
            title="Doublet Detection: Doublet Scores by Predicted Status",
            xaxis_title="Cell Index",
            yaxis_title="Doublet Score",
            height=600,
            margin=dict(l=50, r=50, t=50, b=50),
            legend=dict(
                x=1,  # Place legend to the right
                y=1,
                traceorder='normal',
                font=dict(
                    family="sans-serif",
                    size=12,
                    color="black"
                ),
                bgcolor="White",
                bordercolor="Black",
                borderwidth=1
            )
        )

            # Convert DataFrame to JSON
        df_json = df_doublet.to_json(orient='records')

        return fig, df_json


    def dimensionality_reduction(self):
        ''' Perform PCA and plot the explained variance ratio for each PC. '''
        self.logger.info("Dimensionality reduction using PCA")

        adata = self.adata.copy()
        sc.tl.pca(adata)
        self.adata=adata

        # Extract variance ratio for the specified number of principal components
        variance_ratio = adata.uns['pca']['variance_ratio'][:self.params['n_comps']]
        pc_numbers = [f'PC{i+1}' for i in range(len(variance_ratio))]  # List of PC labels

        # Create a DataFrame with variance ratio and PC numbers
        df_variance_ratio = pd.DataFrame({
            'Principal Component': pc_numbers,
            'Variance Ratio': variance_ratio
        })

        # Create a bar plot for the variance ratio
        fig = go.Figure()

            # Add a scatter trace with lines and markers
        fig.add_trace(go.Scatter(
            x=df_variance_ratio['Principal Component'],
            y=df_variance_ratio['Variance Ratio'],
            mode='lines+markers',  # Plot with both lines and markers
            text=df_variance_ratio['Principal Component'],  # PC labels for hover
            hoverinfo='text+y',  # Show PC number and variance ratio on hover
            name='Explained Variance Ratio',
            marker=dict(size=8, color='#2c7bb6'),
            line=dict(color='#2c7bb6')
        ))

        # Update layout for the plot
        fig.update_layout(
            title='Variance Ratio of Principal Components',
            xaxis=dict(
            title='Principal Component (PC)',
            showgrid=False,  # Disable x-axis grid
            showline=True,  # Show x-axis line
            linecolor='black',  # X-axis line color
            linewidth=1  # X-axis line width
            ),
        yaxis=dict(
            title='Variance Ratio (Log Scale)',
            type='log',  # Set the y-axis to log scale
            showgrid=False,  # Disable y-axis grid
            showline=True,  # Show y-axis line
            linecolor='black',  # Y-axis line color
            linewidth=1  # Y-axis line width
            ),
            height=600,
            legend=dict(x=1, y=1),
            paper_bgcolor='white',  # Set the background outside the plot area to white
            plot_bgcolor='white'    # Set the plot background to white
            
        )

        # Convert DataFrame to JSON for further use if needed
        df_json = df_variance_ratio.to_json(orient='records')

        # Return both the plot and the DataFrame
        return fig, df_json



    def feature_selection(self):
        '''Perform feature selection and plot the results.'''
        self.logger.info("Feature selection")

        adata = self.adata.copy()
        if not isinstance(adata.X, csr_matrix):
            adata.X = csr_matrix(adata.X)

        # Set the parameters for feature selection
        n_top_genes = self.params.get('n_top_genes', 2000)  # Default to 2000 if not specified
        flavor = self.params.get('flavor', 'seurat')  # Default to 'seurat' if not specified

        # Identify highly variable genes
        sc.pp.highly_variable_genes(adata, n_top_genes=n_top_genes, flavor=flavor)

        # Extract relevant data
        hv_genes = adata.var['highly_variable']  # Boolean indicator of highly-variable genes
        means = adata.var['means']  # Means per gene

        dispersions =  adata.var['dispersions'] if flavor in ['seurat', 'cell_ranger'] else adata.var['variances']  # Dispersions per gene
        dispersions_norm = adata.var['dispersions_norm'] if flavor in ['seurat', 'cell_ranger'] else adata.var['variances_norm'] 

        # Create DataFrame for plotting
        df_genes = pd.DataFrame({
            'gene_name':adata.var.index,
            'means': means,
            'dispersions': dispersions,
            'dispersions_norm': dispersions_norm,
            'highly_variable': hv_genes
            
        })

        # Create subplots: 1 row, 2 columns
        fig = make_subplots(
            rows=1, cols=2,
            subplot_titles=(
                "Mean Expression vs. Dispersion (Unnormalized)",
                "Mean Expression vs. Dispersion (Normalized)"
            ),
            horizontal_spacing=0.15
        )

        # Plot 1: Mean Expression vs. Dispersion (Unnormalized)
        # Highly Variable Genes (Unnormalized)
        fig.add_trace(go.Scatter(
            x=df_genes[df_genes['highly_variable']]['means'],
            y=df_genes[df_genes['highly_variable']]['dispersions'],
            mode='markers',
            marker=dict(
                size=6,
                color='#d7191c',  # Red for highly variable genes
                opacity=0.7
            ),
            name='Highly Variable Genes',  # Show only one entry for highly variable genes in the legend
            showlegend=True,  # Show legend for this trace
            text=df_genes[df_genes['highly_variable']].index,  # Add gene names as hover text
            hoverinfo='text+x+y',  # Show gene names, x, and y on hove
        ), row=1, col=1)

        # Other Genes (Unnormalized)
        fig.add_trace(go.Scatter(
            x=df_genes[~df_genes['highly_variable']]['means'],
            y=df_genes[~df_genes['highly_variable']]['dispersions'],
            mode='markers',
            marker=dict(
                size=6,
                color='#2c7bb6',  # Blue for other genes
                opacity=0.7
            ),
            name='Other Genes',  # Show only one entry for other genes in the legend
            text=df_genes[df_genes['highly_variable']].index,  # Add gene names as hover text
            hoverinfo='text+x+y',  # Show gene names, x, and y on hove
            showlegend=True  # Show legend for this trace
        ), row=1, col=1)

        # Plot 2: Mean Expression vs. Dispersion (Normalized)
        # Highly Variable Genes (Normalized)
        fig.add_trace(go.Scatter(
            x=df_genes[df_genes['highly_variable']]['means'],
            y=df_genes[df_genes['highly_variable']]['dispersions_norm'],
            mode='markers',
            marker=dict(
                size=6,
                color='#d7191c',  # Red for highly variable genes
                opacity=0.7
            ),
            name='Highly Variable Genes',  # Same name to group them in the legend
            showlegend=False,  # Hide legend for duplicate entry
            text=df_genes[df_genes['highly_variable']].index,  # Add gene names as hover text
                hoverinfo='text+x+y',  # Show gene names, x, and y on hove
        ), row=1, col=2)

        # Other Genes (Normalized)
        fig.add_trace(go.Scatter(
            x=df_genes[~df_genes['highly_variable']]['means'],
            y=df_genes[~df_genes['highly_variable']]['dispersions_norm'],
            mode='markers',
            marker=dict(
                size=6,
                color='#2c7bb6',  # Blue for other genes
                opacity=0.7
            ),
            name='Other Genes',  # Same name to group them in the legend
            showlegend=False,  # Hide legend for duplicate entry
            text=df_genes[df_genes['highly_variable']].index,  # Add gene names as hover text
                hoverinfo='text+x+y',  # Show gene names, x, and y on hove
        ), row=1, col=2)

        # Update layout for the plot
        fig.update_layout(
            title='Feature Selection: Mean Expression vs. Dispersion',
            height=600,
            legend=dict(x=1.05, y=1),  # Move legend to the right side
            xaxis1=dict(title='Mean Expression of Genes'),
            yaxis1=dict(title='Dispersion of Genes (Unnormalized)'),
            xaxis2=dict(title='Mean Expression of Genes'),
            yaxis2=dict(title='Dispersion of Genes (Normalized)')
        )

        # Convert DataFrame to JSON
        df_json = df_genes.to_json(orient='records')

        return fig, df_json


    def calculate_qc_metrics(self):
        ''' '''
        self.adata.obs['n_genes_by_counts'] = (self.adata.X > 0).sum(axis=1)

        self.adata.obs['total_counts'] = self.adata.X.sum(axis=1)
        mt_genes=[]
        if self.params and (self.params['filterType']=='startswith') and (not (self.params['filterValue']=='')):
            mt_genes = self.adata.var_names.str.startswith(self.params['filterValue'])
        elif self.params and (self.params['filterType']=='regexp') and (not (self.params['filterValue']=='')):
            mt_genes=self.adata.var_names.str.contains(self.params['filterValue'])
        mt_counts = self.adata[:, mt_genes].X.sum(axis=1)
        mt_counts = np.array(mt_counts).flatten()
        self.adata.obs['pct_counts_mt'] = (mt_counts / self.adata.obs['total_counts']) * 100


        return self.adata

    def plot_qc_metrics(self):
        # Assuming qc_df is your DataFrame
        qc_df = self.adata.obs[['n_genes_by_counts', 'total_counts', 'pct_counts_mt']]

        # Create subplots: 4 rows, 1 column
        fig = make_subplots(
            rows=4, cols=1,
            shared_xaxes=False,
            subplot_titles=("n_genes_by_counts", "total_counts", "pct_counts_mt", "Scatter Plot"),
            vertical_spacing=0.1  # Adjust vertical spacing between subplots
        )

        # Add violin plot for 'n_genes_by_counts'
        fig.add_trace(
            go.Violin(
                y=qc_df['n_genes_by_counts'],
                box_visible=True,
                points="all",
                jitter=0.1,
                pointpos=0,
                name='n_genes_by_counts',
                spanmode='soft'
            ),
            row=1, col=1
        )

        # Add violin plot for 'total_counts'
        fig.add_trace(
            go.Violin(
                y=qc_df['total_counts'],
                box_visible=True,
                points="all",
                jitter=0.1,
                pointpos=0,
                name='total_counts',
                spanmode='soft'
            ),
            row=2, col=1
        )

        # Add violin plot for 'pct_counts_mt'
        fig.add_trace(
            go.Violin(
                y=qc_df['pct_counts_mt'],
                box_visible=True,
                points="all",
                jitter=0.1,
                pointpos=0,
                name='pct_counts_mt',
                spanmode='soft'
            ),
            row=3, col=1
        )

        # Add scatter plot for 'total_counts' vs 'n_genes_by_counts'
        fig.add_trace(
            go.Scatter(
                x=qc_df['total_counts'],
                y=qc_df['n_genes_by_counts'],
                mode='markers',
                marker=dict(
                    color=qc_df['pct_counts_mt'],
                    colorscale='Viridis',
                    showscale=True,  # Show color bar only for scatter plot
                    colorbar=dict(
                        title="pct_counts_mt",  # Title for the color scale
                        len=0.25,  # Adjust color bar length to fit within the plot 4 space
                        y=0.125,  # Center color bar within the plot 4 space
                        yanchor='middle'  # Align the color bar within its allotted space
                    )
                ),
                name='Scatter Plot'
            ),
            row=4, col=1
        )

        # Update layout with specific adjustments
        fig.update_layout(
            title_text="QC Metrics",
            height=1200,
            margin=dict(l=50, r=50, t=50, b=50),  # Set margins
            paper_bgcolor="white",  # Background color
            plot_bgcolor="white",
            title_x=0.5,  # Center the title
            showlegend=False,  # Hide the legend for clarity
        )

        # Update axis properties to show y-axis for violin plots
        fig.update_yaxes(
                title="Value",
            showgrid=False,
            zeroline=False,
            showline=True,
            linecolor='black',
            gridcolor='lightgray',
            row=1, col=1
        )

        fig.update_yaxes(
            title="Value",
            showgrid=False,
            zeroline=False,
            showline=True,
            linecolor='black',
            gridcolor='lightgray',
            row=2, col=1
        )

        fig.update_yaxes(
            title="Value",
            showgrid=False,
            zeroline=False,
            showline=True,
            linecolor='black',
            gridcolor='lightgray',
            row=3, col=1
        )

        fig.update_xaxes(
            title="Total Counts",
            showgrid=True,
            zeroline=True,
            showline=True,
            linecolor='black',
            gridcolor='lightgray',
            row=4, col=1
        )

        fig.update_yaxes(
            title="n_genes_by_counts",
            showgrid=True,
            zeroline=True,
            showline=True,
            linecolor='black',
            gridcolor='lightgray',
            row=4, col=1
        )

        # Convert DataFrame to JSON
        df_json = qc_df.to_json(orient='records')

        return fig, df_json


        # fig.show()


    def analyze(self):
        """
        Placeholder method for performing analysis.
        """
        print(f"Performing {self.analysis_type} analysis with parameters: {self.parameters}...")

    def save_results(self):
        """
        Placeholder method for saving results.
        """
        print(f"Saving results to {self.output_path}...")
