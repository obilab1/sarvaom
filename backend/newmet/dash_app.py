import dash
import dash_core_components as dcc
import dash_html_components as html
from django_plotly_dash import DjangoDash
import plotly.express as px
import pandas as pd

import numpy as np

np.random.seed(42)
num_points = 10000000  # 1 million points
df = pd.DataFrame({
        "x": np.random.randn(num_points),
        "y": np.random.randn(num_points),
        "category": np.random.choice(["A", "B"], num_points)
    })
fig = px.scatter(df, x="x", y="y", color="category", title=f"Scatter Plot ")


app = DjangoDash('SimpleExample')  # Register the Dash app

app.layout = html.Div(children=[
    html.H1(children='Hello Dash'),

    html.Div(children='''
        Dash: A web application framework for Python.
    '''),

    dcc.Graph(
        id='example-graph',
        figure=fig
    )
])
