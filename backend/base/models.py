from django.db import models
from django.contrib.auth.models import User
import os

def project_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<project_name>/<filename>
    return f'user_{instance.project.user.id}/{instance.project.project_name}/{filename}'

# Create your models here.

class Note(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    body = models.TextField()

class ProjectCard(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    project_name = models.CharField(max_length=255)
    project_description = models.TextField()
    data_type = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.project_name

class UploadedFile(models.Model):
    project = models.ForeignKey(ProjectCard, related_name='files', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    file = models.FileField(upload_to=project_directory_path)  # Changed to FileField with dynamic path
    metadata = models.JSONField(default=dict)

    def __str__(self):
        return self.name
