from django.contrib import admin
from .models import ProjectCard,Note

class ProjectCardAdmin(admin.ModelAdmin):
    list_display = ('project_name', 'user', 'data_type', 'created_at')
    search_fields = ('project_name', 'user__username', 'data_type')

admin.site.register(ProjectCard, ProjectCardAdmin)

# Register your models here.


admin.site.register(Note)
