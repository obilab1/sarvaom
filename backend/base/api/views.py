from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import status
from rest_framework import generics
from .serializers import UserCreateSerializer
from django.contrib.auth import get_user_model
from rest_framework.permissions import AllowAny
from django.http import JsonResponse
import plotly.express as px
import pandas as pd
import json
from .serializers import NoteSerializer,ProjectCardSerializer,UploadedFileSerializer
# from base.models import Note
from base.models import ProjectCard,UploadedFile
import numpy as np
import plotly.graph_objects as go
import os
from django.core.files.storage import default_storage
from django.shortcuts import get_object_or_404
import logging
from bokeh.embed import json_item
# Set up logging
logger = logging.getLogger(__name__)
from tools.metabolomics.metasurv.msurv import Metasurv
from tools.scsurv.scsurv import SCSurv,SCUtil

metaom=Metasurv()


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom claims
        token['username'] = user.username
        token['roles'] = ['User']
        # ...

        return token


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer





User = get_user_model()

class RegisterView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserCreateSerializer
    permission_classes = [AllowAny]


@api_view(['GET'])
def getRoutes(request):
    routes = [
        '/api/token',
        '/api/token/refresh',
    ]

    return Response(routes)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def getNotes(request):
    user = request.user
    notes = user.note_set.all()
    serializer = NoteSerializer(notes, many=True)
    return Response(serializer.data)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_project_card(request):
    serializer = ProjectCardSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save(user=request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_project_cards(request):
    user = request.user
    project_cards = ProjectCard.objects.filter(user=user)
    serializer = ProjectCardSerializer(project_cards, many=True)
    return Response(serializer.data)


# views.py


# @api_view(['GET'])
# @permission_classes([IsAuthenticated])
# def get_plot_data(request, project_name):
#     # Generate random data

#     # N = 10000000 
#     np.random.seed(42)
#     # Create figure
#     # fig = go.Figure()

#     # fig.add_trace(
#     #     go.Scattergl(
#     #         x = np.random.randn(N),
#     #         y = np.random.randn(N),
#     #         mode = 'markers',
#     #         marker = dict(
#     #             line = dict(
#     #                 width = 1,
#     #                 color = 'DarkSlateGrey')
#     #         )
#     #     )
#     # )
    
#     num_points = 10000000  # 1 million points
#     df = pd.DataFrame({
#         "x": np.random.randn(num_points),
#         "y": np.random.randn(num_points),
#         "category": np.random.choice(["A", "B"], num_points)
#     })
#     fig = px.scatter(df, x="x", y="y", color="category", title=f"Scatter Plot for {project_name}",render_mode='webgl')
#     fig_json = json.loads(fig.to_json())
#     return JsonResponse(fig_json)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_plot_data(request, project_name):
    # Generate random data
    np.random.seed(42)
    num_points = 1000  # 100 million points
    x = np.random.randn(num_points)
    y = np.random.randn(num_points)
    data = {"x": x.tolist(), "y": y.tolist()}
    return JsonResponse(data)



@api_view(['POST'])
@permission_classes([IsAuthenticated])
def upload_files(request, project_name):
   
    user = request.user
    project = get_object_or_404(ProjectCard, project_name=project_name, user=user)
    project_path = os.path.join('user_files', str(user.id), project_name)
    os.makedirs(project_path, exist_ok=True)

    for file in request.FILES.getlist('files'):
        file_path = os.path.join(project_path, file.name)
        with default_storage.open(file_path, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)

    # Create an entry in the UploadedFile model
        UploadedFile.objects.create(
            project=project,
            name=file.name,
            file=file_path,  # Store the file path
            metadata={}
        )

    return Response({'message': 'Files uploaded successfully'}, status=status.HTTP_201_CREATED)



@api_view(['GET'])
@permission_classes([IsAuthenticated])
def list_files(request, project_name):
    user = request.user
    project = get_object_or_404(ProjectCard, project_name=project_name, user=user)
    files = UploadedFile.objects.filter(project=project)
    serializer = UploadedFileSerializer(files, many=True)
    return Response(serializer.data)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def delete_file(request, project_name, file_id):
    try:
        file_instance = UploadedFile.objects.get(id=file_id, project__project_name=project_name)
        file_instance.file.delete()  # This deletes the file from the filesystem
        file_instance.delete()  # This deletes the database record  
        return Response(status=status.HTTP_204_NO_CONTENT)
    except UploadedFile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def rename_file(request, project_name, file_id):
    user = request.user
    project = get_object_or_404(ProjectCard, project_name=project_name, user=user)
    file_instance = get_object_or_404(UploadedFile, id=file_id, project=project)
    new_name = request.data.get('new_name')
    print(new_name)
    if new_name:
        file_instance.name = new_name
        file_instance.save()
        return Response({'message': 'File renamed successfully'}, status=status.HTTP_200_OK)
    return Response({'error': 'New name not provided'}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def update_file_metadata(request, project_name, file_id):
    user = request.user
    project = get_object_or_404(ProjectCard, project_name=project_name, user=user)
    file_instance = get_object_or_404(UploadedFile, id=file_id, project=project)
    file_instance.metadata.update(request.data)

    file_instance.save()
    return Response({'message': 'Metadata updated successfully'}, status=status.HTTP_200_OK)



@api_view(['POST'])
@permission_classes([IsAuthenticated])
def cellxgene_host(request, project_name):
    logger.info("cellxgene hosting")
   
    
    try:
        # Parse the incoming JSON data
        user = request.user
        project = get_object_or_404(ProjectCard, project_name=project_name, user=user)
        if project.data_type == 'Single cell data':
            data = json.loads(request.body)
            logger.info(f"populate_genes: {data}")
            folder_path=None
            file_instance = UploadedFile.objects.filter(project__project_name=project_name).first()
            if file_instance:
            # Extract the folder path from the file's path
                folder_path = os.path.dirname(file_instance.file.path)
                umap_data_path = os.path.join(folder_path, 'sc_data','clustering.h5ad')
                logger.info(f"qc path : {umap_data_path}")
                if ('host' in data.keys()) and (data['host']=='cellxgene'):
                    
                    if os.path.exists(umap_data_path):
                        logger.info("Loading data from previous normalized and pca dim reduction")
                        sutil = SCUtil(folder_path,  logger ,prev_data=umap_data_path,fdata=data)
                        data=sutil.host_on_cell_x_gene()
                        return JsonResponse({
                        'message': 'cellx gene hosting',
                        'data': data,
                       
                    }, status=200)
                    else:
                        logger.info("Need to do clustering first before hosting on cellxGene")
                        logger.error("Invalid Clustering.h5ad data")
                        return JsonResponse({'error': 'Invalid Clustering.h5ad data'}, status=400)
                    
                    
         
                    
    except json.JSONDecodeError as e:
        logger.error("Invalid JSON data")
        return JsonResponse({'error': 'Invalid JSON data'}, status=400)
    except UploadedFile.DoesNotExist as e:
        logger.error(f"File does not exist: {str(e)}")
        return JsonResponse({'error': 'One or more files do not exist'}, status=404)
    except Exception as e:
        logger.error(f"Error processing selection: {str(e)}")
        return JsonResponse({'error': str(e)}, status=500)




@api_view(['POST'])
@permission_classes([IsAuthenticated])
def sc_populate_genes(request, project_name):
    logger.info("single_cell genes umap")
    
    try:
        # Parse the incoming JSON data
        user = request.user
        project = get_object_or_404(ProjectCard, project_name=project_name, user=user)
        if project.data_type == 'Single cell data':
            data = json.loads(request.body)
            logger.info(f"populate_genes: {data}")
            folder_path=None
            file_instance = UploadedFile.objects.filter(project__project_name=project_name).first()
            if file_instance:
            # Extract the folder path from the file's path
                folder_path = os.path.dirname(file_instance.file.path)
                umap_data_path = os.path.join(folder_path, 'sc_data','analysis.h5ad')
                logger.info(f"qc path : {umap_data_path}")
                if ('umap' in data.keys()) and (data['umap']=='genes and parameters'):
                    
                    if os.path.exists(umap_data_path):
                        logger.info("Loading data from previous normalized and pca dim reduction")
                        sutil = SCUtil(folder_path,  logger ,prev_data=umap_data_path)
                    else:
                        logger.info("Previous step data not found, re-initializing SCSurv")
                        sutil = SCUtil(folder_path,logger)


                    genes_paras=sutil.get_genes_parameters()
                    # Ensure genes_paras is JSON serializable
                    if isinstance(genes_paras, pd.DataFrame):
                        genes_paras = genes_paras.to_dict(orient='records')

                    
                    return JsonResponse({
                        'message': 'Geeting geens and parameters',
                        'genes and parameters':genes_paras,
                    
                    }, status=200)
                
                elif ('selected_gene' in data.keys()):
                    if os.path.exists(umap_data_path):
                        logger.info("Loading data from previous normalized and pca dim reduction")
                        sutil = SCUtil(folder_path,  logger ,prev_data=umap_data_path)
                    else:
                        logger.info("Previous step data not found, re-initializing SCSurv")
                        sutil = SCUtil(folder_path,logger)

                    fig,fiter_data=sutil.generate_umap_figure(data['selected_gene'])
     

                    
                   
                    # Convert Bokeh figure to JSON
                    plot_json = fig.to_json()
                    # Save Quality Control output (if needed)
                    # Save Quality Control output in the sc_data directory
                    
                    logger.info("umap of gene is plotted....")
                    #Return a response indicating successful processing
                    return JsonResponse({
                        'message': 'umap of gene is plotted',
                        'bokehPlot': plot_json,
                        'plotlyData': json.loads(plot_json),
                        'tableData':json.loads(fiter_data),
                        'lib':'plotly',
                    }, status=200)
                

                
            
    except json.JSONDecodeError as e:
        logger.error("Invalid JSON data")
        return JsonResponse({'error': 'Invalid JSON data'}, status=400)
    except UploadedFile.DoesNotExist as e:
        logger.error(f"File does not exist: {str(e)}")
        return JsonResponse({'error': 'One or more files do not exist'}, status=404)
    except Exception as e:
        logger.error(f"Error processing selection: {str(e)}")
        return JsonResponse({'error': str(e)}, status=500)



@api_view(['POST'])
@permission_classes([IsAuthenticated])
def single_cell_process(request, project_name):
    logger.info("single_cell_process endpoint called")
    try:
        # Parse the incoming JSON data
        user = request.user
        project = get_object_or_404(ProjectCard, project_name=project_name, user=user)
        if project.data_type == 'Single cell data':

            
            data = json.loads(request.body)
         
            selected_files = data.get('selectedFiles', [])
            selected_options = data.get('selectedOptions', {})
            params = data.get('params', {})
            # current_step = selected_options.get('option', None)
            current_step = data.get('currentStep', '')
            nextStep = data.get('nextStep', '')
            logger.info(f"Selected Files: {params}")
            logger.info(f"Selected Files: {selected_files}")
            logger.info(f"currentStep: {current_step }")
            logger.info(f"nextStep: {nextStep}")
            logger.info(f"selected_options: {selected_options}")
           
            
            folder_path=None
            barcode_file=None
            gene_file=None
            matrix_file=None
            for file_id in selected_files:
                file_instance = UploadedFile.objects.get(id=file_id, project__project_name=project_name)
                file_name = file_instance.name
                if not folder_path:
                    folder_path = os.path.dirname( file_instance.file.path)
                
                logger.info(f"testing single cell files : {file_name}")
                # Always add .mzML files to processed_files
                
                if file_name=='barcodes.tsv':
                    barcode_file=file_name
                elif file_name=='genes.tsv':
                    gene_file=file_name
                elif file_name=='matrix.mtx':
                    matrix_file=file_name

        if (not (barcode_file==None)) & (not (gene_file==None)) & (not (matrix_file==None)):
            if current_step is None:
                logger.error("No current_step provided in selected_options.")
                return JsonResponse({'error': 'No current step provided'}, status=400)
            
            logger.info(f"current step: {current_step}")
            ### this is for single cell analysis 
            if ('option' in selected_options) & ('subCategory' in selected_options) & ('mainCategory' in selected_options):
                if (selected_options['subCategory']=='Analysis Type') & (selected_options['mainCategory']=='Analysis'):
                    if (selected_options['option']=='UMAP'):
                        logger.info(f"before ececuting  : {selected_options['option']}")

                        pca_data_path = os.path.join(folder_path, 'sc_data','qc_data_final.h5ad')
                        logger.info(f"qc path : {pca_data_path}")

                        if os.path.exists(pca_data_path):
                            logger.info("Loading data from previous normalized and pca dim reduction")
                            scom = SCSurv(folder_path, selected_options, params, logger ,prev_data=pca_data_path)
                        else:
                            logger.info("Previous step data not found, re-initializing SCSurv")
                            scom = SCSurv(folder_path, selected_options, params,logger)

                        fig,fiter_data=scom.compute_umap()
                        # Convert the JSON string to a Python list (array of dictionaries)
                        
                        # Convert Bokeh figure to JSON
                        plot_json = fig.to_json()
                        # Save Quality Control output (if needed)
                        # Save Quality Control output in the sc_data directory
                        save_intermediate_result(folder_path, 'analysis.h5ad', scom.adata)
                        
                        #Return a response indicating successful processing
                        return JsonResponse({
                            'message': 'Filter Cells & Genes step executed successfully',
                            'barcode_file': barcode_file,
                            'gene_file': gene_file,
                            'matrix_file': matrix_file,
                            'bokehPlot': plot_json,
                            'plotlyData': json.loads(plot_json),
                            'tableData':json.loads(fiter_data),
                            'lib':'plotly',
                            'currentStep': 'Filter Cells & Genes',
                            'nextStep': 'Doublet Detection'
                        }, status=200)
                    
                    elif  (selected_options['option']=='Clustering'):
                        logger.info(f"before executing  : {selected_options['option']}")

                        pca_data_path = os.path.join(folder_path, 'sc_data','analysis.h5ad')
                        logger.info(f"qc path : {pca_data_path}")

                        if os.path.exists(pca_data_path):
                            logger.info("Loading data from previous normalized and pca dim reduction")
                            scom = SCSurv(folder_path, selected_options, params, logger ,prev_data=pca_data_path)
                        else:
                            logger.info("Previous step data not found, re-initializing SCSurv")
                            scom = SCSurv(folder_path, selected_options, params,logger)
                        
                        fig,fiter_data=scom.compute_clustering()
                        # Convert the JSON string to a Python list (array of dictionaries)
                        
                        # Convert Bokeh figure to JSON
                        plot_json = fig.to_json()
                        # Save Quality Control output (if needed)
                        # Save Quality Control output in the sc_data directory
                        save_intermediate_result(folder_path, 'clustering.h5ad', scom.adata)
                       
                        #Return a response indicating successful processing
                        return JsonResponse({
                            'message': 'Filter Cells & Genes step executed successfully',
                            'barcode_file': barcode_file,
                            'gene_file': gene_file,
                            'matrix_file': matrix_file,
                            'bokehPlot': plot_json,
                            'plotlyData': json.loads(plot_json),
                            'tableData':json.loads(fiter_data),
                            'lib':'plotly',
                            'currentStep': 'Filter Cells & Genes',
                            'nextStep': 'Doublet Detection'
                        }, status=200)
                    

                    elif  (selected_options['option']=='Marker gene set'):
                        logger.info(f"before executing  : {selected_options['option']}")

                        pca_data_path = os.path.join(folder_path, 'sc_data','clustering.h5ad')
                        logger.info(f"qc path : {pca_data_path}")

                        if os.path.exists(pca_data_path):
                            logger.info("Loading data from previous normalized and pca dim reduction")
                            scom = SCSurv(folder_path, selected_options, params, logger ,prev_data=pca_data_path)
                        else:
                            logger.info("Previous step data not found, re-initializing SCSurv")
                            scom = SCSurv(folder_path, selected_options, params,logger)
                        
                        fig,fiter_data=scom.marker_gene_set()
                        # Convert the JSON string to a Python list (array of dictionaries)
                        
                        # Convert Bokeh figure to JSON
                        plot_json = fig.to_json()
                        # Save Quality Control output (if needed)
                        # Save Quality Control output in the sc_data directory
                        save_intermediate_result(folder_path, 'clustering.h5ad', scom.adata)
                       
                        #Return a response indicating successful processing
                        return JsonResponse({
                            'message': 'Filter Cells & Genes step executed successfully',
                            'barcode_file': barcode_file,
                            'gene_file': gene_file,
                            'matrix_file': matrix_file,
                            'bokehPlot': plot_json,
                            'plotlyData': json.loads(plot_json),
                            'tableData':json.loads(fiter_data),
                            'lib':'plotly',
                            'currentStep': 'Filter Cells & Genes',
                            'nextStep': 'Doublet Detection'
                        }, status=200)
                    
                    elif  (selected_options['option']=='DE genes as markers'):
                        logger.info(f"before executing  : {selected_options['option']}")

                        pca_data_path = os.path.join(folder_path, 'sc_data','clustering.h5ad')
                        logger.info(f"qc path : {pca_data_path}")

                        if os.path.exists(pca_data_path):
                            logger.info("Loading data from previous normalized and pca dim reduction")
                            scom = SCSurv(folder_path, selected_options, params, logger ,prev_data=pca_data_path)
                        else:
                            logger.info("Previous step data not found, re-initializing SCSurv")
                            scom = SCSurv(folder_path, selected_options, params,logger)
                        
                        fig,fiter_data=scom.de_genes_as_markers()
                        # Convert the JSON string to a Python list (array of dictionaries)
                        
                        # Convert Bokeh figure to JSON
                        plot_json = fig.to_json()
                        # Save Quality Control output (if needed)
                        # Save Quality Control output in the sc_data directory
                        save_intermediate_result(folder_path, 'clustering.h5ad', scom.adata)
                       
                        #Return a response indicating successful processing
                        return JsonResponse({
                            'message': 'Filter Cells & Genes step executed successfully',
                            'barcode_file': barcode_file,
                            'gene_file': gene_file,
                            'matrix_file': matrix_file,
                            'bokehPlot': plot_json,
                            'plotlyData': json.loads(plot_json),
                            'tableData':json.loads(fiter_data),
                            'lib':'plotly',
                            'currentStep': 'Filter Cells & Genes',
                            'nextStep': 'Doublet Detection'
                        }, status=200)
                    
            

           
            if current_step == "Quality Control":
                selected_options['option']=current_step
                scom=SCSurv(folder_path,selected_options,params,logger)
                
                fig,qc_data=scom.plot_qc_metrics()
                # Convert the JSON string to a Python list (array of dictionaries)
                
                # Convert Bokeh figure to JSON
                plot_json = fig.to_json()
                # Save Quality Control output (if needed)
                 # Save Quality Control output in the sc_data directory
                save_intermediate_result(folder_path, 'qc_data.h5ad', scom.adata)
                
                #Return a response indicating successful processing
                return JsonResponse({
                    'message': 'Single cell data processed successfully',
                    'barcode_file': barcode_file,
                    'gene_file': gene_file,
                    'matrix_file': matrix_file,
                    'bokehPlot': plot_json,
                    'plotlyData': json.loads(plot_json),
                    'tableData':json.loads(qc_data),
                    'lib':'plotly',
                    'currentStep': 'Quality Control',
                    'nextStep': 'Filter Cells & Genes'
                }, status=200)
            
            elif current_step == "Filter Cells & Genes":
                selected_options['option']=current_step
                logger.info("Executing step: Filter Cells & Genes")
                # Check if QC data is available from the previous step
                qc_data_path = os.path.join(folder_path, 'sc_data','qc_data.h5ad')
                logger.info(f"qc path : {qc_data_path}")
                if os.path.exists(qc_data_path):
                    logger.info("Loading data from previous Quality Control step")
                    scom = SCSurv(folder_path, selected_options, params, logger ,prev_data=qc_data_path)
                else:
                    logger.info("Previous step data not found, re-initializing SCSurv")
                    scom = SCSurv(folder_path, selected_options,params,logger)

                fig,fiter_data=scom.filter_cells_genes()
                # Convert the JSON string to a Python list (array of dictionaries)
                
                # Convert Bokeh figure to JSON
                plot_json = fig.to_json()
                # Save Quality Control output (if needed)
                 # Save Quality Control output in the sc_data directory
                save_intermediate_result(folder_path, 'qc_data.h5ad', scom.adata)
                
                #Return a response indicating successful processing
                return JsonResponse({
                    'message': 'Filter Cells & Genes step executed successfully',
                    'barcode_file': barcode_file,
                    'gene_file': gene_file,
                    'matrix_file': matrix_file,
                    'bokehPlot': plot_json,
                    'plotlyData': json.loads(plot_json),
                    'tableData':json.loads(fiter_data),
                    'lib':'plotly',
                    'currentStep': 'Filter Cells & Genes',
                    'nextStep': 'Doublet Detection'
                }, status=200)
            

            elif current_step == "Doublet Detection":
                selected_options['option']=current_step
                logger.info("Executing step: Doublet Detection")
                # Check if QC data is available from the previous step
                qc_data_path = os.path.join(folder_path, 'sc_data','qc_data.h5ad')
                logger.info(f"qc path : {qc_data_path}")
                if os.path.exists(qc_data_path):
                    logger.info("Loading data from previous Quality Control step")
                    
                    scom = SCSurv(folder_path, selected_options, params, logger ,prev_data=qc_data_path)
                else:
                    logger.info("Previous step data not found, re-initializing SCSurv")
                    scom = SCSurv(folder_path, selected_options ,params,logger)

                fig,fiter_data=scom.doublet_detection()
                # Convert the JSON string to a Python list (array of dictionaries)
                
                # Convert Bokeh figure to JSON
                plot_json = fig.to_json()
                # Save Quality Control output (if needed)
                 # Save Quality Control output in the sc_data directory
                save_intermediate_result(folder_path, 'qc_data.h5ad', scom.adata)
                
                #Return a response indicating successful processing
                return JsonResponse({
                    'message': 'Doublet Detection step executed successfully',
                    'barcode_file': barcode_file,
                    'gene_file': gene_file,
                    'matrix_file': matrix_file,
                    'bokehPlot': plot_json,
                    'plotlyData': json.loads(plot_json),
                    'tableData':json.loads(fiter_data),
                    'lib':'plotly',
                    'currentStep': 'Doublet Detection',
                    'nextStep': 'Normalization'
                }, status=200)
            
            elif current_step == "Normalization":
                selected_options['option']=current_step
                logger.info("Executing step: Normlization")
                # Check if QC data is available from the previous step
                qc_data_path = os.path.join(folder_path, 'sc_data','qc_data.h5ad')
                logger.info(f"qc path : {qc_data_path}")
                if os.path.exists(qc_data_path):
                    logger.info("Loading data from previous Quality Control step")
                    scom = SCSurv(folder_path, selected_options, params, logger ,prev_data=qc_data_path)
                else:
                    logger.info("Previous step data not found, re-initializing SCSurv")
                    scom = SCSurv(folder_path, selected_options,params,logger)

                fig,fiter_data=scom.normalization()
                # Convert the JSON string to a Python list (array of dictionaries)
                
                # Convert Bokeh figure to JSON
                plot_json = fig.to_json()
                # Save Quality Control output (if needed)
                 # Save Quality Control output in the sc_data directory
                save_intermediate_result(folder_path, 'qc_data.h5ad', scom.adata)
                
                #Return a response indicating successful processing
                return JsonResponse({
                    'message': 'Doublet Detection step executed successfully',
                    'barcode_file': barcode_file,
                    'gene_file': gene_file,
                    'matrix_file': matrix_file,
                    'bokehPlot': plot_json,
                    'plotlyData': json.loads(plot_json),
                    'tableData':json.loads(fiter_data),
                    'lib':'plotly',
                    'currentStep': 'Normalization',
                    'nextStep': 'Feature Selection'
                }, status=200)
            
            elif current_step == "Feature Selection":
                selected_options['option']=current_step
                logger.info("Executing step: Feature Selection")
                # Check if QC data is available from the previous step
                qc_data_path = os.path.join(folder_path, 'sc_data','qc_data.h5ad')
                logger.info(f"qc path : {qc_data_path}")
                if os.path.exists(qc_data_path):
                    logger.info("Loading data from previous Quality Control step")
                    scom = SCSurv(folder_path, selected_options, params, logger ,prev_data=qc_data_path)
                else:
                    logger.info("Previous step data not found, re-initializing SCSurv")
                    scom = SCSurv(folder_path, selected_options,params,logger)

                fig,fiter_data=scom.feature_selection()
                # Convert the JSON string to a Python list (array of dictionaries)
                
                # Convert Bokeh figure to JSON
                plot_json = fig.to_json()
                # Save Quality Control output (if needed)
                 # Save Quality Control output in the sc_data directory
                save_intermediate_result(folder_path, 'qc_data.h5ad', scom.adata)
                
                #Return a response indicating successful processing
                return JsonResponse({
                    'message': 'Doublet Detection step executed successfully',
                    'barcode_file': barcode_file,
                    'gene_file': gene_file,
                    'matrix_file': matrix_file,
                    'bokehPlot': plot_json,
                    'plotlyData': json.loads(plot_json),
                    'tableData':json.loads(fiter_data),
                    'lib':'plotly',
                    'currentStep': 'Feature Selection',
                    'nextStep': 'Dimensionality Reduction'
                }, status=200)
            
            elif current_step == "Dimensionality Reduction":
                selected_options['option']=current_step
                logger.info("Executing step: Dimensionality Reduction")
                # Check if QC data is available from the previous step
                qc_data_path = os.path.join(folder_path, 'sc_data','qc_data.h5ad')
                logger.info(f"qc path : {qc_data_path}")
                if os.path.exists(qc_data_path):
                    logger.info("Loading data from previous Quality Control step")
                    scom = SCSurv(folder_path, selected_options, params, logger ,prev_data=qc_data_path)
                else:
                    logger.info("Previous step data not found, re-initializing SCSurv")
                    scom = SCSurv(folder_path, selected_options,params,logger)
                fig,fiter_data=scom.dimensionality_reduction()
                # Convert the JSON string to a Python list (array of dictionaries)
                
                # Convert Bokeh figure to JSON
                plot_json = fig.to_json()
                # Save Quality Control output (if needed)
                 # Save Quality Control output in the sc_data directory
                save_intermediate_result(folder_path, 'qc_data_final.h5ad', scom.adata)
                
                #Return a response indicating successful processing
                return JsonResponse({
                    'message': 'Doublet Detection step executed successfully',
                    'barcode_file': barcode_file,
                    'gene_file': gene_file,
                    'matrix_file': matrix_file,
                    'bokehPlot': plot_json,
                    'plotlyData': json.loads(plot_json),
                    'tableData':json.loads(fiter_data),
                    'lib':'plotly',
                    'currentStep': 'Dimensionality Reduction',
                    'nextStep': 'Finished'
                }, status=200)
                
        else:
            return JsonResponse({'error': 'Invalid data type for this endpoint'}, status=400)
        



    except json.JSONDecodeError as e:
        logger.error("Invalid JSON data")
        return JsonResponse({'error': 'Invalid JSON data'}, status=400)
    except UploadedFile.DoesNotExist as e:
        logger.error(f"File does not exist: {str(e)}")
        return JsonResponse({'error': 'One or more files do not exist'}, status=404)
    except Exception as e:
        logger.error(f"Error processing selection: {str(e)}")
        return JsonResponse({'error': str(e)}, status=500)



def save_intermediate_result(folder_path, filename, adata):
    """Helper function to save intermediate results for each step as .h5ad file."""
    
    # Define the path for the sc_data directory within the existing folder_path
    sc_data_path = os.path.join(folder_path, 'sc_data')
    
    # Create the sc_data directory if it doesn't exist
    if not os.path.exists(sc_data_path):
        os.makedirs(sc_data_path)
        logger.info(f"Created directory for single-cell data at: {sc_data_path}")
    
    # Construct the full file path where the AnnData object will be saved
    filepath = os.path.join(sc_data_path, filename)
    
    # Save the AnnData object to the specified file
    adata.write(filepath)
    
    logger.info(f"Saved intermediate AnnData result: {filepath}")
    


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def process_selection(request, project_name):
    logger.info("process_selection endpoint called")
    
    try:
        # Parse the incoming JSON data
        
        user = request.user
        project = get_object_or_404(ProjectCard, project_name=project_name, user=user)
        if project.data_type=='Metabolomics':
            data = json.loads(request.body)
            selected_files = data.get('selectedFiles', [])
            selected_options = data.get('selectedOptions', {})
            metadata_enabled = data.get('metadataEnabled', False)
            params = data.get('params', {})
            
            # Log received data for debugging
            logger.info(f"Selected Files: {project_name}")
            logger.info(f"Selected Files: {selected_files}")
            logger.info(f"Selected Options: {selected_options}")
            logger.info(f"Metadata Enabled: {metadata_enabled}")

                # Processing logic
            processed_files = []
            folder_path=None
            for file_id in selected_files:
                file_instance = UploadedFile.objects.get(id=file_id, project__project_name=project_name)
                file_name = file_instance.name
                if not folder_path:
                    folder_path = os.path.dirname( file_instance.file.path)
                
                
                # Always add .mzML files to processed_files
                if file_name.endswith('.mzML'):
                    processed_files.append(file_name)
                    logger.info(f"Processing mzML file: {file_name}")
                    # Add your logic here to process the mzML file
                sample_file_path=None
                # Check for metadata-enabled processing
                if metadata_enabled:
                    # Look for sample.xlsx or sample.csv file
                    sample_file_path = os.path.join(os.path.dirname(file_instance.file.path), 'sample.xlsx')
                    if not os.path.exists(sample_file_path):
                        sample_file_path = os.path.join(os.path.dirname(file_instance.file.path), 'sample.csv')
                    
                    if os.path.exists(sample_file_path):
                        logger.info(f"Processing metadata file: {sample_file_path}")
                        # Add your logic here to process the sample file
                        processed_files.append(sample_file_path)
                    else:
                        logger.warning(f"Metadata file not found for {file_name}")



    
            #### 
            # 1. we need to read files with tidy ms
            # 2. map sample files 
            # 3. apply algorithms 

            ####
            
            # Log processing result
            logger.info(f"Processed Files: {processed_files}")
            if len(processed_files)>0:
                fig,feature_json=metaom.define_folder(sample_file_path, folder_path,selected_options,params)
                # Convert the JSON string to a Python list (array of dictionaries)
                feature_data = json.loads(feature_json)
                # Convert Bokeh figure to JSON
                plot_json = json_item(fig, "bokehPlot")

        # Return a response indicating successful processing
        return JsonResponse({
            'message': 'Data processed successfully',
            'selectedFiles': processed_files,
            'selectedOptions': selected_options,
            'metadataEnabled': metadata_enabled,
            'bokehPlot': plot_json,
            'tableData':feature_data,
            'lib':'bokeh'

        }, status=200)

    except json.JSONDecodeError as e:

        logger.error("Invalid JSON data")
        return JsonResponse({'error': 'Invalid JSON data'}, status=400)
    except UploadedFile.DoesNotExist as e:
        logger.error(f"File does not exist: {str(e)}")
        return JsonResponse({'error': 'One or more files do not exist'}, status=404)
    except Exception as e:
        logger.error(f"Error processing selection: {str(e)}")
        return JsonResponse({'error': str(e)}, status=500)