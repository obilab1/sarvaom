from django.urls import path
from . import views
from .views import (MyTokenObtainPairView,RegisterView,create_project_card,
                    get_project_cards,
                    get_plot_data,
                    upload_files,
                    list_files, delete_file, rename_file, update_file_metadata,
                    process_selection,
                    single_cell_process,
                    sc_populate_genes,
                    cellxgene_host
                    )


from rest_framework_simplejwt.views import (
    TokenRefreshView,
    )


urlpatterns = [
    path('', views.getRoutes),
    path('notes/', views.getNotes),
    path('register/', RegisterView.as_view(), name='register'),

    path('token/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('create_project_card/', create_project_card, name='create_project_card'),
    path('get_project_cards/', get_project_cards, name='get_project_cards'),
    path('get_plot_data/<str:project_name>/', get_plot_data, name='get_plot_data'),
    path('upload_files/<str:project_name>/', upload_files, name='upload_files'),
    path('projects/<str:project_name>/files/', list_files, name='list_files'),
    path('projects/<str:project_name>/files/<int:file_id>/', delete_file, name='delete_file'),
    path('projects/<str:project_name>/files/<int:file_id>/rename/', rename_file, name='rename_file'),
    path('projects/<str:project_name>/files/<int:file_id>/metadata/', update_file_metadata, name='update_file_metadata'),
    path('process_selection/<str:project_name>/', process_selection, name='process_selection'),  # New route
    path('single_cell_process/<str:project_name>/', single_cell_process, name='single_cell_process'),
    path('sc_populate_genes/<str:project_name>/', sc_populate_genes, name='sc_populate_genes'),
    path('cellxgene_host/<str:project_name>/', cellxgene_host, name='cellxgene_host')

]
