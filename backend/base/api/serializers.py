from rest_framework.serializers import ModelSerializer
from base.models import Note,ProjectCard,UploadedFile
from django.contrib.auth import get_user_model


class NoteSerializer(ModelSerializer):
    class Meta:
        model = Note
        fields = '__all__'


class ProjectCardSerializer(ModelSerializer):
    class Meta:
        model = ProjectCard
        fields = ['id', 'project_name', 'project_description', 'data_type', 'created_at']


class UploadedFileSerializer(ModelSerializer):
    class Meta:
        model = UploadedFile
        fields = ['id', 'name', 'file', 'metadata']


User = get_user_model()

class UserCreateSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password']
        )
        return user

