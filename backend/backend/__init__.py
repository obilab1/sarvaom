import logging
from newmet.dash_app import app  # Ensure the Dash app is imported and registered

logging.basicConfig(level=logging.INFO)
logging.info("Dash app 'SimpleExample' registered.")
